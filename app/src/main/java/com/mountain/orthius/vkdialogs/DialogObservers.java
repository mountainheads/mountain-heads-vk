package com.mountain.orthius.vkdialogs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by vladimir on 10.02.16.
 */

public class DialogObservers<T extends DialogsObserver> extends ArrayList<T> {
    public void notifyObjectModified(List<Map<String,Object>> obj) {
        for (Iterator<T> iter = (Iterator<T>) iterator(); iter.hasNext();)
            iter.next().objectModified(obj);
    }
}