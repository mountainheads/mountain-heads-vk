package com.mountain.orthius.vkdialogs;

/**
 * Created by vladimir on 10.02.16.
 */
public interface Observable {
    void registerObserver(DialogsObserver o);
    void removeObserver(DialogsObserver o);
}
