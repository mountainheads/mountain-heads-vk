package com.mountain.orthius.vkdialogs;

import android.os.Handler;
import android.os.Looper;

import com.mountain.orthius.UILApplication;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.longpollserver.LongPollObserver;
import com.mountain.orthius.longpollserver.LongPollServer;
import com.mountain.orthius.preferences.AppPreferences;
import com.mountain.orthius.utils.ConverterUtil;
import com.mountain.orthius.utils.TimerSearchBreaker;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vladimir on 10.02.16.
 * <p>
 * Наблюдаемый класс для слежения за изменениями диалогов
 */
public class Dialogs implements Observable {


    public interface OnNotPermListener {
        void onError(VKError error);
    }

    private DialogObservers dialogObservers;


    private Integer countUnreadDialogs = 0;
    private List<Map<String, Object>> dialogs;
    private List<Map<String, Object>> removedDialogs = new ArrayList<>();
    private ArrayList<String> userIds = new ArrayList<>();

    private static Dialogs instance;
    private LongPollServer longPollServer;
    private Integer countDialogs = 0;
    private Integer offset = 0;
    OnNotPermListener onNotPermListener;

    public static Dialogs getInstance() {
        if (instance == null) {
            instance = new Dialogs();
        }
        return instance;
    }

    public ArrayList<String> getUserIds() {
        return userIds;
    }

    public Dialogs() {
        dialogObservers = new DialogObservers();
    }

    @Override
    public void registerObserver(DialogsObserver o) {
        dialogObservers.add(o);
    }

    @Override
    public void removeObserver(DialogsObserver o) {
        dialogObservers.remove(o);
    }

    public void connect() {// загружаем 1 раз
        L.d("testDialog Dialogs connect");
        final Handler handler = new Handler(Looper.getMainLooper());
        if (dialogs != null) {// Content not load twice
            return;
        }

        tsb = new TimerSearchBreaker(new TimerSearchBreaker.ISearchTask() {
            @Override
            public void searchUpdate(String query) {
                final Handler handler = new Handler(Looper.getMainLooper());
                if (dialogs != null && offset >= countDialogs) {
                    return;
                }
                VKRequest request = new VKRequest("messages.getDialogs", VKParameters.from("count", 200, "offset", offset));
                request.setRequestListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(final VKResponse response) {
                        super.onComplete(response);

                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                L.d("VKResponse response = " + response.responseString);
                                try {
                                    offset += 200;
                                    tsb.progressComplete();
                                    if (dialogs == null) {
                                        dialogs = new ArrayList();
                                    }
                                    L.d("test dialogs size = " + dialogs.size());
                                    JSONObject responseObject = response.json.getJSONObject("response");

                                    if (!responseObject.isNull("unread_dialogs")) {
                                        countUnreadDialogs = responseObject.getInt("unread_dialogs");
                                    } else {
                                        countUnreadDialogs = 0;
                                    }
                                    L.d("VKResponse response2 = " + response.json);
//                                    JSONArray ids = response.json.getJSONArray("response");
//                                    for (int i = 0; i < ids.length(); i++){
//                                        userIds.add(ids.getJSONObject(i).getString("id"));
//                                    }

                                    countDialogs = responseObject.getInt("count");
                                    List<Map<String, Object>> items = ConverterUtil.toList(responseObject.getJSONArray("items"));
                                    L.d("test items = " + items);
                                    for (Map<String, Object> item : items) {
                                        Map<String, Object> message = (Map<String, Object>) item.get("message");

                                        if (message.get("chat_active") != null) {
                                            List<Integer> chat_active = (List<Integer>) message.get("chat_active");
                                            if (chat_active.size() != 0) {
                                                dialogs.add(item);
                                            }
                                        } else {
                                            if (message.get("user_id") != null) {
                                                dialogs.add(item);
                                            }
                                        }
                                    }
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            dialogObservers.notifyObjectModified(dialogs);
                                        }
                                    });
                                    initLongPollServer();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        thread.start();
                    }

                    @Override
                    public void onError(VKError error) {
                        if (onNotPermListener != null) {
                            onNotPermListener.onError(error);
                        }
                        L.d("error.apiError errorCode = " + error.errorCode + " errorMessage = " + error.errorMessage + " errorReason = " + error.errorReason);
                    }
                });
                request.start();
            }
        });
        addDialogs();
    }

    TimerSearchBreaker tsb;

    public void addDialogs() {
        tsb.runBefore("");
    }

    private void initLongPollServer() {
        if (longPollServer != null) {
            return;
        }
        L.d("testDialog Dialogs initLongPollServer");
        final Handler handler = new Handler(Looper.getMainLooper());
        longPollServer = LongPollServer.getInstance();
        longPollServer.registerObserver(new LongPollObserver() {
                                            @Override
                                            public void updateMessage(JSONObject response) {
                                                // редактируем dialogs

                                                try {
                                                    JSONArray updates = response.getJSONArray("updates");

                                                    for (int i = 0; i < updates.length(); i++) {
                                                        JSONArray item = updates.getJSONArray(i);
                                                        switch (item.getInt(0)) {
                                                            case 2://Удаление диалога
                                                                if (item.getInt(2) == 128) {
                                                                    removeDialog(item);
                                                                }
                                                                dialogObservers.notifyObjectModified(dialogs);
                                                                //{"ts":1644810774,"updates":[[2,457,128,2000000006]]}
                                                                break;
                                                            case 3://Восстановление диалога
                                                                if (item.getInt(2) == 128) {
                                                                    restoreDialog(item);
                                                                }
                                                                dialogObservers.notifyObjectModified(dialogs);
                                                                //{"ts":1644810812,"updates":[[3,456,128,10735373,1455478426," ... ","ёёуууу",{}]]}
                                                                break;
                                                            case 4://Получено новое сообщение
                                                                addNewMessage(item);
                                                                dialogObservers.notifyObjectModified(dialogs);
                                                                break;
                                                            case 6://Прочтение входящих сообщений
                                                                readMessage(item);
                                                                dialogObservers.notifyObjectModified(dialogs);
                                                                break;
                                                            case 7://Прочтение исходящих сообщений
                                                                reaOutMessage(item);
                                                                dialogObservers.notifyObjectModified(dialogs);
                                                                break;
                                                            case 61://Пользователь начал набирать текст в диалоге
                                                                writeOutMessage(item);
                                                                dialogObservers.notifyObjectModified(dialogs);
                                                                break;
                                                            case 62://Пользователь начал набирать текст в беседе
                                                                writeOutMessage(item);
                                                                dialogObservers.notifyObjectModified(dialogs);
                                                                break;
                                                            case 80:// Новый счетчик непрочитанных в левом меню
                                                                countUnreadDialogs = item.getInt(1);
                                                                dialogObservers.notifyObjectModified(dialogs);
                                                                break;
                                                        }
                                                    }

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }

        );
    }

    private Map<String, Object> getDialogById(Integer maimDialogId) { // Поиск диалога по chat_id или user_id
        Integer aimDialogId = maimDialogId;
        if (aimDialogId > 1000000000 && aimDialogId < 2000000000) {
            aimDialogId = -(maimDialogId - 1000000000);
        }

        for (Map<String, Object> dialog : dialogs) {
            Map<String, Object> message = (Map<String, Object>) dialog.get("message");
            Integer dialogId;
            if (message.get("chat_id") != null) {
                dialogId = 2000000000 + (int) message.get("chat_id");
            } else {
                dialogId = (int) message.get("user_id");
            }
            L.d("dialogId = " + dialogId + " aimDialogId = " + aimDialogId);
            if (dialogId.equals(aimDialogId)) {
                L.d("== dialogId = " + dialogId + " aimDialogId = " + aimDialogId);
                return dialog;
            }
        }
        return null;
    }

    private void addNewMessage(JSONArray item) {// добавление сообщения полученного через LongPollServer
        try {


            Map<String, Object> dialog = getDialogById(item.getInt(3));
            Map<String, Object> message;
            if (dialog != null) {
                dialogs.remove(dialog);

                message = (Map<String, Object>) dialog.get("message");
            } else { // Если диалога не было в списке
                dialog = new HashMap<>();
                message = new HashMap<>();
                dialog.put("message", message);
            }
            message.put("id", item.getInt(1));

            message.put("read_state", 0);

            if (item.getInt(3) >= 2000000000) {
                message.put("chat_id", item.getInt(3) - 2000000000);
                if (AppPreferences.getMyId(UILApplication.context).equals("" + item.getJSONObject(7).getInt("from"))) {
                    message.put("out", 1);
                } else {
                    message.put("out", 0);
                }
                message.put("user_id", Integer.parseInt(item.getJSONObject(7).getString("from")));
            } else {
                Integer aimDialogId = item.getInt(3);
                if (aimDialogId > 1000000000 && aimDialogId < 2000000000) {
                    aimDialogId = -(item.getInt(3) - 1000000000);
                }

                message.put("user_id", aimDialogId);
                if (item.getInt(2) == 2 || item.getInt(2) == 3 || item.getInt(2) == 7 || item.getInt(2) == 11 || item.getInt(2) == 19 || item.getInt(2) == 35 || item.getInt(2) == 51) {
                    message.put("out", 1);
                } else {
                    message.put("out", 0);
                }
            }
            message.put("body", item.getString(6));
            message.put("date", item.getInt(4));
            message.put("title", item.getString(5));
            message.put("write_message", null);
            getUnreadMessages(dialog);

            checkAtachments(message, item);
            dialogs.add(0, dialog);
        } catch (JSONException e) {
            //e.printStackTrace();
        }
    }

    private void getUnreadMessages(Map<String, Object> dialog) {
        Map<String, Object> message = (Map<String, Object>) dialog.get("message");
        if ((int) message.get("out") == 0) {
            int countUnread = 0;
            if (dialog.get("unread") != null) {
                countUnread = (int) dialog.get("unread");
            }
            countUnread++;
            dialog.put("unread", countUnread);
        }
    }

    private void readMessage(JSONArray item) {
        try {
            Map<String, Object> dialog = getDialogById(item.getInt(1));
            if (dialog != null) {
                dialog.put("unread", 0);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void reaOutMessage(JSONArray item) {
        try {
            Map<String, Object> dialog = getDialogById(item.getInt(1));
            if (dialog != null) {
                Map<String, Object> message = (Map<String, Object>) dialog.get("message");
                message.put("read_state", 1);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void writeOutMessage(JSONArray item) {
        try {
            Map<String, Object> dialog;
            if (item.getInt(0) == 61) {
                dialog = getDialogById(item.getInt(1));
            } else {
                dialog = getDialogById(2000000000 + item.getInt(2));
            }

            if (dialog != null) {
                final Map<String, Object> message = (Map<String, Object>) dialog.get("message");
                message.put("write_message", item.getInt(1));
                Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (message.get("write_message") == null) {
                            return;
                        }
                        message.put("write_message", null);
                        dialogObservers.notifyObjectModified(dialogs);
                    }
                }, 5000);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void checkAtachments(Map<String, Object> message, JSONArray item) {
        try {
            List attachments = new ArrayList<>();
            Map attachment = new HashMap<>();
            attachment.put("type", item.getJSONObject(7).getString("attach1_type"));
            attachments.add(attachment);
            message.put("attachments", attachments);
        } catch (JSONException e) {
            //e.printStackTrace();
        }
        try {
            item.getJSONObject(7).getString("fwd");
            message.put("fwd_messages", new ArrayList<>());
        } catch (JSONException e) {
            //e.printStackTrace();
        }
    }

    private void removeDialog(JSONArray item) {// удаляем диалог
        try {
            Map<String, Object> dialog = getDialogById(item.getInt(3));
            L.d("removeDialog dialog = " + dialog);
            if (dialog != null) {
                L.d("removeDialog");
                removedDialogs.add(dialog);
                dialogs.remove(dialog);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void restoreDialog(JSONArray item) {// восстанавливаем диалог
        try {
            for (Map<String, Object> dialog : removedDialogs) {
                Map<String, Object> message = (Map<String, Object>) dialog.get("message");
                Integer dialogId;
                if (message.get("chat_id") != null) {
                    dialogId = 2000000000 + (int) message.get("chat_id");
                } else {
                    dialogId = (int) message.get("user_id");
                }
                if (dialogId.equals(item.getInt(3))) {
                    dialogs.add(0, dialog);
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public List<Map<String, Object>> getListDialogs() {
        return dialogs;
    }

    public Integer getCountUnreadDialogs() {
        L.d("getCountUnreadDialogs countUnreadDialogs = " + countUnreadDialogs);
        return countUnreadDialogs;
    }

    public Boolean isConnect() {
        return dialogs != null;
    }

    public void stop() {
        if (longPollServer == null) {
            return;
        }
        longPollServer.stop();
        longPollServer = null;
        instance.clearDialogs();
    }

    private void clearDialogs() {
        dialogs = null;
    }

    public void setNotPermListener(OnNotPermListener onNotPermListener) {
        this.onNotPermListener = onNotPermListener;
    }
}

