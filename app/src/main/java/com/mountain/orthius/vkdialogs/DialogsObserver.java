package com.mountain.orthius.vkdialogs;

import java.util.List;
import java.util.Map;

/**
 * Created by vladimir on 10.02.16.
 */
public interface DialogsObserver {
    void objectModified(List<Map<String, Object>> obj);
}
