package com.mountain.orthius.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mountain.orthius.BottomNavigationViewHelper;
import com.mountain.orthius.Define;
import com.mountain.orthius.R;
import com.mountain.orthius.preferences.AppPreferences;
import com.mountain.orthius.view.activity.base.BaseActivity;
import com.mountain.orthius.view.fragments.MessageFragment;
import com.mountain.orthius.view.fragments.ProfileFragment;
import com.mountain.orthius.view.fragments.RankFragment;
import com.mountain.orthius.view.fragments.RulesFragment;
import com.mountain.orthius.loging.L;
import com.vk.sdk.VKSdk;

public class MainActivity extends BaseActivity {

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            L.d("onNavigationItemSelected = " + item.getItemId());
            Fragment fragment = null;

            switch (item.getItemId()) {
                case R.id.messages:
                    titleTV.setText("Сообщения");
                    fragment = new MessageFragment();
                    break;
                case R.id.profile:
                    titleTV.setText("Профиль");
                    fragment = new ProfileFragment();
                    Toast.makeText(getApplicationContext(), "Поиск может продолжаться до 2-х минут", Toast.LENGTH_LONG).show();
                    break;
                case R.id.rank:
                    titleTV.setText("Рейтинг");
                    fragment = new RankFragment();
                    break;
                case R.id.rules:
                    titleTV.setText("Правила");
                    fragment = new RulesFragment();
                    break;
            }

            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.content, fragment);
            transaction.commit();
            return true;
        }

    };
    private TextView titleTV;
    private TextView countCoinsTV;
    private LinearLayout countCoinsLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        L.d("matches com = " + "com".toString().matches("[^\\x00-\\x7F]+"));
        L.d("matches привет = " + "привет".toString().matches("[^\\x00-\\x7F]+"));


        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        titleTV = (TextView) findViewById(R.id.titleTV);
        titleTV.setText("Сообщения");
        countCoinsTV = (TextView) findViewById(R.id.countCoinsTV);

        countCoinsLL = (LinearLayout) findViewById(R.id.countCoinsLL);
        countCoinsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.disableShiftMode(navigation);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.content, new MessageFragment());
        transaction.commit();
    }

    @Override
    protected void onResume() {
        countCoinsTV.setText(""+AppPreferences.getCountCoins(getApplicationContext()));
        if(AppPreferences.getCountCoins(getApplicationContext()) > 0){
            countCoinsTV.setTextColor(getApplicationContext().getResources().getColor(R.color.get_coins));
        }else{
            countCoinsTV.setTextColor(getApplicationContext().getResources().getColor(R.color.pink));
        }

        super.onResume();
    }
}
