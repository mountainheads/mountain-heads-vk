package com.mountain.orthius.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mountain.orthius.Define;
import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;
import com.vk.sdk.VKSdk;
import com.vk.sdk.util.VKUtil;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());
        L.d("fingerprints = " + fingerprints[0]);

        if(VKSdk.isLoggedIn()){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            L.d("token = " + VKSdk.getAccessToken().accessToken);
            finish();
        }else{
            VKSdk.login(this, Define.SCOPE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(VKSdk.isLoggedIn()){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
