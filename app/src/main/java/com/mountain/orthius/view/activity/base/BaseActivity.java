package com.mountain.orthius.view.activity.base;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mountain.orthius.Define;
import com.mountain.orthius.R;
import com.mountain.orthius.UILApplication;
import com.mountain.orthius.view.activity.MainActivity;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.preferences.AppPreferences;
import com.mountain.orthius.vkdialogs.Dialogs;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import com.vk.sdk.util.VKUtil;

/**
 * Created by alexeybalabanov on 13.11.14.
 */
public class BaseActivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_READ_PHONE_STATE = 1;
    private RelativeLayout progressBarRL;
    private LinearLayout contentLL;
    private LinearLayout adPubNativeView;
    private Toolbar toolbar;

    interface onLifeCycleListener{
        public void onCreate();
        public void onResume();
        public void onPause();
        public void onStop();
        public void onDestroy();
    }
    public static final int REQUEST_CODE = 101;
    private static String S_TOKEN_KEY = "VK_ACCESS_TOKEN";
    public final static String STOP_LONG_PROCESSING_MESSAGE = "STOP_LONG_PROCESSING_MESSAGE";
    public final static String STOP_LONG_PROCESSING_EVENT = "STOP_LONG_PROCESSING_EVENT";

    protected ActionBar ab;
    private Boolean isShowActivity = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());
        L.d("fingerprints = " + fingerprints[0]);


        ImageView icon = (ImageView)findViewById(android.R.id.home);
        if(icon != null){
            icon.setPadding(Math.round(convertDpToPixel(8,getApplicationContext())),0,0,0);
        }
    }

    private void initToolBar() {
        if(toolbar == null){
            toolbar = (Toolbar)findViewById(R.id.toolbar);
            if(toolbar != null){
                setSupportActionBar(toolbar);
                enableHomeUpBack();
            }
        }
    }

    // Our handler for received Intents. This will be called whenever an Intent
    // with an action named "custom-event-name" is broadcasted.

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isShowActivity = true;

        //initAdmobBanner();
        //initToolBar();
        initDialogs();
    }

    private void initDialogs() {
        if (VKSdk.isLoggedIn()) {
            getDialogs().setNotPermListener(new Dialogs.OnNotPermListener() {
                @Override
                public void onError(VKError error) {
                    VKSdk.login(getActivity(), Define.SCOPE);
                }
            });
            getDialogs().connect();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        isShowActivity = false;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void vkLogin(){
        VKSdk.logout();
        VKSdk.login(getActivity(), Define.SCOPE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                AppPreferences.setAuthVk(getApplicationContext());

                AppPreferences.setVkId(getApplicationContext(), res.userId);
            }

            @Override
            public void onError(VKError error) {
                // Произошла ошибка авторизации (например, пользователь запретил авторизацию)
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public Activity getActivity(){
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                finish();
                // Toast.makeText(this, "home pressed", Toast.LENGTH_LONG).show();
                break;
        }

        return true;
    }

    protected void initProgressBar() {
        progressBarRL = (RelativeLayout) findViewById(R.id.progressBarRL);
        contentLL = (LinearLayout) findViewById(R.id.contentLL);
        contentProgress();
    }

    protected void contentLoaded() {
        L.d("contentLoaded()");
        progressBarRL.setVisibility(View.GONE);
        contentLL.setVisibility(View.VISIBLE);
    }

    protected void contentProgress() {
        L.d("contentProgress()");
        progressBarRL.setVisibility(View.VISIBLE);
        contentLL.setVisibility(View.GONE);
    }

    protected void enableHomeUpBack(){
        ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.show();
        ImageView icon = (ImageView)findViewById(android.R.id.home);
        if(icon != null){
            icon.setPadding(Math.round(convertDpToPixel(8, getApplicationContext())), 0, 0, 0);
        }

        ActivityInfo activityInfo = null;
        try {
            activityInfo = getPackageManager().getActivityInfo(
                    getComponentName(), PackageManager.GET_META_DATA);
            String title = activityInfo.loadLabel(getPackageManager())
                    .toString();
            setTitle(title);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    public Dialogs getDialogs(){
        return ((UILApplication) getApplication()).getDialogs();
    }

    protected Boolean isShowActivity(){
        return isShowActivity;
    }
}

