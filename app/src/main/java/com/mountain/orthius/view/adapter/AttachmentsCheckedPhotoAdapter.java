package com.mountain.orthius.view.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by vladimir on 26.08.16.
 */
public class AttachmentsCheckedPhotoAdapter extends RecyclerView.Adapter<AttachmentsCheckedPhotoAdapter.SimpleViewHolder> {
    public final static String IMAGE = "IMAGE";
    public final static String TYPE = "TYPE";

    public final static String TYPE_PHOTO = "photo";
    public final static String TYPE_VIDEO = "video";

    private final Context mContext;
    private final List<Map<String,Object>> mItems;
    private final RecyclerView attachmentsTWV;
    private List<Boolean> checkedList = new ArrayList<>();
    private Integer widthScreen;
    private Integer numColumns;

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        public final ImageView imageView1;
        public final ImageView stateIV;
        public final RelativeLayout rootRL;
        public final ImageView playIV;
        public final RelativeLayout blackRectRL;
        public final TextView titleTV;

        public SimpleViewHolder(View view) {
            super(view);
            imageView1 = (ImageView) view.findViewById(R.id.imageView1);
            stateIV = (ImageView) view.findViewById(R.id.stateIV);
            rootRL = (RelativeLayout) view.findViewById(R.id.rootRL);
            playIV = (ImageView) view.findViewById(R.id.playIV);
            blackRectRL = (RelativeLayout) view.findViewById(R.id.blackRectRL);
            titleTV = (TextView) view.findViewById(R.id.titleTV);
        }
    }

    public AttachmentsCheckedPhotoAdapter(Context context, RecyclerView attachmentsTWV) {
        mContext = context;
        this.mItems = new ArrayList<>();
        this.attachmentsTWV = attachmentsTWV;
        checkUploadAttachments();
    }
    public void addItems(List<Map<String,Object>> items) {
        for(Map<String,Object> item:items){
            addItem(item);
            checkedList.add(false);
        }
    }

    public void addItem(Map<String,Object> item) {
        mItems.add(item);
        notifyItemInserted(mItems.size()-1);
        checkUploadAttachments();
    }

    public void removeItem(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);

        checkUploadAttachments();
    }

    public void checkUploadAttachments(){
        if(getItemCount() == 0){
            attachmentsTWV.setVisibility(View.GONE);
        }else{
            attachmentsTWV.setVisibility(View.VISIBLE);
        }
    }
    public List<Map<String,Object>> getSelected(){
        List<Map<String,Object>> selected = new ArrayList<>();
        int i = 0;
        for(Boolean bool:checkedList){
            if(bool){
                selected.add(mItems.get(i));
            }
            i++;
        }
        return selected;
    }

    public void setAllUnselected(){
        List<Boolean> checkedListCopy = new ArrayList<>(checkedList);
        checkedList = new ArrayList<>();
        int i = 0;
        for(Map<String,Object> item:mItems){
            checkedList.add(false);
            if(checkedListCopy.get(i)){
                notifyItemChanged(i);
            }
            i++;
        }
    }


    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.item_attachment_checked, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {

        if(widthScreen != null && numColumns != null){
            int width = Math.round((widthScreen)/numColumns);
            holder.imageView1.setLayoutParams(
                    new RelativeLayout.LayoutParams(
                            width,
                            width));
            holder.rootRL.setLayoutParams(
                    new LinearLayout.LayoutParams(
                            width,
                            width));
        }
        Map<String,Object> item = mItems.get(position);

        if(item.get(IMAGE) != null){
            Glide.with(mContext).load(item.get(IMAGE)).centerCrop()
                    .placeholder(new ColorDrawable(mContext.getResources().getColor(R.color.blue_dark_empty_photo)))
                    .crossFade().into(new GlideDrawableImageViewTarget(holder.imageView1));
        }else{
            String photo = "";

            if(item.get("photo_604") != null){
                photo = item.get("photo_604").toString();
            }else if(item.get("photo_320") != null){
                photo = item.get("photo_320").toString();
            }else if(item.get("photo_130") != null){
                photo = item.get("photo_130").toString();
            }else if(item.get("photo_75") != null){
                photo = item.get("photo_75").toString();
            }

            Glide.with(mContext).load(photo).centerCrop()
                    .placeholder(new ColorDrawable(mContext.getResources().getColor(R.color.blue_dark_empty_photo)))
                    .crossFade().into(new GlideDrawableImageViewTarget(holder.imageView1));
        }

        if(item.get(TYPE) != null && item.get(TYPE).equals(TYPE_VIDEO)){
            holder.blackRectRL.setVisibility(View.VISIBLE);
            holder.playIV.setVisibility(View.VISIBLE);
            if(item.get("title") != null){
                holder.titleTV.setText(item.get("title").toString());
            }
        }else{
            holder.blackRectRL.setVisibility(View.GONE);
            holder.playIV.setVisibility(View.GONE);
        }


        if(checkedList.get(position)){
            holder.stateIV.setImageResource(R.drawable.ic_check_fill_24dp);
        }else{
            holder.stateIV.setImageResource(R.drawable.ic_check_empty_24dp);
        }

        holder.imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getSelected().size() >= 10){
                    Toast.makeText(mContext, "Максимум 10 вложений", Toast.LENGTH_LONG).show();
                    return;
                }
                L.d("click check = " + position + " " + checkedList.get(position));
                checkedList.set(position,!checkedList.get(position));
                notifyItemChanged(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setWidthScreen(Integer widthScreen) {
        this.widthScreen = widthScreen;
    }

    public void setNumColumns(Integer numColumns) {
        this.numColumns = numColumns;
    }
}
