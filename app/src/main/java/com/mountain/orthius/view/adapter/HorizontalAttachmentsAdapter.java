package com.mountain.orthius.view.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.mountain.orthius.R;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;



/**
 * Created by vladimir on 26.08.16.
 */
public class HorizontalAttachmentsAdapter extends RecyclerView.Adapter<HorizontalAttachmentsAdapter.SimpleViewHolder> {
    public final static String IMAGE = "IMAGE";
    public final static String DOC_PATH = "DOC_PATH";
    public final static String DOC_NAME = "DOC_NAME";

    public final static String TYPE = "TYPE";

    public final static String TYPE_PHOTO = "photo";
    public final static String TYPE_VIDEO = "video";
    public final static String TYPE_AUDIO = "audio";
    public final static String TYPE_DOC = "doc";
    public final static String TYPE_PLACE = "TYPE_PLACE";

    private final Context mContext;
    private List<Map<String,Object>> mItems;
    private final RecyclerView attachmentsTWV;
    private List<Boolean> checkedList = new ArrayList<>();
    private List<String> imagesToUpload;



    private static ClickListener clickListener;

    public void clearAll() {
        mItems = new ArrayList<>();
        checkVisible();
        notifyDataSetChanged();
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public final ImageView imageView1;
        public final RelativeLayout videoPanelRL;
        public final TextView titleTV;
        public final ImageView videoPlayIV;
        public final ImageView removeIV;
        public final RelativeLayout progressBarRL;

        public SimpleViewHolder(View view) {
            super(view);
            imageView1 = (ImageView) view.findViewById(R.id.imageView1);
            videoPlayIV = (ImageView) view.findViewById(R.id.videoPlayIV);
            videoPanelRL = (RelativeLayout) view.findViewById(R.id.videoPanelRL);
            titleTV = (TextView) view.findViewById(R.id.titleTV);
            removeIV = (ImageView) view.findViewById(R.id.removeIV);
            progressBarRL = (RelativeLayout) view.findViewById(R.id.progressBarRL);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public HorizontalAttachmentsAdapter(Context context, RecyclerView attachmentsTWV) {
        mContext = context;
        this.mItems = new ArrayList<>();
        this.attachmentsTWV = attachmentsTWV;
        checkVisible();
    }

    public void addItems(List<Map<String,Object>> items) {
        for(Map<String,Object> item:items){
            addItem(item);
            checkedList.add(false);
        }
        startUploadLocalFiles();

    }

    private void startUploadLocalFiles() {

    }


    public void addItem(Map<String,Object> item) {
        mItems.add(item);
        notifyItemInserted(mItems.size()-1);
        checkVisible();
    }

    public void clearGeoPlaceItem(){
        int i = 0;
        for(Map<String,Object> item:mItems){
            if(item.get(TYPE).equals(TYPE_PLACE)){
                removeItem(i);
            }
            i++;
        }
    }

    public void removeItem(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
        checkVisible();
    }

    private void checkVisible(){
        if(getItemCount() == 0){
            attachmentsTWV.setVisibility(View.GONE);
        }else{
            attachmentsTWV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.item_attachment_upload, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        Map<String, Object> item = mItems.get(position);
        holder.progressBarRL.setVisibility(View.GONE);
        if(item.get(IMAGE) != null){
            Glide.with(mContext).load(item.get(IMAGE)).centerCrop()
                    .placeholder(new ColorDrawable(mContext.getResources().getColor(R.color.blue_dark_empty_photo)))
                    .crossFade().into(new GlideDrawableImageViewTarget(holder.imageView1));
            holder.progressBarRL.setVisibility(View.VISIBLE);
        }else{
            String photo = "";

            if(item.get("photo_130") != null){
                photo = item.get("photo_130").toString();
            }else if(item.get("photo_75") != null){
                photo = item.get("photo_75").toString();
            }
            if(!photo.isEmpty()){
                Glide.with(mContext).load(photo).centerCrop()
                        .placeholder(new ColorDrawable(mContext.getResources().getColor(R.color.blue_dark_empty_photo)))
                        .crossFade().into(new GlideDrawableImageViewTarget(holder.imageView1));
            }
        }

        if(item.get(TYPE).equals(TYPE_VIDEO)){
            holder.videoPanelRL.setVisibility(View.VISIBLE);
            holder.videoPlayIV.setVisibility(View.VISIBLE);
            if(item.get("title") != null){
                holder.titleTV.setText(item.get("title").toString());
            }
        }else if(item.get(TYPE).equals(TYPE_AUDIO)){
            holder.videoPanelRL.setVisibility(View.VISIBLE);
            holder.videoPlayIV.setVisibility(View.VISIBLE);
            if(item.get("title") != null){
                holder.titleTV.setText(item.get("title").toString());
            }
            holder.videoPlayIV.setImageResource(R.drawable.ic_audiotrack_white_24dp);
        }else if(item.get(TYPE).equals(TYPE_DOC)){
            holder.videoPanelRL.setVisibility(View.VISIBLE);
            holder.videoPlayIV.setVisibility(View.VISIBLE);
            if(item.get("title") != null){
                holder.titleTV.setText(item.get("title").toString());
            }
            holder.videoPlayIV.setImageResource(R.drawable.ic_description_white_24dp);
        }else if(item.get(TYPE).equals(TYPE_PLACE)) {
            holder.videoPanelRL.setVisibility(View.VISIBLE);
            holder.videoPlayIV.setVisibility(View.VISIBLE);
            holder.titleTV.setText("Место");
            holder.videoPlayIV.setImageResource(R.drawable.ic_place_white_24dp);
        }else{
            holder.videoPanelRL.setVisibility(View.GONE);
            holder.videoPlayIV.setVisibility(View.GONE);
        }

    }

    public String getAttachmentsToSend(){
        String out = "";
        for(Map<String, Object> item:mItems){
            if(item.get(TYPE) != null && item.get("owner_id") != null && item.get("id") != null){
                if(item.get(TYPE).equals(TYPE_PLACE)){
                    continue;
                }
                if(!out.isEmpty()){
                    out += ",";
                }
                out += item.get(TYPE).toString() + item.get("owner_id").toString() + "_" + item.get("id");
            }
        }
        return out;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        HorizontalAttachmentsAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }
}
