package com.mountain.orthius.view.activity.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.mountain.orthius.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vladimir on 03.09.16.
 */
public abstract class BaseAttachmentsActivity extends BaseActivity {
    public static final String ATTACHMENTS = "ATTACHMENTS";
    public static final String USER_ID = "USER_ID";

    @BindView(R.id.recyclerView)
    protected RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attachments_gallery);
        ButterKnife.bind(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_attachments, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem itemMenu) {
        // TODO Auto-generated method stub
        switch (itemMenu.getItemId()) {
            case R.id.done:
                Intent data = new Intent();
                List<Map<String,Object>> selectList = getSelected();
                data.putExtra(ATTACHMENTS, (ArrayList) selectList);
                setResult(Activity.RESULT_OK, data);
                finish();
            default:
                return super.onOptionsItemSelected(itemMenu);
        }
    }

    abstract protected List<Map<String,Object>> getSelected();
}
