package com.mountain.orthius.view.fragments;

/**
 * Created by spbiphones on 20.10.2017.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;

import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.utils.JsonUtils;
import com.mountain.orthius.utils.TimerSearchBreaker;
import com.mountain.orthius.view.activity.base.BaseActivity;
import com.mountain.orthius.view.adapter.TalksAdapter;
import com.mountain.orthius.vkdialogs.Dialogs;
import com.mountain.orthius.vkdialogs.DialogsObserver;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;


import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vladimir on 21.03.16.
 */
public class MessageFragment extends BaseFragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    @BindView(R.id.talksLV)
    RecyclerView talksLV;
    private TalksAdapter talksAdapter;
    private Toolbar toolbar;
    private Dialogs dialogs;
    private View child;

    private Boolean isSearchMode = false;
    private TimerSearchBreaker tsb;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MessageFragment newInstance() {
        MessageFragment fragment = new MessageFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.content_chats, container, false);
        ButterKnife.bind(this, rootView);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);


        tsb = new TimerSearchBreaker(getActivity(), new TimerSearchBreaker.ISearchTask() {
            @Override
            public void searchUpdate(String query) {
                if(query == null || query.length() == 1){
                    return;
                }
                VKRequest request = new VKRequest("messages.search", VKParameters.from("q", query,"count","100"));
                request.setRequestListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(final VKResponse response) {
                        super.onComplete(response);
                        try {
                            JSONObject responseObject = response.json.getJSONObject("response");
                            List<Map<String,Object>> items =  (List<Map<String,Object>>) JsonUtils.jsonToMap(responseObject).get("items");
                            talksAdapter.setSearchedDialogs(items,isSearchMode);
                            isSearchMode = true;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(VKError error) {
                        L.d("error.apiError errorCode = " + error.errorCode + " errorMessage = " + error.errorMessage + " errorReason = " + error.errorReason);
                    }
                });
                request.start();
            }
        });

        setupTalksLV();
        initProgressBar();
        dialogs = ((BaseActivity)getActivity()).getDialogs();

        if(dialogs.getListDialogs() != null && !dialogs.getListDialogs().isEmpty()){
            contentLoaded();
            talksAdapter = new TalksAdapter(getActivity(),dialogs.getListDialogs());
            talksLV.setAdapter(talksAdapter);
        }

        dialogs.registerObserver(dialogsObserver);
        return rootView;
    }

    private void setupTalksLV() {
        final LinearLayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(getActivity());
        talksLV.setLayoutManager(mLayoutManager);
        talksLV.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0) {
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (++pastVisiblesItems + visibleItemCount + 5 > totalItemCount && talksAdapter != null) {
                        dialogs.addDialogs();
                    }
                }
            }
        });
    }

    private DialogsObserver dialogsObserver = new DialogsObserver() {
        @Override
        public void objectModified(List<Map<String,Object>> dialogList) {
            if(isSearchMode){
                return;
            }
            contentLoaded();
            L.d("dialogList = " + dialogList);
            if(talksAdapter == null){
                talksAdapter = new TalksAdapter(getActivity(),dialogList);
                talksLV.setAdapter(talksAdapter);
            }else {
                talksAdapter.setDialogs(dialogList);
            }
        }
    };

    public void onCreateActivity(){
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    public void filterByWord(String text){
        if(text.isEmpty()){
            talksAdapter.returnAllDialogs();
            isSearchMode = false;
        }else{
            tsb.run(text);
        }
    }
}