package com.mountain.orthius.view.adapter.base;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.makeramen.RoundedImageView;
import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.utils.ConverterUtil;
import com.mountain.orthius.utils.DateHandler;
import com.mountain.orthius.utils.asymmetricgridviev.DemoItem;
import com.mountain.orthius.utils.asymmetricgridviev.lib.AsymmetricRecyclerView;
import com.mountain.orthius.utils.asymmetricgridviev.lib.AsymmetricRecyclerViewAdapter;
import com.mountain.orthius.utils.image.ImageManager;
import com.mountain.orthius.utils.view.ExpandableHeightGridView;
import com.mountain.orthius.view.adapter.PVkAudioAdapter;
import com.mountain.orthius.view.adapter.PVkDocAdapter;
import com.mountain.orthius.view.adapter.PVkNoteAdapter;
import com.mountain.orthius.view.adapter.PVkPhotoAdapter;
import com.mountain.orthius.view.adapter.PVkPollAnswerAdapter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * Created by vladimir on 25.03.16.
 */
public class MediaVKContent {
    public enum ContentType {
        FULL_SCREEN_POST, POST, MESSAGES, RESEND_MESSAGES;
    }

    public void setupFields(final Activity act, Map<String, Object> response, Map<String, Object> post, final BaseViewHolder bvh, ContentType type) {
        hideViews(bvh);
        setupTextTV(bvh, post);
        setupDateTV(bvh, post);
        setupAttachments(act, post, bvh, type);
        setupOwnerInfo(act, response, post, bvh);
    }

    private void hideViews(BaseViewHolder bvh) {
        bvh.photoGV.setVisibility(View.GONE);
        bvh.oneImageIV.setVisibility(View.GONE);
        bvh.videoGV.setVisibility(View.GONE);
        bvh.audioLV.setVisibility(View.GONE);
        bvh.docLV.setVisibility(View.GONE);
        bvh.noteLV.setVisibility(View.GONE);
        bvh.pollLL.setVisibility(View.GONE);
        bvh.linkLL.setVisibility(View.GONE);
        bvh.stickerIV.setVisibility(View.GONE);
        bvh.albumRL.setVisibility(View.GONE);
    }

    private void setupTextTV(BaseViewHolder bvh, Map<String, Object> post) {
        if (post.get("text") != null && !post.get("text").toString().isEmpty()) {
            bvh.textTV.setVisibility(View.VISIBLE);
            bvh.textTV.setText(post.get("text").toString());
        } else {
            if (post.get("body") != null && !post.get("body").toString().isEmpty()) {
                bvh.textTV.setVisibility(View.VISIBLE);
                bvh.textTV.setText(post.get("body").toString());
            } else {
                bvh.textTV.setVisibility(View.GONE);
            }
        }
    }

    private void setupDateTV(BaseViewHolder bvh, Map<String, Object> post) {
        try {
            String date = DateHandler.getFullDateToJournal(DateHandler.convertDateToString(new Date(ConverterUtil.getLongFromString(post.get("date").toString()) * 1000)));
            bvh.createDateTV.setText(date);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    private void setupAttachments(final Activity act, Map<String, Object> post, final BaseViewHolder bvh, final ContentType type) {
        Display display = act.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        final int widthDisplay = size.x;
        List<Map<String, Object>> attachments = (List<Map<String, Object>>) post.get("attachments");
        if (post.get("geo") != null) {
            if (((Map<String, Object>) post.get("geo")).get("coordinates") != null) {
                String coordinates = ((Map<String, Object>) post.get("geo")).get("coordinates").toString();
                final String[] coordinatesArray = coordinates.split("\\s+");
                bvh.mapIV.setVisibility(View.VISIBLE);
                L.d("http://maps.google.com/maps/api/staticmap?center=" + coordinatesArray[0] + "," + coordinatesArray[1] + "&zoom=15&size=220x180&sensor=false&markers=icon:http://icon-icons.com/icons2/317/PNG/512/map-marker-icon_34392.png|" + coordinatesArray[0] + "," + coordinatesArray[1]);
                Glide.with(act).load("http://maps.google.com/maps/api/staticmap?center=" + coordinatesArray[0] + "," + coordinatesArray[1] + "&zoom=15&size=220x180&sensor=false&markers=icon:http://icon-icons.com/icons2/317/PNG/512/map-marker-icon_34392.png|" + coordinatesArray[0] + "," + coordinatesArray[1]).centerCrop()
                        .placeholder(new ColorDrawable(act.getResources().getColor(R.color.blue_dark_empty_photo)))
                        .crossFade().into(new GlideDrawableImageViewTarget(bvh.mapIV));

                bvh.mapIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:" + coordinatesArray[0] + "," + coordinatesArray[1]));
                        i.setClassName("com.google.android.apps.maps",
                                "com.google.android.maps.MapsActivity");
                        act.startActivity(i);
                    }
                });
            } else {
                bvh.mapIV.setVisibility(View.GONE);
            }
        } else {
            bvh.mapIV.setVisibility(View.GONE);
        }

        bvh.gifIV.setVisibility(View.GONE);

        if (attachments != null) {
            final ArrayList<String> photoList = new ArrayList<>();
            final List<Map<String, Object>> photoItems = new ArrayList<>();
            ArrayList<Map<String, Object>> audioList = new ArrayList<>();
            final ArrayList<Map<String, Object>> videoList = new ArrayList<>();
            final ArrayList<Map<String, Object>> docList = new ArrayList<>();
            ArrayList<Map<String, Object>> noteList = new ArrayList<>();
            ArrayList<Map<String, Object>> pollAnswerList = new ArrayList<>();
            for (Map<String, Object> attachment : attachments) { // заполняет массивы
                L.d("attachment = " + attachment);
                if (attachment.get("type").equals("photo")) {
//                    bvh.photoGV.setVisibility(View.VISIBLE);
                    Map<String, Object> photo = (Map<String, Object>) attachment.get("photo");

                    photoList.add(photo.get("photo_604").toString());
                    photoItems.add(photo);
                }
                if (attachment.get("type").equals("audio")) {
                    bvh.audioLV.setVisibility(View.VISIBLE);
                    Map<String, Object> audio = (Map<String, Object>) attachment.get("audio");
                    audioList.add(audio);
                }
                if (attachment.get("type").equals("video")) {
                    Map<String, Object> video = (Map<String, Object>) attachment.get("video");
                    //bvh.videoGV.setVisibility(View.VISIBLE);
                    bvh.photoGV.setVisibility(View.VISIBLE);
                    videoList.add(video);
                }
                if (attachment.get("type").equals("doc")) {
                    bvh.docLV.setVisibility(View.VISIBLE);
                    Map<String, Object> doc = (Map<String, Object>) attachment.get("doc");
                    docList.add(doc);
                }
                if (attachment.get("type").equals("note")) {
                    bvh.noteLV.setVisibility(View.VISIBLE);
                    Map<String, Object> doc = (Map<String, Object>) attachment.get("note");
                    noteList.add(doc);
                }
                if (attachment.get("type").equals("poll")) {
                    bvh.pollLL.setVisibility(View.VISIBLE);
                    Map<String, Object> poll = (Map<String, Object>) attachment.get("poll");
                    if (poll.get("question") != null) {
                        bvh.pollTV.setText(poll.get("question").toString());
                        bvh.pollTV.setVisibility(View.VISIBLE);
                    } else {
                        bvh.pollTV.setVisibility(View.GONE);
                    }
                    List<Map<String, Object>> answers = (List<Map<String, Object>>) poll.get("answers");

                    for (Map<String, Object> answer : answers) {
                        pollAnswerList.add(answer);
                    }
                }
                if (attachment.get("type").equals("link")) {
                    bvh.linkLL.setVisibility(View.VISIBLE);
                    final Map<String, Object> link = (Map<String, Object>) attachment.get("link");

                    if (link.get("title") != null) {
                        bvh.linkTitleTV.setText(link.get("title").toString());
                    }
                    if (link.get("url") != null) {
                        bvh.linkUrlTV.setText(link.get("url").toString());
                        bvh.linkLL.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(link.get("url").toString()));
                                act.startActivity(intent);
                            }
                        });
                    }
                    if (link.get("description") != null) {
                        bvh.linkDescriptionTV.setText(link.get("description").toString());
                    }

                }
                if (attachment.get("type").equals("sticker")) {
                    final Map<String, Object> sticker = (Map<String, Object>) attachment.get("sticker");

                    if (sticker.get("photo_128") != null) {
                        bvh.stickerIV.setVisibility(View.VISIBLE);
                        Glide.with(act).load(sticker.get("photo_128").toString()).centerCrop()
                                .placeholder(new ColorDrawable(act.getResources().getColor(R.color.blue_dark_empty_photo)))
                                .crossFade().into(bvh.stickerIV);
                    }
                }
                if (attachment.get("type").equals("gift")) {
                    final Map<String, Object> gift = (Map<String, Object>) attachment.get("gift");

                    if (gift.get("thumb_256") != null) {
                        bvh.stickerIV.setVisibility(View.VISIBLE);
                        Glide.with(act).load(gift.get("thumb_256").toString()).centerCrop()
                                .placeholder(new ColorDrawable(act.getResources().getColor(R.color.blue_dark_empty_photo)))
                                .crossFade().into(bvh.stickerIV);
                    }
                }
                if (attachment.get("type").equals("album")) {
                    final Map<String, Object> album = (Map<String, Object>) attachment.get("album");

                    if (album.get("thumb") != null) {
                        Map<String, Object> thumb = (Map<String, Object>) album.get("thumb");
                        bvh.albumRL.setVisibility(View.VISIBLE);

                        String photoURL = "";

                        if (thumb.get("photo_807") != null) {
                            photoURL = thumb.get("photo_807").toString();
                        } else if (thumb.get("photo_604") != null) {
                            photoURL = thumb.get("photo_604").toString();
                        } else if (thumb.get("photo_130") != null) {
                            photoURL = thumb.get("photo_130").toString();
                        }



                        int itemWidth = 0;
                        if (type.equals(ContentType.MESSAGES)) {//Если это сообщение
                            itemWidth = widthDisplay - (int) ConverterUtil.convertDpToPixel(76, act);
                        } else if (type.equals(ContentType.RESEND_MESSAGES)) {
                            itemWidth = widthDisplay - (int) ConverterUtil.convertDpToPixel(76, act);
                        } else if (type.equals(ContentType.FULL_SCREEN_POST)) {
                            itemWidth = widthDisplay;
                        } else {//Если обычный пост
                            itemWidth = widthDisplay - (int) ConverterUtil.convertDpToPixel(32, act);
                        }

                        bvh.albumRL.getLayoutParams().width = itemWidth;
                        bvh.albumRL.getLayoutParams().height = itemWidth;

                        Glide.with(act).load(photoURL).centerCrop()
                                .placeholder(new ColorDrawable(act.getResources().getColor(R.color.blue_dark_empty_photo)))
                                .crossFade().into(bvh.albumPhotoIV);


                    }
                    if (album.get("title") != null) {
                        bvh.albumTitleTV.setText(album.get("title").toString());
                        if (album.get("size") != null) {
                            bvh.albumTitleTV.setText(bvh.albumTitleTV.getText() + " (" + album.get("size") + " фото)");
                        }
                    }
                }
            }
            L.d("photoList = " + photoList);
            if (!photoList.isEmpty() || !videoList.isEmpty()) { // отображаем контент
                L.d("photoList.size() = " + photoList.size());
                L.d("videoList.isEmpty() = " + videoList.isEmpty());
                if(photoList.size() == 1 && videoList.isEmpty()){
                    L.d("photoList in if");
                    bvh.oneImageIV.setVisibility(View.VISIBLE);
                    Picasso.with(act)
                            .load(photoList.get(0)).placeholder(R.drawable.ic_account_box_56dp).into(bvh.oneImageIV, new Callback() {

                        @Override
                        public void onSuccess() {
                            Float aspect = (float)bvh.oneImageIV.getDrawable().getIntrinsicHeight() / (float)bvh.oneImageIV.getDrawable().getIntrinsicWidth();
                            Integer width = widthDisplay;
                            Integer height = Math.round(widthDisplay * aspect);
                            bvh.oneImageIV.getLayoutParams().width = width;
                            bvh.oneImageIV.getLayoutParams().height = height;
//                            bvh.oneImageIV.setImageBitmap(bitmap);
                        }

                        @Override
                        public void onError() {

                        }
                    });

//                    Glide.with(act).load(photoList.get(0)).fitCenter()
//                            .placeholder(new ColorDrawable(act.getResources().getColor(R.color.blue_dark_empty_photo)))
//                            .crossFade().into(bvh.oneImageIV);
                }else{
                    bvh.photoGV.setVisibility(View.VISIBLE);
                    L.d("videoList 2 = " + videoList);

                    final ArrayList<Map<String, Object>> photoMapList = new ArrayList<>();
                    for (String photoPath : photoList) {
                        Map photoMap = new HashMap();
                        photoMap.put("type", "photo");
                        photoMap.put("link", photoPath);
                        photoMapList.add(photoMap);
                    }
                    photoMapList.addAll(videoList);

                    bvh.photoGV.setRequestedColumnCount(6);

                    List<DemoItem> demoList = new ArrayList<>();
//                Integer miniTiles = ((int) Math.floor(photoMapList.size()/3)) * 3;
//                Integer bigTiles = photoMapList.size() - miniTiles;
//                final Integer allRows = miniTiles/3*2+bigTiles*3;


                    Integer miniTiles = ((int) Math.floor(photoMapList.size() / 3)) * 3;
                    Integer middleTiles = ((int) Math.floor((photoMapList.size() - miniTiles) / 2)) * 2;
                    Integer bigTiles = photoMapList.size() - miniTiles - middleTiles;
                    final Integer allRows = miniTiles / 3 * 4 + middleTiles / 2 * 3 + bigTiles * 6;
                    L.d("allRows = " + allRows + " miniTiles = " + miniTiles + " middleTiles = " + middleTiles + " bigTiles = " + bigTiles);

                    int itemWidth = 0;
                    if (type.equals(ContentType.MESSAGES)) {//Если это сообщение
                        itemWidth = widthDisplay - (int) ConverterUtil.convertDpToPixel(76, act);
                    } else if (type.equals(ContentType.RESEND_MESSAGES)) {
                        itemWidth = widthDisplay - (int) ConverterUtil.convertDpToPixel(76, act);
                    } else if (type.equals(ContentType.FULL_SCREEN_POST)) {
                        itemWidth = widthDisplay;
                    } else {//Если обычный пост
                        itemWidth = widthDisplay - (int) ConverterUtil.convertDpToPixel(32, act);
                    }


                    final int heightGallery = allRows * (int) Math.floor(itemWidth / 6 - (int) ConverterUtil.convertDpToPixel(4, act));
                    ViewGroup.LayoutParams lp = bvh.photoGV.getLayoutParams();
                    lp.height = heightGallery;
                    lp.width = itemWidth;
                    bvh.photoGV.setLayoutParams(lp);
//                bvh.photoGV.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        Resources resources = act.getResources();
//                        DisplayMetrics metrics = resources.getDisplayMetrics();
//                    }
//                });
                    int i = 0;
                    for (Map<String, Object> photoPath : photoMapList) {
                        int colSpan;
                        if (i < miniTiles) {
                            if ((Math.floor(i / 3)) % 2 == 0) {
                                if (i % 3 == 0) {
                                    colSpan = 4;
                                } else {
                                    colSpan = 2;
                                }
                            } else {
                                if (i % 3 == 2) {
                                    colSpan = 4;
                                } else {
                                    colSpan = 2;
                                }
                            }
                        } else if (i < miniTiles + middleTiles) {
                            colSpan = 3;
                        } else {
                            colSpan = 6;
                        }
                        int rowSpan = colSpan;
                        DemoItem item = new DemoItem(colSpan, rowSpan, i, photoPath);
                        demoList.add(item);
                        i++;
                    }
//                Collections.reverse(demoList);
                    //bvh.photoGV.setDebugging(true);

                    final PVkPhotoAdapter pVkPhotoAdapter = new PVkPhotoAdapter(act, demoList, new PVkPhotoAdapter.OnItemClickListener() {
                        @Override
                        public void clickItem(Integer position, Boolean isVideo) {
                            if (isVideo) {
                                Integer ownerID = (int) photoMapList.get(position).get("owner_id");
                                String videoID = ownerID + "_" + photoMapList.get(position).get("id").toString();
                                if (photoMapList.get(position).get("access_key") != null) {
                                    videoID += "_" + photoMapList.get(position).get("access_key");
                                }

                                VKRequest request = new VKRequest("video.get", VKParameters.from("videos", videoID, "owner_id", ownerID));

                                request.setRequestListener(new VKRequest.VKRequestListener() {
                                    @Override
                                    public void onComplete(VKResponse response) {
                                        super.onComplete(response);
                                        L.d("VKResponse response = " + response.responseString);


                                    }

                                    @Override
                                    public void onError(VKError error) {
                                        L.d("error.apiError errorCode = " + error.errorCode + " errorMessage = " + error.errorMessage + " errorReason = " + error.errorReason);
                                    }
                                });
                                request.start();
                            } else {

                            }

                        }
                    });

                    final AsymmetricRecyclerViewAdapter asymmetricAdapter =
                            new AsymmetricRecyclerViewAdapter(act, bvh.photoGV, pVkPhotoAdapter);
                    bvh.photoGV.post(new Runnable() {
                        @Override
                        public void run() {
                            bvh.photoGV.setAdapter(asymmetricAdapter);
                            L.d("photoGV width = " + bvh.photoGV.getWidth());
                            L.d("heightGallery = " + heightGallery + " photoGV getHeight = " + bvh.photoGV.getHeight());
                        }
                    });

                    //bvh.photoGV.setAdapter(asymmetricAdapter);
                }

            }
            if (!audioList.isEmpty()) {
                final PVkAudioAdapter pVkAudioAdapter = new PVkAudioAdapter(act, audioList);


                bvh.audioLV.setAdapter(pVkAudioAdapter);
                setListViewHeightBasedOnChildren(bvh.audioLV);

                bvh.audioLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Integer ownerID = (int) pVkAudioAdapter.getItem(i).get("owner_id");
                        String audioID = ownerID + "_" + pVkAudioAdapter.getItem(i).get("id").toString();

                        VKRequest request = new VKRequest("audio.getById", VKParameters.from("audios", audioID, "owner_id", ownerID));
                        request.setRequestListener(new VKRequest.VKRequestListener() {
                            @Override
                            public void onComplete(VKResponse response) {
                                super.onComplete(response);
                                L.d("VKResponse response = " + response.responseString);


                            }

                            @Override
                            public void onError(VKError error) {
                                L.d("error.apiError errorCode = " + error.errorCode + " errorMessage = " + error.errorMessage + " errorReason = " + error.errorReason);
                            }
                        });
                        request.start();
                    }
                });
            }

            if (!docList.isEmpty()) {
                final PVkDocAdapter pVkDocAdapter = new PVkDocAdapter(act, docList);
                bvh.docLV.setAdapter(pVkDocAdapter);
                setListViewHeightBasedOnChildren(bvh.docLV);
                bvh.docLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        if (pVkDocAdapter.getItem(i).get("url") == null) {
                            return;
                        }

                    }
                });
                for (Map<String, Object> doc : docList) {
                    if (doc.get("ext").equals("gif")) {
                        try {
//                            Glide.with(act) // replace with 'this' if it's in activity
//                                    .load(doc.get("url").toString())
//                                    .asGif()
//                                    .error(R.color.blue_light) // show error drawable if the image is not a gif
//                                    .into(bvh.gifIV);
//                            bvh.gifIV.setImageURI(Uri.parse(doc.get("url").toString()));
                            String htmlString = "<!DOCTYPE html><html><body style = \"text-align:center\"><img src=\""+doc.get("url").toString()+"\" alt=\"pageNo\" width=\"100%\"></body></html>";
                            bvh.gifIV.loadDataWithBaseURL(null, htmlString, "text/html", "UTF-8", null);

                            bvh.gifIV.setVisibility(View.VISIBLE);
                            bvh.gifIV.setWebViewClient(new WebViewClient() {
                                @Override
                                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                    view.loadUrl(url);
                                    return true;
                                }
                            });
                            bvh.gifIV.post(new Runnable() {
                                @Override
                                public void run() {
                                    L.d("bvh.gifIV.getWidth() = " + bvh.gifIV.getWidth());
                                    if(bvh.gifIV.getWidth() != 0){
                                        int height = bvh.gifIV.getWidth();
                                        int width = bvh.gifIV.getWidth();
                                        bvh.gifIV.setLayoutParams(new LinearLayout.LayoutParams(height, width));
                                    }
                                    L.d("getHeight 1 = " + bvh.gifIV.getLayoutParams().height);
                                    L.d("getHeight 2 = " + bvh.gifIV.getHeight());
                                }
                            });
                            //bvh.gifIV.loadUrl(doc.get("url").toString());
                            L.d("set visible gif");
                            L.d("getVisibility = " + bvh.gifIV.getVisibility());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                }

            }
            if (!noteList.isEmpty()) {
                PVkNoteAdapter pVkNoteAdapter = new PVkNoteAdapter(act, noteList);
                bvh.noteLV.setAdapter(pVkNoteAdapter);
                setListViewHeightBasedOnChildren(bvh.noteLV);
            }

            if (!pollAnswerList.isEmpty()) {
                PVkPollAnswerAdapter pVkPollAnswerAdapter = new PVkPollAnswerAdapter(act, pollAnswerList);
                bvh.pollLV.setAdapter(pVkPollAnswerAdapter);
                setListViewHeightBasedOnChildren(bvh.pollLV);
            }
            if (!pollAnswerList.isEmpty()) {
                PVkPollAnswerAdapter pVkPollAnswerAdapter = new PVkPollAnswerAdapter(act, pollAnswerList);
                bvh.pollLV.setAdapter(pVkPollAnswerAdapter);
                setListViewHeightBasedOnChildren(bvh.pollLV);
            }
        }
    }

    private void setupOwnerInfo(final Activity act, Map<String, Object> response, Map<String, Object> post, final BaseViewHolder bvh) {
        if (response != null) {
            String from_id;
            if (post.get("from_id") != null) {
                from_id = ("" + post.get("from_id"));
            } else {
                from_id = ("" + post.get("source_id"));
            }
            if (from_id.startsWith("-")) {
                from_id = from_id.replace("-", "");
                List<Map<String, Object>> groups = (List<Map<String, Object>>) response.get("groups");
                for (Map<String, Object> group : groups) {
                    if (group.get("id").toString().equals(from_id)) {
                        if (group.get("name") != null) {
                            bvh.nameTV.setText(group.get("name").toString());
                        }
                        if (group.get("photo_100") != null) {
                            ImageManager.loadRoundedImage(act, group.get("photo_50").toString(), bvh.avatarIV);
                        }
                        break;
                    }
                }
            } else {
                List<Map<String, Object>> profiles = (List<Map<String, Object>>) response.get("profiles");
                for (Map<String, Object> profile : profiles) {
                    if (profile.get("id").toString().equals(from_id)) {
                        if (profile.get("first_name") != null) {
                            bvh.nameTV.setText(profile.get("first_name").toString());
                        }
                        if (profile.get("last_name") != null) {
                            if (bvh.nameTV.getText().toString().isEmpty()) {
                                bvh.nameTV.setText(profile.get("last_name").toString());
                            } else {
                                bvh.nameTV.append(" " + profile.get("last_name").toString());
                            }
                        }
                        if (profile.get("photo_50") != null) {
                            ImageManager.loadRoundedImage(act, profile.get("photo_50").toString(), bvh.avatarIV);
                        }
                        break;
                    }
                }
            }
        }
    }

    public void setListViewHeightBasedOnChildren(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        L.d(" = " + params.height);
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public void initBaseFields(BaseViewHolder baseViewHolder, View view) {
        baseViewHolder.avatarIV = (RoundedImageView) view.findViewById(R.id.avatarIV);
        baseViewHolder.nameTV = (TextView) view.findViewById(R.id.nameTV);
        baseViewHolder.createDateTV = (TextView) view.findViewById(R.id.createDateTV);

        baseViewHolder.textTV = (TextView) view.findViewById(R.id.textTV);
        baseViewHolder.photoGV = (AsymmetricRecyclerView) view.findViewById(R.id.photoGV);
        baseViewHolder.oneImageIV = (ImageView) view.findViewById(R.id.oneImageIV);
        baseViewHolder.videoGV = (ExpandableHeightGridView) view.findViewById(R.id.videoGV);
        baseViewHolder.audioLV = (ListView) view.findViewById(R.id.audioLV);
        baseViewHolder.docLV = (ListView) view.findViewById(R.id.docLV);
        baseViewHolder.noteLV = (ListView) view.findViewById(R.id.noteLV);
        baseViewHolder.pollLL = (LinearLayout) view.findViewById(R.id.pollLL);

        baseViewHolder.linkLL = (LinearLayout) view.findViewById(R.id.linkLL);
        baseViewHolder.linkTitleTV = (TextView) view.findViewById(R.id.linkTitleTV);
        baseViewHolder.linkUrlTV = (TextView) view.findViewById(R.id.linkUrlTV);
        baseViewHolder.linkDescriptionTV = (TextView) view.findViewById(R.id.linkDescriptionTV);

        baseViewHolder.stickerIV = (ImageView) view.findViewById(R.id.stickerIV);

        baseViewHolder.pollLV = (ListView) view.findViewById(R.id.pollLV);
        baseViewHolder.pollTV = (TextView) view.findViewById(R.id.pollTitleTV);
        baseViewHolder.mapIV = (ImageView) view.findViewById(R.id.mapIV);
        baseViewHolder.albumRL = (RelativeLayout) view.findViewById(R.id.albumRL);

        baseViewHolder.albumPhotoIV = (ImageView) view.findViewById(R.id.albumPhotoIV);
        baseViewHolder.albumTitleTV = (TextView) view.findViewById(R.id.albumTitleTV);
        baseViewHolder.gifIV = (WebView) view.findViewById(R.id.gifIV);
    }

    public static class BaseViewHolder {
        RoundedImageView avatarIV;
        TextView nameTV;
        TextView createDateTV;

        TextView textTV;
        AsymmetricRecyclerView photoGV;
        ImageView oneImageIV;
        ExpandableHeightGridView videoGV;
        ListView audioLV;
        ListView docLV;
        ListView noteLV;
        LinearLayout pollLL;

        LinearLayout linkLL;
        TextView linkTitleTV;
        TextView linkUrlTV;
        TextView linkDescriptionTV;

        ImageView stickerIV;
        ListView pollLV;
        TextView pollTV;
        ImageView mapIV;

        RelativeLayout albumRL;
        ImageView albumPhotoIV;
        TextView albumTitleTV;

        WebView gifIV;
    }
}
