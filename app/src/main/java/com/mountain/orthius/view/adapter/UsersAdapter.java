package com.mountain.orthius.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.RoundedImageView;
import com.mountain.orthius.R;
import com.mountain.orthius.utils.PicassoCrutch;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class UsersAdapter extends BaseAdapter {
    private Context ctx;
    private LayoutInflater lInflater;
    private List<Map<String, String>> users;

    public UsersAdapter(Context context, List<Map<String, String>> users) {
        ctx = context;
        this.users = users;
        if(ctx != null){
            lInflater = (LayoutInflater) ctx
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Map<String, String> getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item_user, parent, false);
        }

        Map<String, String> user = getItem(position);

        RoundedImageView avatarIV = (RoundedImageView) view.findViewById(R.id.avatarIV);

        TextView nameTV = (TextView) view.findViewById(R.id.nameTV);
        TextView rankTV = (TextView) view.findViewById(R.id.rankTV);
        TextView countCoinsTV = (TextView) view.findViewById(R.id.countCoinsTV);
        TextView countCoins1TV = (TextView) view.findViewById(R.id.countCoins1TV);
        LinearLayout countCoinsLL = (LinearLayout) view.findViewById(R.id.countCoinsLL);
        LinearLayout yourLL = (LinearLayout) view.findViewById(R.id.yourLL);

        countCoinsTV.setVisibility(View.GONE);
        countCoinsLL.setVisibility(View.GONE);
        if(position == 0){
            countCoinsLL.setVisibility(View.VISIBLE);
        }else{
            countCoinsTV.setVisibility(View.VISIBLE);
        }
        yourLL.setVisibility(View.GONE);
        if(position == 10){
            yourLL.setVisibility(View.VISIBLE);
        }


        nameTV.setText(user.get("name"));
        rankTV.setText(user.get("rank"));
        countCoinsTV.setText(user.get("countCoins"));
        countCoins1TV.setText(user.get("countCoins"));

        if(user.get("photo") != null){
            PicassoCrutch.loadRoundedImage(ctx, user.get("photo").toString(), avatarIV);
        }



        return view;
    }


}

