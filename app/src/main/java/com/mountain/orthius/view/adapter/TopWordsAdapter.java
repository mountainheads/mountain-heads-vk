package com.mountain.orthius.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.RoundedImageView;
import com.mountain.orthius.R;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;


public class TopWordsAdapter extends RecyclerView.Adapter<TopWordsAdapter.MyViewHolder> {
    private Context ctx;


    private ArrayList<String> allOutWords;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView wordTV;



        public MyViewHolder(View view) {
            super(view);
            wordTV = (TextView) view.findViewById(R.id.wordTV);

        }
    }


    public TopWordsAdapter(Context context, ArrayList<String> allOutWords) {
        ctx = context;
        this.allOutWords = allOutWords;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_top_word_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

            holder.wordTV.setText(allOutWords.get(position));





    }

    @Override
    public int getItemCount() {
        return allOutWords.size();
    }


}
