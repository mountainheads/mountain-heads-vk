package com.mountain.orthius.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mountain.orthius.R;
import com.mountain.orthius.view.activity.base.BaseActivity;

public class AllRulesActivity extends BaseActivity {

    private WebView contentWV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_rules);
        enableHomeUpBack();
        contentWV = (WebView) findViewById(R.id.contentWV);
        contentWV.setWebViewClient(new WebViewClient());
        contentWV.loadUrl("https://best-language.ru");
    }
}
