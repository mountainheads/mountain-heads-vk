package com.mountain.orthius.view.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.utils.asymmetricgridviev.DemoItem;
import com.mountain.orthius.utils.asymmetricgridviev.ViewHolder;
import com.mountain.orthius.utils.asymmetricgridviev.lib.AGVRecyclerViewAdapter;

import java.util.List;
import java.util.Map;



public class PVkPhotoAdapter extends AGVRecyclerViewAdapter<ViewHolder> {
    private final OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void clickItem(Integer position, Boolean isVideo);
    }
    Context ctx;
    LayoutInflater lInflater;
    List<DemoItem> imageLinks;


    public PVkPhotoAdapter(Context context, List<DemoItem> imageLinks, OnItemClickListener onItemClickListener) {
        ctx = context;
        this.imageLinks = imageLinks;
        this.onItemClickListener = onItemClickListener;
        //Collections.reverse(this.imageLinks);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("RecyclerViewActivity", "onCreateView");
        return new ViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        L.d("onBindViewHolder = " + position);
        final Map<String,Object> photoInfo = imageLinks.get(position).getPhotoInfo();
        if(photoInfo.get("type") != null && photoInfo.get("type").equals("photo")){
            Glide.with(ctx).load(photoInfo.get("link").toString()).centerCrop()
                    .placeholder(new ColorDrawable(ctx.getResources().getColor(R.color.blue_dark_empty_photo)))
                    .crossFade().into(new GlideDrawableImageViewTarget(holder.image) {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                    super.onResourceReady(resource, animation);
                    //never called
                    if (ctx == null) {
                        return;
                    }
                    holder.image.setImageDrawable(resource);
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    super.onLoadFailed(e, errorDrawable);
                    //never called
                }
            });
        }else{
            holder.videoPanelRL.setVisibility(View.VISIBLE);
            holder.videoPlayIV.setVisibility(View.VISIBLE);
            if(photoInfo.get("title") != null){
                holder.titleTV.setText(photoInfo.get("title").toString());
            }
            String link = "";
            if(photoInfo.get("photo_640") != null){
                link = photoInfo.get("photo_640").toString();
            }else if(photoInfo.get("photo_320") != null){
                link = photoInfo.get("photo_320").toString();
            }else if(photoInfo.get("photo_130") != null){
                link = photoInfo.get("photo_130").toString();
            }

            Glide.with(ctx).load(link).centerCrop()
                    .placeholder(new ColorDrawable(ctx.getResources().getColor(R.color.blue_dark_empty_photo)))
                    .crossFade().into(new GlideDrawableImageViewTarget(holder.image) {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                    super.onResourceReady(resource, animation);
                    //never called
                    if (ctx == null) {
                        return;
                    }
                    holder.image.setImageDrawable(resource);
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    super.onLoadFailed(e, errorDrawable);
                    //never called
                }
            });
        }


        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.clickItem(position,!(photoInfo.get("type") != null && photoInfo.get("type").equals("photo")));
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2 == 0 ? 1 : 0;
    }

    @Override
    public int getItemCount() {
        return imageLinks.size();
    }

    @Override
    public DemoItem getItem(int position) {
        return imageLinks.get(position);
    }
}

