package com.mountain.orthius.view.fragments;

/**
 * Created by spbiphones on 20.10.2017.
 */

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mountain.orthius.R;
import com.mountain.orthius.view.activity.AllRulesActivity;
import com.mountain.orthius.view.activity.GamesActivity;
import com.mountain.orthius.view.activity.SpeakTrueScreen;

public class RulesFragment extends BaseFragment {
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rules, viewGroup, false);


        TextView fontUniq1TV = (TextView) view.findViewById(R.id.fontUniq1TV);
        TextView fontUniq2TV = (TextView) view.findViewById(R.id.fontUniq2TV);
        TextView fontUniq3TV = (TextView) view.findViewById(R.id.fontUniq3TV);

        CardView rulesCV = view.findViewById(R.id.rulesCV);
        rulesCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AllRulesActivity.class);
                startActivity(intent);
            }
        });

        TextView moreGamesTV = (TextView) view.findViewById(R.id.moreGamesTV);
        moreGamesTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), GamesActivity.class);
                startActivity(intent);
            }
        });

        TextView moreRulesTV = (TextView) view.findViewById(R.id.moreRulesTV);
        moreRulesTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SpeakTrueScreen.class);
                startActivity(intent);

            }
        });

        TextView textWordDayTV = (TextView) view.findViewById(R.id.textWordDayTV);
        String sourceString = "<b>Эпидерсия</b> – это загадочная, непонятная, часто неприятная, нежелательная вещь или ситуация. <br> <b>Синонимы:</b> фигня, херня, ерунда, лажа.";
        textWordDayTV.setText(Html.fromHtml(sourceString));


        TextView speakTrueTV = (TextView) view.findViewById(R.id.speakTrueTV);
        String sourceString2 = "<font color='#f27a94'>Одеть</font> Надежду – <font color='#f27a94'>надеть</font> одежду. <br><font color='#f27a94'>Одеть</font> Наташку – <font color='#f27a94'>надеть</font> рубашку. <br>Т.е <font color='#f27a94'>одеть</font> кого-то, надеть что-то.";
        speakTrueTV.setText(Html.fromHtml(sourceString2), TextView.BufferType.SPANNABLE);

        return view;
    }
}