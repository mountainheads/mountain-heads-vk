package com.mountain.orthius.view.fragments;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mountain.orthius.R;


/**
 * Created by vladimir on 21.03.16.
 */
public class BaseFragment extends Fragment {
    protected View rootView;
    protected RelativeLayout progressBarRL;
    private LinearLayout contentLL;

    protected void initProgressBar() {
        progressBarRL = (RelativeLayout) rootView.findViewById(R.id.progressBarRL);
        contentLL = (LinearLayout) rootView.findViewById(R.id.contentLL);
        contentProgress();
    }

    protected void contentLoaded() {
        progressBarRL.setVisibility(View.GONE);
        contentLL.setVisibility(View.VISIBLE);
    }

    protected void contentProgress() {
        progressBarRL.setVisibility(View.VISIBLE);
        contentLL.setVisibility(View.GONE);
    }
}
