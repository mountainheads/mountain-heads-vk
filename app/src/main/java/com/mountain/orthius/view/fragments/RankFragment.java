package com.mountain.orthius.view.fragments;

/**
 * Created by spbiphones on 20.10.2017.
 */

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.preferences.AppPreferences;
import com.mountain.orthius.utils.JsonUtils;
import com.mountain.orthius.view.adapter.UsersAdapter;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RankFragment extends BaseFragment {
    private ListView topLV;
    private List<Map<String,String>> users = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup viewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_rank, viewGroup, false);

        initProgressBar();
        topLV = (ListView) rootView.findViewById(R.id.topLV);

        final VKRequest request = new VKRequest("users.get", VKParameters.from("user_ids", AppPreferences.getMyId(getActivity()),"fields", "photo_50"));
        request.setRequestListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(final VKResponse response) {
                super.onComplete(response);
                try {
                    List<Map<String,Object>> users = (List<Map<String,Object>>) JsonUtils.toMap(response.json).get("response");
                    Map<String,Object> user = users.get(0);

                    addUser("    1.", "https://pp.userapi.com/c320116/v320116006/1abe/EPhF3ro_EeM.jpg","Виктор Хакатонов", "10 351");
                    addUser("    2.", "https://pp.userapi.com/c604530/v604530160/11d2c/8YYERnXXo4M.jpg","Маша Эрмитова", "9 029");
                    addUser("    3.", "https://sun9-15.userapi.com/c540106/v540106181/2ee13/iq4xureWi7Q.jpg","Лена Третьякова", "8 814");
                    addUser("    4.", "https://pp.userapi.com/c638322/v638322484/10caf/J5DMAxBaNng.jpg","Вася Тиньков", "8 612");
                    addUser("    5.", "https://pp.userapi.com/c624421/v624421892/fd32/TKtgS-UBJZA.jpg","Эльдар Дуров", "8 581");
                    addUser("    6.", "https://pp.userapi.com/c626923/v626923943/47a40/Zxp02lBZDQY.jpg","Вера Кадрова", "8 212");
                    addUser("    7.", "https://pp.userapi.com/c604329/v604329456/fd23/HhazjQ4m_uo.jpg","Максим Быков", "8 179");
                    addUser("    8.", "https://pp.userapi.com/c616824/v616824220/18efa/FQauev-d5Bk.jpg","Евгений Морозов", "7 972");
                    addUser("    9.", "https://pp.userapi.com/c310624/v310624876/5a2a/itl3pMxYxKg.jpg","Андрей Рожков", "7 862");
                    addUser("  10.", "https://pp.userapi.com/c637618/v637618500/4771f/yAvv10J8qdw.jpg","Алексей Кузьмин", "7 524");

                    try{
                        addUser(827 - (AppPreferences.getCountCoins(getActivity())) + ".", user.get("photo_50").toString(),user.get("first_name") + " " + user.get("last_name"), ""+AppPreferences.getCountCoins(getActivity()));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    contentLoaded();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VKError error) {
                if(error.apiError != null && error.apiError.errorCode == 6){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            request.repeat();
                        }
                    }, 1000);
                }
                L.d("error.apiError errorCode = " + error.apiError);
            }
        });
        request.start();



        UsersAdapter usersAdapter = new UsersAdapter(getActivity(),users);
        topLV.setAdapter(usersAdapter);
        return rootView;
    }

    private void addUser(String rank, String photo, String name, String countCoind){
        Map<String,String> user = new HashMap<>();
        user.put("rank",rank);
        user.put("photo",photo);
        user.put("name",name);
        user.put("countCoins",countCoind);

        users.add(user);
    }
}