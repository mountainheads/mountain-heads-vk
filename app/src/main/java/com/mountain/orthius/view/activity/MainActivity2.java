package com.mountain.orthius.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.mountain.orthius.R;
import com.mountain.orthius.utils.SpellerManager;
import com.mountain.orthius.utils.view.ColorTextView;
import com.mountain.orthius.view.activity.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity2 extends BaseActivity {

    public static final String TEXT = "TEXT";

    @BindView(R.id.mistakesTextTV)
    ColorTextView mistakesTextTV;
    @BindView(R.id.rightWordTV)
    TextView rightWordTV;
    @BindView(R.id.variantsTV)
    TextView variantsTV;
    @BindView(R.id.myWordET)
    EditText myWordET;

    @BindView(R.id.fixAllBTN)
    Button fixAllBTN;
    @BindView(R.id.nextWordBTN)
    Button nextWordBTN;
    @BindView(R.id.fixWordBTN)
    Button fixWordBTN;

    @BindView(R.id.variantsLL)
    LinearLayout variantsLL;
    private SpellerManager spellerManager;

    @BindView(R.id.sendBTN)
    Button sendBTN;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        enableHomeUpBack();
        setContentView(R.layout.activity_main2);
        String text = "Пришло теплае лето. На лисной опушки распускаюца колоколчики, незабутки, шыповник. Белые ромашки пратягивают к сонцу свои нежные лепески. Вылитают из уютных гнёзд птинцы. У зверей взраслеет смена. Мидвежата старше всех. Они радились еще холодной зимой в берлоги. Теперь они послушно следуют за строгай матерью. Рыжые лесята весело играют у нары. А кто мелькает в сасновых ветках? Да это лофкие бельчята совершают свои первые высотные прышки. В сумерках выходят на охоту колючии ежата." +
                "Не обижайте лесных малышей. Приходите в лес верными друзями. Унас под крыльцом живут ежы. По вечерам вся семья выходит гулять. Взрослые ежи роют землю маленькими лапами. Они достают корешки и едят. Маленкие ежата в это время играют, резвяца.\n" +
                "Аднажды к старому ежу подбежала сабака. Ёж свернулся вклубок и замер. Собака осторожно покатила ежа кпруду. Ёш плюхнулся в воду и поплыл. Я прогнал сабаку. На следующюю весну остался под крыльцом один старый ёжек. Куда девались остальные? Они переселились в другое место. Старый ёж незахотел пакинуть мой дом.";

        text = getIntent().getStringExtra(TEXT);
        ButterKnife.bind(this);
        spellerManager = new SpellerManager(text, mistakesTextTV, rightWordTV, myWordET, variantsTV, variantsLL, true);

        spellerManager.checkText();

        fixAllBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                spellerManager.fixMistakes();

            }
        });
        fixWordBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spellerManager.fixCurrentMistake();
            }
        });
        nextWordBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spellerManager.checkNextWord();
            }
        });

        sendBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("message", mistakesTextTV.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
