package com.mountain.orthius.view.fragments;

/**
 * Created by spbiphones on 20.10.2017.
 */

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.makeramen.RoundedImageView;
import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.preferences.AppPreferences;
import com.mountain.orthius.utils.ConverterUtil;
import com.mountain.orthius.utils.DeclensionManager;
import com.mountain.orthius.utils.JsonUtils;
import com.mountain.orthius.utils.PicassoCrutch;
import com.mountain.orthius.utils.StatisticsGraphicController;
import com.mountain.orthius.view.activity.HelpActivity;
import com.mountain.orthius.view.activity.StartActivity;
import com.mountain.orthius.vkdialogs.Dialogs;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileFragment extends Fragment {
    @BindView(R.id.graphLC)
    LineChart graphLC;

    @BindView(R.id.avatarIV)
    RoundedImageView avatarIV;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.exitTV)
    TextView exitTV;
    @BindView(R.id.countWordsTV)
    TextView countWordsTV;
    @BindView(R.id.percentTV)
    TextView percentTV;
    @BindView(R.id.myWordsTV)
    TextView myWordsTV;
    @BindView(R.id.infoLL)
    LinearLayout infoLL;

    @BindView(R.id.coinsCountTV)
    TextView coinsCountTV;
    @BindView(R.id.topWordsRV)
    RecyclerView topWordsRV;
    @BindView(R.id.topMistakesRV)
    RecyclerView topMistakesRV;
    @BindView(R.id.progressRL)
    RelativeLayout progressRL;
    @BindView(R.id.progressRL2)
    RelativeLayout progressRL2;
    @BindView(R.id.progressRL3)
    RelativeLayout progressRL3;
    @BindView(R.id.textProgressPB)
    ProgressBar textProgressPB;
    private StatisticsGraphicController statisticsGraphicController;
    private ArrayList<String> topUserIds = new ArrayList<>();
    private LinearLayoutManager verticalLayoutManager;
    private LinearLayoutManager verticalLayoutManager2;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistics_profile, viewGroup, false);
        ButterKnife.bind(this, view);

        coinsCountTV.setText(DeclensionManager.declension(AppPreferences.getCountCoins(getActivity()), "очко","очка","очков"));
        infoLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });
        statisticsGraphicController = new StatisticsGraphicController();
        exitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VKSdk.logout();
                Intent intent = new Intent(getActivity(), StartActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        verticalLayoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        verticalLayoutManager2
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        topWordsRV.setLayoutManager(verticalLayoutManager);
        topMistakesRV.setLayoutManager(verticalLayoutManager2);

        loadMyInfo();

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (statisticsGraphicController!=null){
            statisticsGraphicController.setStop();
        }
        L.d(("onDestroy profilefragment"));
    }

    private void loadMyInfo(){


        List<Map<String, Object>> dialogs = Dialogs.getInstance().getListDialogs();
        for (Map<String, Object> dialog : dialogs){
            Map<String,Object> message = (Map<String,Object>)dialog.get("message");
            L.d("ProfileFragment user_ids = " + message.get("user_id"));

            topUserIds.add(message.get("user_id").toString());

        }
        loadStats();
        statisticsGraphicController.setProgress(progressRL,progressRL2,progressRL3, textProgressPB,myWordsTV);
        statisticsGraphicController.getData(topUserIds, graphLC, getActivity(), percentTV, countWordsTV, topWordsRV, topMistakesRV);


    }

    private void loadStats(){
        final VKRequest request = new VKRequest("users.get",  VKParameters.from("fields", "photo_100"));
        request.setRequestListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(final VKResponse response) {
                super.onComplete(response);
                try {
                    JSONArray array = response.json.getJSONArray("response");
                    PicassoCrutch.loadRoundedImage(getContext(), array.getJSONObject(0).getString("photo_100"), avatarIV);
                    nameTV.setText(array.getJSONObject(0).getString("first_name") + " " + array.getJSONObject(0).getString("last_name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(VKError error) {
            }
        });
        request.start();


    }
}