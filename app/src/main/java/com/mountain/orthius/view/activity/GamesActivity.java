package com.mountain.orthius.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mountain.orthius.R;
import com.mountain.orthius.view.activity.base.BaseActivity;

public class GamesActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games);

        enableHomeUpBack();
    }
}
