package com.mountain.orthius.view.adapter.base;

import android.app.Activity;
import android.view.View;
import android.widget.BaseAdapter;

import java.util.Map;

/**
 * Created by vladimir on 01.02.16.
 */
public abstract class MediaVKBaseAdapter extends BaseAdapter {

    private MediaVKContent mediaVKContent;

    public MediaVKBaseAdapter(){
        mediaVKContent = new MediaVKContent();
    }

    protected void setupFields(final Activity act, Map<String, Object> response, Map<String, Object> post, final MediaVKContent.BaseViewHolder bvh, MediaVKContent.ContentType contentType) {
        mediaVKContent.setupFields(act, response, post, bvh,contentType);
    }

    protected void initBaseFields(MediaVKContent.BaseViewHolder baseViewHolder, View view){
        mediaVKContent.initBaseFields(baseViewHolder,view);
    }
}
