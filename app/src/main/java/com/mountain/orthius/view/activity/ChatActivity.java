package com.mountain.orthius.view.activity;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.makeramen.RoundedImageView;
import com.mountain.orthius.Define;
import com.mountain.orthius.R;
import com.mountain.orthius.UILApplication;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.longpollserver.LongPollObserver;
import com.mountain.orthius.longpollserver.LongPollServer;
import com.mountain.orthius.model.MistakesModel;
import com.mountain.orthius.preferences.AppPreferences;
import com.mountain.orthius.utils.ConverterUtil;
import com.mountain.orthius.utils.JsonUtils;
import com.mountain.orthius.utils.TimerSearchBreaker;
import com.mountain.orthius.utils.image.ImageManager;
import com.mountain.orthius.utils.view.PickAttachments;
import com.mountain.orthius.view.activity.base.BaseActivity;
import com.mountain.orthius.view.adapter.MessageAdapter;
import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;
import com.rockerhieu.emojicon.emoji.Objects;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends BaseActivity implements EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {
    public final static String CHAT_ID = "CHAT_ID";
    public final static String USER_ID = "USER_ID";

    public final static String USER_NAME = "USER_NAME";
    public final static String USER_AVATAR = "USER_AVATAR";

    public final static String CHAT_ACTIVE_UIDS = "CHAT_ACTIVE_UIDS";

    private MessageAdapter messageAdapter;
    private String userId;
    private String chatId;
    private Integer dialogId;
    private String userName;
    private String userAvatar;
    private String chatsActiveUids;
    private LongPollServer longPollServer;
    private volatile Integer lastMessageId = 0;
    private int firstMessageId = 0;

    @BindView(R.id.messageListLV)
    ExpandableListView messageListLV;
    @BindView(R.id.messageET) EmojiconEditText messageET;
    @BindView(R.id.sendBTN)
    ImageButton sendBTN;
    @BindView(R.id.emojiBTN)
    ImageButton emojiBTN;

    @BindView(R.id.writeStatusLL)
    LinearLayout writeStatusLL;
    @BindView(R.id.writeStatusTV)
    TextView writeStatusTV;

    @BindView(R.id.getCoinsTV)
    TextView getCoinsTV;


    private int countMessages;
    private View emojicons;
    private PickAttachments pickAttachments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        enableHomeUpBack();
        initProgressBar();
        setTitle("");
        getCoinsTV.setVisibility(View.INVISIBLE);
        userId = getIntent().getStringExtra(USER_ID);
        chatId = getIntent().getStringExtra(CHAT_ID);

        if(chatId!=null){
            setTitle(getString(R.string.chat_activity_group_talk));
        }

        userName = getIntent().getStringExtra(USER_NAME);

        userAvatar = getIntent().getStringExtra(USER_AVATAR);
        chatsActiveUids = getIntent().getStringExtra(CHAT_ACTIVE_UIDS);

        L.d("chatId = " + chatId + " userName = " + userName + " userId = " + userId);

        messageListLV.setDivider(null);
        messageListLV.setDividerHeight(0);
        messageListLV.setGroupIndicator(null);

        messageListLV.setEmptyView(findViewById(R.id.emptyLL));

        messageListLV.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                if (++firstVisibleItem + visibleItemCount - 20 < 0 && messageAdapter != null && isCanLoading) {
                    tsbHistory.runBefore("");
                }
            }
        });

        emojicons =  findViewById(R.id.emojicons);

        emojicons.setVisibility(View.GONE);
        emojiBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();
                scrollMyListViewToBottom();
                if(emojicons.getVisibility() == View.GONE){
                    emojicons.setVisibility(View.VISIBLE);
                    disableSoftInputFromAppearing(messageET);
                    emojiBTN.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_keyboard_black_24dp));
                }else {
                    emojiBTN.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_insert_emoticon_black_24dp));
                    enableSoftInputFromAppearing(messageET);
                    emojicons.setVisibility(View.GONE);
                }
            }
        });

        setupSendUI();
        initTSBHistory();
        addMessageList();
        setupLongPollServer();
        setupActionBar();

        initTSBOnline();
        pickAttachments = new PickAttachments(getActivity(),false);
        setEmojiconFragment(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        String message = data.getStringExtra("message");
        messageET.setText(message);
        sendMessage();
    }

    private void setEmojiconFragment(boolean useSystemDefault) {
//        getSupportFragmentManager()
//                .beginTransaction()
//                .replace(R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
//                .commit();
        messageET.setUseSystemDefault(useSystemDefault);
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(messageET, emojicon);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(messageET);
    }



    private void setupLongPollServer() {
        longPollServer = LongPollServer.getInstance();
        longPollServer.registerObserver(longPollObserver);
    }

    private LongPollObserver longPollObserver = new LongPollObserver() {
        @Override
        public void updateMessage(JSONObject response) {
            try {
                JSONArray updates = response.getJSONArray("updates");
                for (int i = 0; i < updates.length(); i++) {
                    JSONArray item = updates.getJSONArray(i);
                    switch (item.getInt(0)) {
                        case 2://Прочтение входящих сообщений
                            if (item.getInt(2) == 128) {

                            }
                            break;
                        case 3://Прочтение входящих сообщений
                            if (item.getInt(2) == 128) {

                            }
                            break;
                        case 4://Получено новое сообщение
                            Integer currentId = item.getInt(3) ;
                            if(currentId > 1000000000 && currentId < 2000000000){
                                currentId = -(item.getInt(3) - 1000000000);
                            }
                            L.d("dialogId = " + dialogId + " currentId = " + currentId);
                            if (dialogId.equals(currentId)) {
                                writeStatusLL.setVisibility(View.GONE);
                                addNewMessage(response.getString("ts"), item.getInt(1));
                                L.d("ChatActivity addNewMessage");
                            }
                            break;
                        case 6://Прочтение входящих сообщений
                            break;
                        case 7://Прочтение исходящих сообщений
                            if(messageAdapter != null){
                                messageAdapter.reaOutMessage();
                            }
                            break;
                        case 61://Пользователь начал набирать текст в диалоге
                            if (dialogId == item.getInt(1)) {
                                writeStatusLL.setVisibility(View.VISIBLE);
                                writeStatusTV.setText("Печатает...");
                                Handler handler = new Handler(Looper.getMainLooper());
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        writeStatusLL.setVisibility(View.GONE);
                                    }
                                }, 5000);
                            }
                            break;
                        case 62://Пользователь начал набирать текст в беседе
                            if (dialogId == (2000000000 + item.getInt(2))) {
                                String firstName = messageAdapter.getUser(item.getInt(1)+"").get("first_name").toString();

                                writeStatusLL.setVisibility(View.VISIBLE);
                                writeStatusTV.setText(firstName + " печатает...");
                                Handler handler = new Handler(Looper.getMainLooper());
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        writeStatusLL.setVisibility(View.GONE);
                                    }
                                }, 5000);
                            }
                            break;
                        case 80:
                            break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void setupActionBar() {
        if(userId == null || userId.isEmpty()){
            return;
        }
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(actionBar.getDisplayOptions() | ActionBar.DISPLAY_SHOW_CUSTOM);
        RoundedImageView abAvatarIV = new RoundedImageView(actionBar.getThemedContext());
        abAvatarIV.setCornerRadius(ConverterUtil.convertDpToPixel(56, getApplicationContext()));
        abAvatarIV.setScaleType(ImageView.ScaleType.CENTER);
        abAvatarIV.setImageResource(R.drawable.ic_ab_done);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                (int)ConverterUtil.convertDpToPixel(40,getApplicationContext()),
                (int) ConverterUtil.convertDpToPixel(40,getApplicationContext()), Gravity.LEFT
                | Gravity.CENTER_VERTICAL);
        abAvatarIV.setLayoutParams(layoutParams);
        ImageManager.loadRoundedImage(getApplicationContext(), userAvatar, abAvatarIV);

        TextView title = new TextView(actionBar.getThemedContext());
        title.setText(userName);
        title.setTextSize(18);
        title.setPadding((int) ConverterUtil.convertDpToPixel(12, getApplicationContext()), (int) ConverterUtil.convertDpToPixel(6, getApplicationContext()), 0, 0);

        title.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.vk_white));

        LinearLayout abRoot = new LinearLayout(actionBar.getThemedContext());
        abRoot.addView(abAvatarIV);
        abRoot.addView(title);

        actionBar.setCustomView(abRoot);

        abRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void addMessageList() {
        tsbHistory.runBefore("");
    }
    private TimerSearchBreaker tsbHistory;
    private void initTSBHistory() {
        tsbHistory = new TimerSearchBreaker(new TimerSearchBreaker.ISearchTask() {
            @Override
            public void searchUpdate(String query) {
                if(messageAdapter != null){
                    if(countMessages == messageAdapter.getMessages().size()) {
                        return;
                    }
                }
                VKRequest request;
                if(firstMessageId != 0){
                    firstMessageId--;
                }
                if (userId != null) {
                    dialogId = Integer.parseInt(userId);
                    L.d("userId = " + userId + " firstMessageId = " + firstMessageId);
                    if(firstMessageId == 0){
                        request = new VKRequest("messages.getHistory", VKParameters.from("count", 200, "user_id", userId));
                    }else{
                        request = new VKRequest("messages.getHistory", VKParameters.from("count", 200, "user_id", userId,"start_message_id",firstMessageId));
                    }

                } else {
                    int peer_id = (2000000000 + Integer.parseInt(chatId));
                    L.d("peer_id = " + peer_id);
                    dialogId = peer_id;
                    if(firstMessageId == 0){
                        request = new VKRequest("messages.getHistory", VKParameters.from("count", 200, "peer_id", peer_id, "version", "5.38"));
                    }else{
                        request = new VKRequest("messages.getHistory", VKParameters.from("count", 200, "peer_id", peer_id,"start_message_id",firstMessageId, "version", "5.38"));
                    }
                }
                request.setRequestListener(new VKRequest.VKRequestListener() {
                    public boolean isFirstLoading = false;

                    @Override
                    public void onComplete(final VKResponse response) {
                        super.onComplete(response);

                        L.d("VKResponse response = " + response.responseString);
                        if(messageAdapter != null){
                            L.d("messageAdapter.getMessages().size() = " + messageAdapter.getMessages().size());
                        }

                        try {
                            isCanLoading = false;
                            tsbHistory.progressComplete();
                            JSONObject responseObject = response.json.getJSONObject("response");
                            List<Map<String, Object>> items = (List<Map<String, Object>>) JsonUtils.jsonToMap(responseObject).get("items");
                            Collections.reverse(items);
                            countMessages = responseObject.getInt("count");

                            if(items.size() != 0){
                                lastMessageId = (int) items.get(items.size()-1).get("id");
                                firstMessageId = (int) items.get(0).get("id");
                            }

                            if(messageAdapter == null){
                                messageAdapter = new MessageAdapter(getActivity(), items, chatId != null);
                                messageListLV.setAdapter(messageAdapter);

                                isFirstLoading = true;
                            }else{
                                messageAdapter.addInStartMessages(items);
                            }

                            messageAdapter.setOnLoadListener(new MessageAdapter.OnLoadListener() {
                                @Override
                                public void onLoadContent() {
                                    contentLoaded();
                                    expandedAllGroup(messageAdapter);
                                    messageListLV.setSelectedChild(messageAdapter.getSaveGroupPosition(), messageAdapter.getSaveChildPosition(), true);
                                    L.d("messageAdapter listener");
                                    if(!isFirstLoading){
                                        L.d("messageAdapter listener isCanLoading");
                                        isCanLoading = true;
                                    }else{
                                        messageListLV.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                // Select the last row so it will scroll into view...
                                                messageListLV.setSelectedChild(messageAdapter.getGroupCount() - 1, messageAdapter.getChildrenCount(messageAdapter.getGroupCount() - 1) - 1, true);
                                                contentLoaded();
                                                isCanLoading = true;
                                            }
                                        });
                                    }
                                }
                            });

                            loadChatInfo();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(VKError error) {
                        L.d("error.apiError errorCode = " + error.errorCode + " errorMessage = " + error.errorMessage + " errorReason = " + error.errorReason);
                    }
                });
                request.start();
            }
        });
    }

    private TimerSearchBreaker tsbOnline;
    private void initTSBOnline() {
        tsbOnline = new TimerSearchBreaker(getActivity(), new TimerSearchBreaker.ISearchTask() {
            @Override
            public void searchUpdate(String ts) {
                VKRequest request = new VKRequest("messages.getLongPollHistory", VKParameters.from("ts", ts, "pts", LongPollServer.pts));
                request.setRequestListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(final VKResponse response) {
                        super.onComplete(response);
                        L.d("VKResponse response = " + response.responseString);
                        try {
                            JSONObject responseObject = response.json.getJSONObject("response");
                            LongPollServer.pts = responseObject.getString("new_pts");
                            List<Map<String, Object>> items = (List<Map<String, Object>>) JsonUtils.jsonToMap(responseObject.getJSONObject("messages")).get("items");
                            List<Map<String, Object>> filterItems = new ArrayList<>();
                            for(Map<String, Object> item:items){
                                if((int)item.get("out") == 0){
                                    item.put("from_id",item.get("user_id"));
                                }else{
                                    item.put("from_id", Integer.parseInt(AppPreferences.getMyId(getApplicationContext())));
                                }
                                if(dialogId >= 2000000000){
                                    L.d("text talk id = " + item.get("id"));
                                    if(item.get("chat_id") == null){
                                        continue;
                                    }else{
                                        if(dialogId != 2000000000 + (int)item.get("chat_id")){
                                            continue;
                                        }
                                        if((int)item.get("id") > lastMessageId){

                                            filterItems.add(item);
                                            lastMessageId = (int)item.get("id");
                                        }
                                    }
                                }else{
                                    if(item.get("user_id") == null){
                                        continue;
                                    }else{
                                        if(dialogId != (int)item.get("user_id")){
                                            continue;
                                        }
                                        if((int)item.get("id") > lastMessageId){

                                            filterItems.add(item);
                                            lastMessageId = (int)item.get("id");
                                        }
                                    }
                                }
                            }


                            L.d("messageAdapter = " + messageAdapter + " filterItems size = " + filterItems.size());
                            if(messageAdapter == null){
                                messageAdapter = new MessageAdapter(getActivity(), filterItems, chatId != null);
                                messageListLV.setAdapter(messageAdapter);
                            }else{
                                messageAdapter.addMessages(filterItems);
                            }
                            loadChatInfo();
                            contentLoaded();
                            scrollMyListViewToBottom();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(VKError error) {
                        L.d("error.apiError errorCode = " + error.errorCode + " errorMessage = " + error.errorMessage + " errorReason = " + error.errorReason);
                    }
                });
                request.start();
            }
        });
    }
    private void addNewMessage(String ts, final int messageID){
        L.d("addNewMessage ts = " + ts);
        tsbOnline.runNow(ts);
    }

    private void setupSendUI(){

        messageET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                scrollMyListViewToBottom();
                return false;
            }
        });
        sendBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
    }

    private Float getCoinsYPos ;
    private void sendMessage(){
        final String message = messageET.getText().toString();
        if (messageET.getText().toString().isEmpty() && pickAttachments.getAttachmentsToSend().isEmpty() && pickAttachments.getAttachToMessagePlace() == null) {
            return;
        }

        L.d("attachmentsAdapter.getAttachmentsToSend() = " + pickAttachments.getAttachmentsToSend());

        VKParameters vkParameters = null ;
        if (userId != null) {
            vkParameters = VKParameters.from("message", messageET.getText().toString(), "user_id", userId, "attachment", pickAttachments.getAttachmentsToSend());
        } else {
            int peer_id = (2000000000 + Integer.parseInt(chatId));
            L.d("peer_id = " + peer_id);
            vkParameters = VKParameters.from("message", messageET.getText().toString(), "peer_id", peer_id, "version", "5.38", "attachment", pickAttachments.getAttachmentsToSend());
        }
        if(pickAttachments.getAttachToMessagePlace() != null){
            vkParameters.put("lat",pickAttachments.getAttachToMessagePlace().latitude);
            vkParameters.put("long",pickAttachments.getAttachToMessagePlace().longitude);
            pickAttachments.clearAttachToMessagePlace();
        }
        markAsRead();
        final VKRequest request = new VKRequest("messages.send", vkParameters);
        request.setRequestListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(final VKResponse response) {
                super.onComplete(response);
                messageET.setText("");
                pickAttachments.clearAll();
            }

            @Override
            public void onError(VKError error) {
                L.d("error.apiError errorCode = " + error);
                try {
                    if(error.apiError != null && error.apiError.errorCode == 6){
                        request.repeat();
                    }
                }catch (Exception e){

                }
            }
        });
        request.start();

        Boolean containMat = false;
        String[] matWords = {"бляд", "пиздец", "блять" , "блядина","бля", " хуй ", "хуйня", "хуйня", "хуюшки", "хуев", "пизда", " еб", "ебал", " ебать ", " нахуй "};
        for(String word:matWords){
            if((" " + message.toLowerCase() + " ").contains(word)){
                containMat = true;
            }
        }
        if(containMat){
            addBonus(-2);
        }else{
            UILApplication.getApi().checkTextOnlyMistakes(message, "7").enqueue(new Callback<ArrayList<MistakesModel>>() {
                @Override
                public void onResponse(Call<ArrayList<MistakesModel>> call, Response<ArrayList<MistakesModel>> response) {
                    L.d("checkText response = " + response);
                    L.d("checkText response.body() = " + response.body());
                    Boolean contaimMistake = false;
                    for(MistakesModel mistake:response.body()){
                        if(mistake.getS() != null && mistake.getS().length != 0){
                            contaimMistake = true;
                            break;
                        }
                    }
                    if(!contaimMistake){
                        if(message.toLowerCase().contains(Define.WORD_DAY)){
                            addBonus(5);
                        }else{
                            addBonus(1);
                        }
                    }else{
                        addBonus(-1);
                    }

                    if (response.body() != null) {

                    } else {
                        Toast.makeText(UILApplication.context, "Проверьте интернет соединение", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<MistakesModel>> call, Throwable t) {

                }
            });
        }

    }

    private void addBonus(Integer countCoins){
        if(countCoins > 0){
            setBackgroundInView(getCoinsTV, getResources().getDrawable(R.drawable.get_coins));
        }else{
            setBackgroundInView(getCoinsTV, getResources().getDrawable(R.drawable.lose_coins));
        }
        if(countCoins == -1){
            getCoinsTV.setText("- 1 очко (ошибки)");
        }

        if(countCoins == -2){
            getCoinsTV.setText("- 2 очка (за мат)");
        }

        if(countCoins == 1){
            getCoinsTV.setText("+1 очко (без ошибок)");
        }

        if(countCoins == 2){
            getCoinsTV.setText("+2 очкa");
        }
        if(countCoins == 5){
            getCoinsTV.setText("+5 очков (слово дня)");
        }

        if(getCoinsYPos == null){
            getCoinsYPos = getCoinsTV.getY();
        }

        getCoinsTV.setVisibility(View.VISIBLE);
        getCoinsTV.animate().alpha(0.0f).setDuration(1200);
        getCoinsTV.animate().y(getCoinsYPos - 150).setDuration(1200);
        Handler handler = new Handler(getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getCoinsTV.setVisibility(View.INVISIBLE);
                getCoinsTV.animate().alpha(1.0f).setDuration(1);
                getCoinsTV.animate().y(getCoinsYPos).setDuration(1);
            }
        },2000);
        AppPreferences.addCountCoins(getApplicationContext(),countCoins);
    }
    private void setBackgroundInView(View view, Drawable drawable) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(drawable);
        } else {
            view.setBackground(drawable);
        }
    }

    private void loadChatInfo(){
        if (userId != null) {
            messageAdapter.setSingleUser(userName, userAvatar);
        } else {
            messageAdapter.loadChatActiveUsers(chatsActiveUids);
        }
    }

    private void markAsRead(){
        VKRequest request;
        if (userId != null) {
            request = new VKRequest("messages.markAsRead", VKParameters.from("peer_id", userId));
        } else {
            int peer_id = (2000000000 + Integer.parseInt(chatId));
            L.d("peer_id = " + peer_id);
            request = new VKRequest("messages.markAsRead", VKParameters.from("peer_id", peer_id));
        }

        request.setRequestListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(final VKResponse response) {
                super.onComplete(response);

            }

            @Override
            public void onError(VKError error) {

            }
        });
        request.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        longPollServer.removeObserver(longPollObserver);
    }

    private void collapsedAllGroup(MessageAdapter adapter){
        for(int i = 0; i < adapter.getGroupCount(); i++){
            messageListLV.collapseGroup(i);
        }
    }

    private void expandedAllGroup(MessageAdapter adapter){
        L.d("historyLV = " + messageListLV.getExpandableListAdapter().getGroupCount() + " adapter = " + adapter.getGroupCount());
        if(messageListLV == null){
            return;
        }
        for(int i = 0; i < adapter.getGroupCount(); i++){
            messageListLV.expandGroup(i);
        }
    }
    private Boolean isCanLoading = false;
    private void scrollMyListViewToBottom() {
        Handler handler = new Handler(getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                messageListLV.post(new Runnable() {
                    @Override
                    public void run() {
                        // Select the last row so it will scroll into view...
                        messageListLV.setSelectedChild(messageAdapter.getGroupCount() - 1, messageAdapter.getChildrenCount(messageAdapter.getGroupCount() - 1) - 1, true);
                        contentLoaded();
                        isCanLoading = true;
                    }
                });
            }
        }, 500);
    }

    private void hideSoftKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    public void disableSoftInputFromAppearing(EditText editText) {
        if (Build.VERSION.SDK_INT >= 11) {
            editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
            editText.setTextIsSelectable(true);
        } else {
            editText.setRawInputType(InputType.TYPE_NULL);
            editText.setFocusable(true);
        }
    }
    public void enableSoftInputFromAppearing(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_chat, menu);

        return true;
    }

    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.book:
                Intent intent = new Intent(this, MainActivity2.class);
                intent.putExtra(MainActivity2.TEXT, messageET.getText().toString());
                startActivityForResult(intent,1);
                return true;
            case R.id.flag:
                final StringBuilder allMessages = new StringBuilder();
                for(Map<String,Object> message:messageAdapter.getMessages()){
                    if(message.get("body") != null){
                        message.put("pos_start", allMessages.length());

                        allMessages.append(message.get("body").toString() + " ");

                        message.put("pos_end", allMessages.length());
                        L.d("allMessages slice = " + allMessages.substring((int)message.get("pos_start"), (int)message.get("pos_end")) + " message = " + message.get("body"));
                    }else{
                        L.d("Body = null");
                    }
                }

                UILApplication.getApi().checkTextOnlyMistakes(allMessages.toString(), "7").enqueue(new Callback<ArrayList<MistakesModel>>() {
                    @Override
                    public void onResponse(Call<ArrayList<MistakesModel>> call, Response<ArrayList<MistakesModel>> response) {
                        L.d("checkText response = " + response);
                        L.d("checkText response.body() = " + response.body());
                        for(MistakesModel mistake:response.body()){
                            if(mistake.getS() != null && mistake.getS().length != 0){
                                for(Map<String, Object> message:messageAdapter.getMessages()){
                                    if(message.get("body").toString().contains(mistake.getWord())){
                                        L.d("checkText mistake detect = " + mistake.getPos() + " mistake.getWord() = " + mistake.getWord());
                                        message.put("mistake", true);
                                    }
                                }
                            }
                        }
                        messageAdapter.notifyDataSetChanged();

                        if (response.body() != null) {

                        } else {
                            Toast.makeText(UILApplication.context, "Проверьте интернет соединение", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ArrayList<MistakesModel>> call, Throwable t) {

                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!messageET.getText().toString().isEmpty()){
            Handler handler = new Handler(getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    enableSoftInputFromAppearing(messageET);
                }
            }, 300);

        }
    }
}
