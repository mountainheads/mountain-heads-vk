package com.mountain.orthius.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mountain.orthius.R;

import java.util.ArrayList;
import java.util.Map;


public class PVkNoteAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Map<String,Object>> audioList;
    private Integer widthScreen = 0;
    private Integer numColumns = 1;


    public PVkNoteAdapter(Context context, ArrayList<Map<String, Object>> audioList) {
        ctx = context;
        this.audioList = audioList;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return audioList.size();
    }

    @Override
    public Map<String,Object> getItem(int position) {
        return audioList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item_note, parent,
                    false);
        }

        final Map<String,Object> audio = getItem(position);

        TextView titleTV = (TextView) view.findViewById(R.id.titleTV);



        if(audio.get("title") != null) {
            titleTV.setText(audio.get("title").toString());
        }

        return view;
    }
}

