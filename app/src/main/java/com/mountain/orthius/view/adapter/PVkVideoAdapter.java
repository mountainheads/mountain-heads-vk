package com.mountain.orthius.view.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


public class PVkVideoAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    List<Map<String,Object>> videoList;
    private Integer widthItem;
    private Integer numColumns = 1;


    public PVkVideoAdapter(Context context, ArrayList<Map<String,Object>> videoList) {
        ctx = context;
        this.videoList = videoList;
        Collections.reverse(this.videoList);
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public PVkVideoAdapter(Context context, List<Map<String,Object>> videoList) {
        ctx = context;
        this.videoList = videoList;
        Collections.reverse(this.videoList);
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return videoList.size();
    }

    @Override
    public Map<String,Object> getItem(int position) {
        return videoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null){
            convertView = lInflater.inflate(R.layout.item_video, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imageView1 = (ImageView) convertView.findViewById(R.id.imageView1);
            viewHolder.titleTV = (TextView) convertView.findViewById(R.id.titleTV);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.imageView1.getLayoutParams().height = (int)(widthItem/1.5);


        final Map<String,Object> video = getItem(position);
        L.d("video = " + video);
        String link = null;
        if(video.get("photo_640") != null){
            link = video.get("photo_640").toString();
        }else if(video.get("photo_320") != null){
            link = video.get("photo_320").toString();
        }

        if(link != null && !link.isEmpty()){
            L.d("link = " + link);
            Glide.with(ctx).load(link).centerCrop()
                    .placeholder(new ColorDrawable(ctx.getResources().getColor(R.color.blue_dark_empty_photo)))
                    .crossFade().into(viewHolder.imageView1);
        }

        if(video.get("title") != null){
            viewHolder.titleTV.setText(video.get("title").toString());
        }


        return convertView;
    }


    public Integer getNumColumns() {
        return numColumns;
    }

    public void setNumColumns(Integer numColumns) {
        this.numColumns = numColumns;
    }

    static class ViewHolder {
        public ImageView imageView1;
        public TextView titleTV;
    }

    public void setWidthItem(Integer widthItem) {
        this.widthItem = widthItem;
    }
}

