package com.mountain.orthius.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;

import java.util.ArrayList;
import java.util.Map;


public class PVkPollAnswerAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Map<String,Object>> docList;
    private Integer widthScreen = 0;
    private Integer numColumns = 1;


    public PVkPollAnswerAdapter(Context context, ArrayList<Map<String, Object>> docList) {
        ctx = context;
        this.docList = docList;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return docList.size();
    }

    @Override
    public Map<String,Object> getItem(int position) {
        return docList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null){
            convertView = lInflater.inflate(R.layout.item_poll_answer, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.titleTV = (TextView) convertView.findViewById(R.id.titleTV);
            viewHolder.digitTV = (TextView) convertView.findViewById(R.id.digitTV);
            viewHolder.percentLL = (LinearLayout) convertView.findViewById(R.id.percentLL);
            viewHolder.backPercentLL = (LinearLayout) convertView.findViewById(R.id.backPercentLL);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Map<String, Object> answer = getItem(position);

        Integer votes = (int)answer.get("votes");
        final Double rate = (Double) answer.get("rate");

        if (answer.get("text") != null) {
            viewHolder.titleTV.setText(answer.get("text").toString());
        }

        viewHolder.digitTV.setText(rate + "% (" + votes + ")");


        Display display = ((Activity)ctx).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int widthDisplay = size.x;
        int heightDisplay = size.y;

        viewHolder.backPercentLL.setLayoutParams(new RelativeLayout.LayoutParams(widthDisplay, Math.round(convertDpToPixel(24, ctx))));


        if(rate == 100){
            viewHolder.percentLL.setLayoutParams(new RelativeLayout.LayoutParams(widthDisplay, Math.round(convertDpToPixel(24, ctx))));
        }else{
            L.d("width rect poll" + (int)(widthDisplay / 100 * rate) + " layoutWidth = " + widthDisplay + " rate = " + rate);
            viewHolder.percentLL.setLayoutParams(new RelativeLayout.LayoutParams((int)(widthDisplay / 100 * rate), Math.round(convertDpToPixel(24, ctx))));
        }

        return convertView;
    }
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    static class ViewHolder {
        public TextView titleTV;
        public TextView digitTV;
        public LinearLayout percentLL;
        public LinearLayout backPercentLL;
    }
}