package com.mountain.orthius.view.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;

import com.mountain.orthius.view.adapter.base.MediaVKBaseAdapter;

import java.util.List;
import java.util.Map;

/**
 * Created by alexeybalabanov on 20.10.14.
 */
public class MessageResendAdapter extends MediaVKBaseAdapter {
    private final MessageAdapter messageAdapter;

    private Map<String,Map<String,Object>> users;

    public MessageResendAdapter(Activity context, List<Map<String, Object>> messages, boolean isMultiChat) {
        messageAdapter = new MessageAdapter(context, messages, isMultiChat);
        messageAdapter.setMessageResendAdapter(this);
    }


    @Override
    public int getCount() {
        return messageAdapter.getMessages().size();
    }

    @Override
    public Map<String, Object> getItem(int position) {
        return messageAdapter.getMessages().get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return messageAdapter.getChildView(-1,position,false,convertView,parent);
    }

    public void loadChatActiveUsers(String chatsActiveUids) {
        messageAdapter.loadChatActiveUsers(chatsActiveUids);
    }

    public void setParentAdapter(MessageAdapter parentAdapter) {
        messageAdapter.setParentAdapter(parentAdapter);
    }

    public Map<String, Map<String, Object>> getUsers() {
        return users;
    }

    public void setUsers(Map<String, Map<String, Object>> users) {
        this.users = users;
    }
}

