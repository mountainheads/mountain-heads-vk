package com.mountain.orthius.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import com.mountain.orthius.R;
import com.mountain.orthius.utils.MyHtmlTagHandler;
import com.mountain.orthius.view.activity.base.BaseActivity;

public class SpeakTrueScreen extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speak_true_screen);
        enableHomeUpBack();

        TextView speakTrueTV = (TextView) findViewById(R.id.speakTrueTV);
        String sourceString2 = "<font color='#f27a94'>Одеть</font> Надежду – <font color='#f27a94'>надеть</font> одежду. <br><font color='#f27a94'>Одеть</font> Наташку – <font color='#f27a94'>надеть</font> рубашку. <br>Т.е <font color='#f27a94'>одеть</font> кого-то, надеть что-то.";
        speakTrueTV.setText(Html.fromHtml(sourceString2), TextView.BufferType.SPANNABLE);

        TextView fontUniq3TV2 = (TextView) findViewById(R.id.fontUniq3TV2);
        String sourceString3 = "Ко<font color='#f27a94'>м</font>форка, бу<font color='#f27a94'>х</font>галтер";
        fontUniq3TV2.setText(Html.fromHtml(sourceString3), TextView.BufferType.SPANNABLE);

        TextView fontUniq3TV3 = (TextView) findViewById(R.id.fontUniq3TV3);
        String sourceString4 = "<font color='#f27a94'><strike>конфорка, бугалтер</font>";
        fontUniq3TV3.setText(Html.fromHtml(sourceString4, null, new MyHtmlTagHandler()));
    }
}
