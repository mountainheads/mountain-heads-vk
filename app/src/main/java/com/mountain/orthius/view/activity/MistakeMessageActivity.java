package com.mountain.orthius.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.mountain.orthius.R;
import com.mountain.orthius.utils.SpellerManager;
import com.mountain.orthius.utils.view.ColorTextView;
import com.mountain.orthius.view.activity.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MistakeMessageActivity extends BaseActivity {
    @BindView(R.id.mistakesTextTV)
    ColorTextView mistakesTextTV;
    @BindView(R.id.rightWordTV)
    TextView rightWordTV;
    @BindView(R.id.variantsTV)
    TextView variantsTV;
    @BindView(R.id.myWordET)
    EditText myWordET;

    @BindView(R.id.nextWordBTN)
    Button nextWordBTN;
    @BindView(R.id.prevWordBTN)
    Button prevWordBTN;

    @BindView(R.id.variantsLL)
    LinearLayout variantsLL;
    @BindView(R.id.textSV)
    ScrollView textSV;
    private SpellerManager spellerManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mistake_message);

        enableHomeUpBack();


        String text = "Пришло теплае лето. На лисной опушки распускаюца колоколчики, незабутки, шыповник. Белые ромашки пратягивают к сонцу свои нежные лепески. Вылитают из уютных гнёзд птинцы. У зверей взраслеет смена. Мидвежата старше всех. Они радились еще холодной зимой в берлоги. Теперь они послушно следуют за строгай матерью. Рыжые лесята весело играют у нары. А кто мелькает в сасновых ветках? Да это лофкие бельчята совершают свои первые высотные прышки. В сумерках выходят на охоту колючии ежата." +
                "Не обижайте лесных малышей. Приходите в лес верными друзями. Унас под крыльцом живут ежы. По вечерам вся семья выходит гулять. Взрослые ежи роют землю маленькими лапами. Они достают корешки и едят. Маленкие ежата в это время играют, резвяца.\n" +
                "Аднажды к старому ежу подбежала сабака. Ёж свернулся вклубок и замер. Собака осторожно покатила ежа кпруду. Ёш плюхнулся в воду и поплыл. Я прогнал сабаку. На следующюю весну остался под крыльцом один старый ёжек. Куда девались остальные? Они переселились в другое место. Старый ёж незахотел пакинуть мой дом.";
        text = getIntent().getStringExtra(MainActivity2.TEXT);
        ButterKnife.bind(this);
        spellerManager = new SpellerManager(text, mistakesTextTV, rightWordTV, myWordET, variantsTV, variantsLL, false);

        spellerManager.checkText();

        nextWordBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spellerManager.checkNextWordNoFix();
            }
        });

        prevWordBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spellerManager.checkPrevWordNoFix();
            }
        });

    }

}
