package com.mountain.orthius.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mountain.orthius.R;

import java.util.ArrayList;
import java.util.Map;



public class PVkAudioAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Map<String,Object>> audioList;
    private Integer widthScreen = 0;
    private Integer numColumns = 1;


    public PVkAudioAdapter(Context context, ArrayList<Map<String, Object>> audioList) {
        ctx = context;
        this.audioList = audioList;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return audioList.size();
    }

    @Override
    public Map<String,Object> getItem(int position) {
        return audioList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null){
            convertView = lInflater.inflate(R.layout.item_audio, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.authorTV = (TextView) convertView.findViewById(R.id.authorTV);
            viewHolder.nameTrackTV = (TextView) convertView.findViewById(R.id.nameTrackTV);
            viewHolder.durationTV = (TextView) convertView.findViewById(R.id.durationTV);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Map<String,Object> audio = getItem(position);

        if(audio.get("artist") != null){
            viewHolder.authorTV.setText(audio.get("artist").toString());
        }
        if(audio.get("title") != null){
            viewHolder.nameTrackTV.setText(audio.get("title").toString());
        }
        if(audio.get("duration") != null) {
            Integer minutes = (int) Math.abs((int)audio.get("duration") / (double) 60);
            Integer seconds = (int) ((int)audio.get("duration") % (double) 60);

            String minutesText = minutes < 10 ? "0" + minutes : "" + minutes;
            String secondsText = seconds < 10 ? "0" + seconds : "" + seconds;
            viewHolder.durationTV.setText(minutesText + ":" + secondsText);
        }

        return convertView;
    }

    static class ViewHolder {

        public TextView durationTV;
        public TextView authorTV;
        public TextView nameTrackTV;
    }
}

