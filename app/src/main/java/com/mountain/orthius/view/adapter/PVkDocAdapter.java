package com.mountain.orthius.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mountain.orthius.R;

import java.util.ArrayList;
import java.util.Map;


public class PVkDocAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Map<String,Object>> docList;
    private Integer widthScreen = 0;
    private Integer numColumns = 1;


    public PVkDocAdapter(Context context, ArrayList<Map<String, Object>> docList) {
        ctx = context;
        this.docList = docList;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return docList.size();
    }

    @Override
    public Map<String,Object> getItem(int position) {
        return docList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null){
            convertView = lInflater.inflate(R.layout.item_doc, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.titleTV = (TextView) convertView.findViewById(R.id.titleTV);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Map<String,Object> doc = getItem(position);


        if(doc.get("title") != null) {
            viewHolder.titleTV.setText(doc.get("title").toString());
        }

        return convertView;
    }
    static class ViewHolder {

        public TextView titleTV;
    }
}

