package com.mountain.orthius.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.linearlistview.LinearListView;
import com.makeramen.RoundedImageView;
import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.preferences.AppPreferences;
import com.mountain.orthius.utils.DateHandler;
import com.mountain.orthius.utils.JsonUtils;
import com.mountain.orthius.utils.TimerSearchBreaker;
import com.mountain.orthius.utils.image.LoaderTalkPhoto;
import com.mountain.orthius.view.activity.MainActivity2;
import com.mountain.orthius.view.activity.MistakeMessageActivity;
import com.mountain.orthius.view.adapter.base.MediaVKBaseExpandableAdapter;
import com.mountain.orthius.view.adapter.base.MediaVKContent;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * Created by alexeybalabanov on 20.10.14.
 */
public class MessageAdapter extends MediaVKBaseExpandableAdapter {


    private MessageResendAdapter messageResendAdapter;
    private int countMessagesToStart;
    private int saveGroupPosition;
    private int saveChildPosition;

    public interface OnLoadListener{
        public void onLoadContent();
    }

    private Activity act;
    private LayoutInflater lInflater;

    private List<Map<String, Object>> messages;
    private List<MessageInDay> messageInDayList = new ArrayList<>();

    private String userName;
    private String userAvatar;

    private Map<String,Map<String,Object>> users;
    private Map<String,Map<String,Object>> groups = new HashMap<>();
    private String chatsActiveUids = "";
    private String groupsInfo = "";
    private MessageAdapter parentAdapter;
    private Boolean isMultiChat;

    public MessageAdapter(Activity context, List<Map<String, Object>> messages, boolean isMultiChat) {
        this.isMultiChat = isMultiChat;
        act = context;
        this.messages = messages;
        if(act != null){
            lInflater = (LayoutInflater) act
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        initLoaderUser();
        initLoaderGroups();
        sortedMessages();
    }

    @Override
    public Map<String, Object> getChild(int groupPosition, int childPosition) {
        return messageInDayList.get(groupPosition).getMessage(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    public void sortedMessages() {
        AsyncTask databaseTask = new AsyncTask<Object, List<MessageInDay>, List<MessageInDay>>() {
            @Override
            protected List<MessageInDay> doInBackground(Object... params) {
                List<MessageInDay> historyInDayList = convertToPairHistoryInDay(messages);
                return historyInDayList;
            } //протетсить догрузку старых сообщений и общение в реальном врмени внутри диалога

            @Override
            protected void onPostExecute(List<MessageInDay> messageList) {
                super.onPostExecute(messageList);
                messageInDayList = messageList;
                notifyDataSetChanged();
                if(onLoadListener != null){
                    onLoadListener.onLoadContent();
                }
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            databaseTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            databaseTask.execute();
        }
    }

    private List<MessageInDay> convertToPairHistoryInDay(List<Map<String, Object>> messages){
        //Collections.reverse(mHistoryList);
        List<MessageInDay> historyList = new ArrayList<MessageInDay>();
        MessageInDay pairMessageInDay = null;

        Map<String, Object> earlyMessage = null;

        int i = 0;


        for(Map<String, Object> message:messages){

            Long currentDate = Long.valueOf((int)message.get("date")) * 1000;
            Long earlyDate = null;
            if(earlyMessage != null){
                earlyDate = Long.valueOf((int)earlyMessage.get("date")) * 1000;
            }

            if(historyList.isEmpty() || !DateHandler.convertOnlyDateToString(new Date(currentDate)).equals(DateHandler.convertOnlyDateToString(new Date(earlyDate)))){
                pairMessageInDay = new MessageInDay();
                pairMessageInDay.setDate(DateHandler.convertDateToString(new Date(currentDate)));
                pairMessageInDay.addMessage(message);
                historyList.add(pairMessageInDay);
            }else{
                historyList.get(historyList.size()-1).addMessage(message);
            }
            if(i == countMessagesToStart){
                saveGroupPosition = historyList.size()-1;
                saveChildPosition = pairMessageInDay.size()-1;
            }
            earlyMessage = message;
            i++;
        }
        return historyList;
    }


    public class MessageInDay {
        private List<Map<String, Object>> messages = new ArrayList<>();
        private String date;

        public Map<String, Object> getMessage(int position) {
            return messages.get(position);
        }

        public void addMessage(Map<String, Object> message) {
            messages.add(message);
        }
        public int size() {
            return messages.size();
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public List<Map<String, Object>> getMessages() {
            return messages;
        }
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        if(messageInDayList == null || messageInDayList.size() == 0){
            return 0;
        }
        return messageInDayList.get(groupPosition).size();
    }

    @Override
    public String getGroup(int groupPosition) {
        try {
            return DateHandler.getDateToJournal(messageInDayList.get(groupPosition).getDate());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public int getGroupCount() {
        return messageInDayList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        LinearLayout layout = (LinearLayout) convertView;
        if (layout == null)
            layout = (LinearLayout) lInflater.inflate(R.layout.item_text_group, null);
        TextView labelTv = (TextView) layout.findViewById(R.id.labelTv);
        labelTv.setText(getGroup(groupPosition));
        return layout;
    }

    @Override
    public View getChildView(int groupPosition, int position, boolean isLastChild, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null){
            if(groupPosition == -1){
                convertView = lInflater.inflate(R.layout.item_message_resend, parent, false);
            }else{
                convertView = lInflater.inflate(R.layout.item_message, parent, false);
            }
            viewHolder = new ViewHolder();
            viewHolder.avatarIV = (RoundedImageView) convertView.findViewById(R.id.avatarIV);
            viewHolder.textTV = (TextView) convertView.findViewById(R.id.textTV);
            viewHolder.timeTV = (TextView) convertView.findViewById(R.id.timeTV);
            viewHolder.userNameTV = (TextView) convertView.findViewById(R.id.userNameTV);
            viewHolder.contentLL = (LinearLayout) convertView.findViewById(R.id.contentLL);
            viewHolder.messageLL = (LinearLayout) convertView.findViewById(R.id.messageLL);
            viewHolder.resendMessagesLL = (LinearLayout) convertView.findViewById(R.id.resendMessagesLL);
            viewHolder.resendMessagesLV = (LinearListView) convertView.findViewById(R.id.resendMessagesLV);
            viewHolder.repostLL = (LinearLayout) convertView.findViewById(R.id.repostLL);
            viewHolder.stateMessageIV = (ImageView) convertView.findViewById(R.id.stateMessageIV);
            initBaseFields(viewHolder, convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }



        final Map<String, Object> message;

        setupView(viewHolder);
        if(groupPosition == -1){// пересланные сообщения
            message = messages.get(position);
            setupResendMessages(viewHolder,message);
        }else{
            message = getChild(groupPosition, position);
            setupMessages(viewHolder, message);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(message.get("mistake") != null && (boolean) message.get("mistake")){
                    Intent intent = new Intent(act, MistakeMessageActivity.class);
                    intent.putExtra(MainActivity2.TEXT, message.get("body").toString());
                    act.startActivity(intent);
                }
            }
        });

        initDate(viewHolder.timeTV, message);
        initStateMessage(viewHolder.stateMessageIV, message);



        initResendMessage(viewHolder,message);
        setupFields(act, null, message, viewHolder, MediaVKContent.ContentType.MESSAGES);

        setupExitOrEnterUser(viewHolder, message);
        initRepost(message, viewHolder);
        //Picasso.with(ctx).load(note.getElementsByClass("search_item_img").attr("src")).placeholder(R.drawable.abc_ab_bottom_solid_dark_holo).resize(220, 220).centerCrop().into(avatarIV);
        return convertView;
    }

    private void setupView(ViewHolder viewHolder) {
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.setMargins(0,0,0,0); // llp.setMargins(left, top, right, bottom);
        viewHolder.textTV.setLayoutParams(llp);
        viewHolder.userNameTV.setVisibility(View.GONE);
    }

    private void setupExitOrEnterUser(ViewHolder viewHolder, Map<String, Object> message) {
        if(message.get("action") != null && message.get("action").toString().equals("chat_kick_user")){
            viewHolder.textTV.setVisibility(View.VISIBLE);
            viewHolder.textTV.setText(R.string.message_adapter_user_gone);
        }
        if(message.get("action") != null && message.get("action").toString().equals("chat_create")){
            viewHolder.textTV.setVisibility(View.VISIBLE);
            viewHolder.textTV.setText(R.string.message_adapter_user_create);
        }
    }

    private void setupMessages(ViewHolder viewHolder, Map<String, Object> message) {
        Integer fromId = (int) message.get("from_id");
        if (fromId == Integer.parseInt(AppPreferences.getMyId(act))) {
            // мое сообщение
            viewHolder.avatarIV.setVisibility(View.GONE);
            viewHolder.contentLL.setGravity(Gravity.RIGHT);


            viewHolder.messageLL.setBackgroundResource(R.drawable.bubble_right);
            if(message.get("mistake") != null && (boolean) message.get("mistake")){
                viewHolder.messageLL.setBackgroundResource(R.drawable.bubble_right_mistake);
            }

            LinearLayout.LayoutParams newLlp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            newLlp.setMargins(convertDpToPixel(32,act), 0, 0, 0); // llp.setMargins(left, top, right, bottom);
            viewHolder.messageLL.setLayoutParams(newLlp);
        } else {
            // чужое
            LinearLayout.LayoutParams newLlp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            if(isMultiChat){
                initAvatar(viewHolder,fromId);
                newLlp.setMargins(0, 0, 0, 0); // llp.setMargins(left, top, right, bottom);
            } else {
                viewHolder.avatarIV.setVisibility(View.GONE);
                newLlp.setMargins(0, 0, convertDpToPixel(32, act), 0); // llp.setMargins(left, top, right, bottom);
            }

            viewHolder.messageLL.setLayoutParams(newLlp);

            viewHolder.messageLL.setBackgroundResource(R.drawable.bubble_left);
            if(message.get("mistake") != null && (boolean) message.get("mistake")){
                viewHolder.messageLL.setBackgroundResource(R.drawable.bubble_left_mistake);
            }
            viewHolder.contentLL.setGravity(Gravity.LEFT);

        }
    }

    private void setupResendMessages(ViewHolder viewHolder,Map<String, Object> message) {
        viewHolder.messageLL.setBackgroundColor(Color.TRANSPARENT);
        viewHolder.contentLL.setBackgroundColor(Color.TRANSPARENT);
        initAvatar(viewHolder, (int) message.get("user_id"));
        LinearLayout.LayoutParams newLlp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        newLlp.setMargins(0, 0, 0, 0); // llp.setMargins(left, top, right, bottom);
        viewHolder.messageLL.setLayoutParams(newLlp);
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private void initAvatar(ViewHolder viewHolder, Integer fromId) {
        viewHolder.userNameTV.setVisibility(View.GONE);

        viewHolder.avatarIV.setVisibility(View.VISIBLE);

        Map<String,Map<String,Object>> users;
        if(isResendMessages()){
            users = parentAdapter.getUsers();
        }else{
            users = this.users;
        }

        if (users != null) {
            Map<String, Object> user = users.get("" + fromId);
            L.d("text resend fromId = " + fromId);
            L.d("text resend user = " + user);
            if (user != null) {
                if (user.get("photo_50") != null) {
                    LoaderTalkPhoto.loadImage(act, user.get("photo_50").toString(), viewHolder.avatarIV);
                }
                if (user.get("first_name") != null) {
                    viewHolder.userNameTV.setVisibility(View.VISIBLE);
                    viewHolder.userNameTV.setText(user.get("first_name") + " " + user.get("last_name"));
                }
            } else {
                if (isResendMessages()) {
                    parentAdapter.loadChatActiveUser("" + fromId);
                }else{
                    loadChatActiveUser("" + fromId);
                }
            }
        } else {
            if (userAvatar != null) {
                //LoaderTalkPhoto.loadImage(act, userAvatar, avatarIV);
            }
            if (userName != null) {
            }
            if (!isResendMessages()) {
                viewHolder.avatarIV.setVisibility(View.GONE);
            }
        }
    }

    private void initResendMessage(ViewHolder viewHolder, Map<String, Object> message) {
        if(message.get("fwd_messages") == null){
            viewHolder.resendMessagesLL.setVisibility(View.GONE);
            return;
        }
        viewHolder.resendMessagesLL.setVisibility(View.VISIBLE);

        List<Map<String,Object>> resendMessages = (List<Map<String,Object>>) message.get("fwd_messages");

        MessageResendAdapter messageAdapter = new MessageResendAdapter(act,resendMessages, false);
        String uids = "";
        Boolean existNewUsers = false;
        for(Map<String,Object> resendMessage:resendMessages){
            uids += resendMessage.get("user_id") + ",";
            if(users == null || !users.containsKey(resendMessage.get("user_id").toString())){
                existNewUsers = true;
            }
        }
        if(existNewUsers){
            messageAdapter.loadChatActiveUsers(uids);
        }
        //messageAdapter.setListView(resendMessagesLV);
        messageAdapter.setParentAdapter(this);
        viewHolder.resendMessagesLV.setAdapter(messageAdapter);
    }

    private void initDate(TextView timeTV, Map<String,Object> message){

        Long date = Long.valueOf((int)message.get("date")) * 1000;
        DateHandler.getCHPUDurationBy(date);

        String outDate = DateHandler.getOnlyTime(new Date(date));
        timeTV.setText(outDate);
    }

    private void initRepost(Map<String, Object> message, ViewHolder viewHolder){
        viewHolder.repostLL.removeAllViews();
        if(message.get("attachments") != null && ((List<Map<String, Object>>)message.get("attachments")).get(0).get("type").equals("wall")){
            viewHolder.repostLL.setVisibility(View.VISIBLE);
            Map<String,Object> repost = (Map<String,Object>)((List<Map<String, Object>>)message.get("attachments")).get(0).get("wall");
            View repostView = lInflater.inflate(R.layout.item_post, viewHolder.repostLL);
            RoundedImageView avatarRepostIV = (RoundedImageView) repostView.findViewById(R.id.avatarIV);

            LinearLayout contentLL = (LinearLayout) repostView.findViewById(R.id.contentLL);
            contentLL.setPadding(convertDpToPixel(16, act), convertDpToPixel(0, act), convertDpToPixel(16, act), convertDpToPixel(16, act));

            TextView nameTV = (TextView) repostView.findViewById(R.id.nameTV);
            nameTV.setPadding(convertDpToPixel(16, act), convertDpToPixel(0, act), convertDpToPixel(16, act), convertDpToPixel(0, act));

            TextView createDateTV = (TextView) repostView.findViewById(R.id.createDateTV);
            try {
                String date = DateHandler.getFullDateToJournal(DateHandler.convertDateToString(new Date((int)repost.get("date") * 1000)));
                createDateTV.setText(date);
            } catch (Exception e) {
                //e.printStackTrace();
            }

            avatarRepostIV.setLayoutParams(new LinearLayout.LayoutParams(convertDpToPixel(40, act), convertDpToPixel(40, act)));

            RelativeLayout statsInfoRL = (RelativeLayout) repostView.findViewById(R.id.statsInfoRL);
            statsInfoRL.setVisibility(View.GONE);

            //repostLL.removeAllViews();
            //repostLL.addView(repostView);
            ViewHolder repostHolder = new ViewHolder();
            initBaseFields(repostHolder,repostView);
            setupFields(act, null, repost, repostHolder, MediaVKContent.ContentType.MESSAGES);

            String groupId = repost.get("from_id").toString().replace("-", "");

            Map<String,Map<String,Object>> groups;
            if(isResendMessages()){
                groups = parentAdapter.getGroups();
            }else{
                groups = this.groups;
            }

            if(groups != null){ // загрузка репоста в диалог
                if(groups.get(groupId) == null){
                    loadChatGroupInfo(groupId);
                }else{
                    nameTV.setText(groups.get(groupId).get("name").toString());
                    LoaderTalkPhoto.loadImage(act, groups.get(groupId).get("photo_50").toString(), avatarRepostIV);
                }
            }
        }else{
            viewHolder.repostLL.setVisibility(View.GONE);
        }
    }

    public void setSingleUser(String userName, String userAvatar) {
        this.userName = userName;
        this.userAvatar = userAvatar;
        notifyDataSetChanged();
    }

    TimerSearchBreaker tsbLoadUsers;

    public void loadChatActiveUser(String user) {
        this.chatsActiveUids += user+",";
        tsbLoadUsers.run(chatsActiveUids);
    }
    public void loadChatActiveUsers(String chatsActiveUids) {
        this.chatsActiveUids = chatsActiveUids;
        tsbLoadUsers.runNow(chatsActiveUids);
    }

    private void initLoaderUser(){
        final Handler handler = new Handler(Looper.getMainLooper());
        tsbLoadUsers = new TimerSearchBreaker(act, new TimerSearchBreaker.ISearchTask() {
            @Override
            public void searchUpdate(String query) {
                chatsActiveUids = "";
                VKRequest request = new VKRequest("users.get", VKParameters.from("user_ids", query,"fields","photo_50"));
                request.setRequestListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(final VKResponse response) {
                        super.onComplete(response);
                        L.d("VKResponse response = " + response.responseString);
                        try {
                            if(isResendMessages()){
                                if(parentAdapter.getUsers() == null){
                                    parentAdapter.setUsers(new HashMap<String,Map<String,Object>>());
                                }
                            }else{
                                if(MessageAdapter.this.users == null){
                                    MessageAdapter.this.users = new HashMap<>();
                                }
                            }

                            JSONObject responseObject = response.json;
                            Map<String, Object> responseMap = JsonUtils.jsonToMap(responseObject);
                            List<Map<String, Object>> users = (List<Map<String, Object>>) responseMap.get("response");

                            Map<String,Map<String,Object>> usersMap = new HashMap<>();
                            for(Map<String, Object> user:users){
                                usersMap.put(user.get("id").toString(),user);
                            }

                            Map<String,Map<String,Object>> allUsers;
                            if(isResendMessages()){
                                allUsers = parentAdapter.getUsers();
                            }else{
                                allUsers = MessageAdapter.this.users;
                            }
                            allUsers.putAll(usersMap);
                            notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(VKError error) {
                        L.d("error.apiError errorCode = " + error.errorCode + " errorMessage = " + error.errorMessage + " errorReason = " + error.errorReason);
                    }
                });
                request.start();
            }
        });
    }

    public void loadChatGroupInfo(String group) {
        this.groupsInfo += group+",";
        tsbLoadGroups.run(groupsInfo);
    }
    TimerSearchBreaker tsbLoadGroups;
    private void initLoaderGroups(){
        tsbLoadGroups = new TimerSearchBreaker(act, new TimerSearchBreaker.ISearchTask() {
            @Override
            public void searchUpdate(String query) {
                groupsInfo = "";
                VKRequest request = new VKRequest("groups.getById", VKParameters.from("group_ids", query));
                request.setRequestListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(final VKResponse response) {
                        super.onComplete(response);

                        L.d("VKResponse response = " + response.responseString);
                        try {
                            JSONObject responseObject = response.json;
                            Map<String, Object> responseMap = JsonUtils.jsonToMap(responseObject);
                            List<Map<String, Object>> groups = (List<Map<String, Object>>) responseMap.get("response");

                            Map<String,Map<String,Object>> groupsMap = new HashMap<>();
                            for(Map<String, Object> group:groups){
                                groupsMap.put(group.get("id").toString(),group);
                            }
                            if(isResendMessages()){
                                parentAdapter.addGroups(groupsMap);
                            }else{
                                MessageAdapter.this.groups.putAll(groupsMap);
                            }

                            notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(VKError error) {
                        L.d("error.apiError errorCode = " + error.errorCode + " errorMessage = " + error.errorMessage + " errorReason = " + error.errorReason);
                    }
                });
                request.start();
            }
        });
    }

    private static Integer convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    private void initStateMessage(ImageView stateMessageIV, Map<String, Object> message) {
        if(message.get("out") != null && message.get("read_state") != null){
            if((int)message.get("out") == 1){
                stateMessageIV.setVisibility(View.VISIBLE);
                if((int)message.get("read_state") == 0){
                    stateMessageIV.setImageResource(R.drawable.ic_done_green_24dp);
                }else{
                    stateMessageIV.setImageResource(R.drawable.ic_done_all_green_24dp);
                }
            }else{
                stateMessageIV.setVisibility(View.GONE);
            }

        }else {
            stateMessageIV.setVisibility(View.GONE);
        }
    }

    private Boolean isResendMessages(){
        return parentAdapter != null;
    }

    public void setMessages(List<Map<String, Object>> messages){
        this.messages = messages;
        notifyDataSetChanged();
    }

    public void addMessages(List<Map<String, Object>> messages) {
        L.d("addMessages = " + messages);
        if(messageInDayList.isEmpty()){
            this.messages = messages;
            sortedMessages();
        }else{
            if(this.messages.size() == 1 && messages.size() == 1){
                L.d("this.messages.get(0).get(\"date\") = " + this.messages.get(0).get("date") + " messages.get(0).get(\"date\") = " + messages.get(0).get("date"));
                if(this.messages.get(0).get("date") == messages.get(0).get("date")){
                    return;
                }
            }

            this.messages.addAll(messages);
            for(Map<String, Object> message:messages){
                messageInDayList.get(messageInDayList.size()-1).addMessage(message);
            }
            notifyDataSetChanged();
        }
    }

    public void addInStartMessages(List<Map<String, Object>> messages) {
        L.d("addMessages = " + messages);
        if(messageInDayList.isEmpty()){
            this.messages = messages;
            sortedMessages();
        }else{

            L.d("this.messages.size() = " + this.messages.size() + " messages.size() = " + messages.size());
            if(this.messages.size() == 1 && messages.size() == 1){
                L.d("this.messages.get(0).get(\"date\") = " + this.messages.get(0).get("date") + " messages.get(0).get(\"date\") = " + messages.get(0).get("date"));
                if((int)this.messages.get(0).get("date") == (int)messages.get(0).get("date")){
                    L.d("return");
                    return;
                }
            }
            countMessagesToStart = messages.size();
            messages.addAll(new ArrayList<>(this.messages));
            this.messages = messages;
            sortedMessages();
        }
    }


    private OnLoadListener onLoadListener;
    public void setOnLoadListener(OnLoadListener onLoadListener) {
        this.onLoadListener = onLoadListener;
    }

    public Map<String, Map<String, Object>> getUsers() {
        return users;
    }

    public void setUsers(Map<String, Map<String, Object>> users) {
        this.users = users;
    }

    public void addGroups(Map<String, Map<String, Object>> groups) {
        this.groups.putAll(groups);
    }

    public Map<String, Map<String, Object>> getGroups() {
        return groups;
    }

    protected static class ViewHolder extends MediaVKContent.BaseViewHolder {
        RoundedImageView avatarIV;
        TextView textTV;
        TextView timeTV;
        TextView userNameTV;
        LinearLayout contentLL;
        LinearLayout messageLL;
        LinearLayout resendMessagesLL;
        LinearListView resendMessagesLV;
        LinearLayout repostLL;
        ImageView stateMessageIV;
    }

    public List<Map<String, Object>> getMessages() {
        return messages;
    }

    public void setParentAdapter(MessageAdapter parentAdapter) {
        this.parentAdapter = parentAdapter;
    }

    public void setMessageResendAdapter(MessageResendAdapter messageResendAdapter) {
        this.messageResendAdapter = messageResendAdapter;
    }

    @Override
    public void notifyDataSetChanged() {
        if(messageResendAdapter != null){
            messageResendAdapter.notifyDataSetChanged();
        }else{
            super.notifyDataSetChanged();
        }
    }

    public void reaOutMessage(){
        if(messageInDayList == null){
            return;
        }
        for(MessageInDay messageInDay:messageInDayList){
            for(int i = messageInDay.getMessages().size() - 1 ; i >= 0 ; i--){
                Map<String,Object> message = messageInDay.getMessages().get(i);
                message.put("read_state",1);
            }
        }
        notifyDataSetChanged();
    }

    public Map<String,Object> getUser(String id){
        return users.get(id);
    }

    public int getSaveGroupPosition() {
        return saveGroupPosition;
    }

    public int getSaveChildPosition() {
        return saveChildPosition;
    }
}