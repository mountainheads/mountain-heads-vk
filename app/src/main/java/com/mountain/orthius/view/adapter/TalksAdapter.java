package com.mountain.orthius.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.makeramen.RoundedImageView;
import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.preferences.AppPreferences;
import com.mountain.orthius.utils.DateHandler;
import com.mountain.orthius.utils.JsonUtils;
import com.mountain.orthius.utils.TimerSearchBreaker;
import com.mountain.orthius.utils.image.LoaderTalkPhoto;
import com.mountain.orthius.view.activity.ChatActivity;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TalksAdapter extends RecyclerView.Adapter<TalksAdapter.ViewHolder> {
    private Context ctx;
    private LayoutInflater lInflater;
    private List<Map<String,Object>> dialogs;
    private Map<Integer, String> multiPhotos = new HashMap<>();;
    private Map<Integer, String> names = new HashMap<>();;
    private String chatsActiveUids = "";
    private String chatsActiveUidsGroup = "";
    private boolean isFirstLoadedUserInfo = false;
    private ArrayList<Map<String, Object>> dialogsBackup;


    public TalksAdapter(Context context, List<Map<String,Object>> dialogs) {
        ctx = context;
        this.dialogs = dialogs;
        if (ctx != null) {
            lInflater = (LayoutInflater) ctx
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        initLoaderUsers();
        fillInfoUsers();
    }

    private void fillInfoUsers(){
        for (Map<String,Object> item:dialogs) {
            Map<String,Object> message = (Map<String,Object>)item.get("message");

            if (message.get("chat_active") != null) {
                List<Integer> chat_active = (List<Integer>) message.get("chat_active");
                if (chat_active.size() != 0) {
                    for (Integer userId:chat_active) {
                        loadChatActiveUser(userId+"", false);
                    }
                }
            } else {
                if (message.get("user_id") != null) {
                    loadChatActiveUser(message.get("user_id").toString(), false);
                }
            }
        }
        tsbLoadUsers.runNow(chatsActiveUids);
        tsbLoadGroups.runNow(chatsActiveUidsGroup);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(ctx).inflate(R.layout.item_talk, parent, false);
        return new ViewHolder(view);
    }

    private Boolean statisticMode = false;
    public void setStatisticMode(Boolean mode){
        statisticMode = mode;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Map<String,Object> talk = getItem(position);
        Map<String,Object> message = (Map<String,Object>)talk.get("message");

        if (message == null) {
            return;
        }
        initDate(holder, message);
        initTitle(holder, message);
        initBody(holder, message);
        initAvatar(holder, message);
        initStateMessage(holder, message);
        initUnread(holder, talk);

        holder.rootRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Map<String, Object> message = (Map<String, Object>) getItem(position).get("message");

                Intent intent = new Intent(ctx, ChatActivity.class);
                if (message.get("chat_id") != null) {
                    intent.putExtra(ChatActivity.CHAT_ID, message.get("chat_id").toString());
                    intent.putExtra(ChatActivity.CHAT_ACTIVE_UIDS, getChatActiveUids(position));
                } else {
                    intent.putExtra(ChatActivity.USER_ID, message.get("user_id").toString());
                    intent.putExtra(ChatActivity.USER_NAME, getUserName(position));
                    intent.putExtra(ChatActivity.USER_AVATAR, getUserAvatar(position));
                }
                ((Activity)ctx).startActivity(intent);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return dialogs.size();
    }

    private void initStateMessage(ViewHolder holder, Map<String, Object> message) {
        ImageView stateMessageIV = holder.stateMessageIV;

        if(message.get("out") != null && message.get("read_state") != null){
            if((int)message.get("out") == 1){
                stateMessageIV.setVisibility(View.VISIBLE);
                if((int)message.get("read_state") == 0){
                    stateMessageIV.setImageResource(R.drawable.ic_done_green_24dp);
                }else{
                    stateMessageIV.setImageResource(R.drawable.ic_done_all_green_24dp);
                }
            }else{
                stateMessageIV.setVisibility(View.GONE);
            }

        }else {
            stateMessageIV.setVisibility(View.GONE);
        }
    }

    private void initUnread(ViewHolder holder, Map<String, Object> talk) {
        TextView unReadTV = holder.unReadTV;
        if(talk.get("unread") != null){
            if((int)talk.get("unread") == 0){
                unReadTV.setVisibility(View.GONE);
            }else{
                unReadTV.setVisibility(View.VISIBLE);
                unReadTV.setText(talk.get("unread").toString());
            }
        }else {
            unReadTV.setVisibility(View.GONE);
        }
    }


    private void initDate(ViewHolder view, Map<String,Object> message){
        TextView timeTV = view.timeTV;

        Long date = Long.valueOf((int)message.get("date")) * 1000;
        DateHandler.getCHPUDurationBy(date);

        String outDate = null;
        if (DateHandler.getOnlyDate(new Date(date)).equals(DateHandler.getOnlyDate(new Date()))) {
            outDate = DateHandler.getOnlyTime(new Date(date));
        } else {
            outDate = DateHandler.getOnlyDate(new Date(date));
        }
        timeTV.setText(outDate);
    }

    private void initTitle(ViewHolder view, Map<String,Object> message){
        TextView titleTV = view.titleTV;

        String title = message.get("title").toString();

        if (names != null && message.get("chat_active") == null && message.get("user_id") != null && names.get(message.get("user_id")) != null) {
            titleTV.setText(names.get(message.get("user_id")));
        } else if (title.isEmpty()) {
            titleTV.setText(R.string.talks_untitled);
        } else {
            titleTV.setText(title);
        }
    }

    private void initBody(ViewHolder view, Map<String,Object> message){
        TextView bodyTV = view.bodyTV;
        String body = message.get("body").toString();

        if(message.get("write_message") != null){
            if(message.get("chat_id") == null){
                bodyTV.setText("Печатает...");
            }else{
                bodyTV.setText(names.get(message.get("write_message")) + " печатает...");
            }
            return;
        }

        if (body.isEmpty()) {
            bodyTV.setText(R.string.talks_messages_empty);
            if (message.get("attachments") != null && ((List<Map<String,Object>>)message.get("attachments")).get(0).get("type").equals("sticker")) {
                bodyTV.setText(R.string.talks_adapter_sticker);
            }
            if (message.get("attachments") != null && ((List<Map<String,Object>>)message.get("attachments")).get(0).get("type").equals("audio")) {
                bodyTV.setText(R.string.talks_adapter_audio);
            }
            if (message.get("attachments") != null && ((List<Map<String,Object>>)message.get("attachments")).get(0).get("type").equals("photo")) {
                bodyTV.setText(R.string.talks_adapter_photo);
            }
            if (message.get("attachments") != null && ((List<Map<String,Object>>)message.get("attachments")).get(0).get("type").equals("video")) {
                bodyTV.setText(R.string.talks_adapter_video);
            }
            if (message.get("attachments") != null && ((List<Map<String,Object>>)message.get("attachments")).get(0).get("type").equals("doc")) {
                bodyTV.setText(R.string.talks_adapter_doc);
            }
            if (message.get("attachments") != null && ((List<Map<String,Object>>)message.get("attachments")).get(0).get("type").equals("wall")) {
                bodyTV.setText(R.string.talks_adapter_wall);
            }
            if (message.get("attachments") != null && ((List<Map<String,Object>>)message.get("attachments")).get(0).get("type").equals("gift")) {
                bodyTV.setText(R.string.talks_adapter_gift);
            }
            if (message.get("attachments") != null && ((List<Map<String,Object>>)message.get("attachments")).get(0).get("type").equals("link")) {
                bodyTV.setText(R.string.talks_adapter_link);
            }
            if (message.get("fwd_messages") != null) {
                bodyTV.setText(R.string.talks_adapter_resend);
            }

        } else {
            bodyTV.setText(body);
        }
        if(message.get("chat_id") != null){
            if(names.get(message.get("user_id")) != null){
                bodyTV.setText(names.get(message.get("user_id")).split("\\s+")[0] + ": " + bodyTV.getText());
            }
        }
    }

    private void initAvatar(ViewHolder view, Map<String,Object> message){
        RoundedImageView avatarIV = view.avatarIV;
        avatarIV.setImageResource(R.drawable.ic_account_box_56dp);
        if (message.get("photo_50") == null && message.get("chat_id") != null) {
            if (multiPhotos != null) {
                createPhotoCollage(view, message);
            }
        } else if (message.get("photo_50") != null) {
            LoaderTalkPhoto.with(ctx).loadImage(message.get("photo_50").toString(), avatarIV, (int) message.get("id"));
        } else if (multiPhotos != null && message.get("user_id") != null && multiPhotos.get(message.get("user_id")) != null) {
            LoaderTalkPhoto.with(ctx).loadImage(multiPhotos.get(message.get("user_id")), avatarIV, (int) message.get("id"));
        } else {
            loadChatActiveUser(message.get("user_id").toString());
        }
    }

    private void createPhotoCollage(ViewHolder view, Map<String,Object> message){
        final RoundedImageView avatarIV = view.avatarIV;
        List<Integer> chat_active = (List<Integer>) message.get("chat_active");
        if (chat_active == null) {// Если сообщение пришло черег лонг пул и ранее ифны о диалоге не было, то загружаем ее
            loadDialogInfo((int) message.get("chat_id"), message);
            return;
        }

        final List<String> images = new ArrayList<>();
        Boolean success = true;
        for (Integer userId:chat_active) {
            if(multiPhotos.get(userId) == null){
                loadChatActiveUser(message.get("user_id").toString());
                success = false;
                continue;
            }
            images.add(multiPhotos.get(userId));
        }
        if(success){
            LoaderTalkPhoto.with(ctx).loadImages(images, avatarIV, (int) message.get("id"));
        }
    }

    TimerSearchBreaker tsbLoadUsers;
    TimerSearchBreaker tsbLoadGroups;

    public void loadChatActiveUser(String user) {
        if(isFirstLoadedUserInfo){
            loadChatActiveUser(user, true);
        }
    }
    public void loadChatActiveUser(String user, Boolean withTSB) {
        if(!this.names.containsKey(Integer.parseInt(user))){
            if(Integer.parseInt(user) > 0){
                this.chatsActiveUids += user+",";
                if(withTSB){
                    tsbLoadUsers.run(chatsActiveUids);
                }

            }else{
                this.chatsActiveUidsGroup += user.replace("-","")+",";
                if(withTSB){
                    tsbLoadGroups.run(chatsActiveUidsGroup);
                }
            }
        }
    }

    public void initLoaderUsers() {// Загрузка инфы о юзерах, фото и имя
        tsbLoadUsers = new TimerSearchBreaker(ctx, new TimerSearchBreaker.ISearchTask() {
            @Override
            public void searchUpdate(String query) {
                if(query == null){
                    return;
                }

                final Handler handler = new Handler(Looper.getMainLooper());
                L.d("test query = " + query);
                final VKRequest request = new VKRequest("users.get", VKParameters.from("user_ids", query, "fields", "photo_50"));

                request.setRequestListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(final VKResponse response) {
                        super.onComplete(response);
                        isFirstLoadedUserInfo = true;

                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                L.d("VKResponse response = " + response.responseString);
                                chatsActiveUids = "";
                                try {
                                    List<Object> responseObject = JsonUtils.toList(response.json.getJSONArray("response"));
                                    L.d("responseObject size = " + responseObject.size());
                                    for (Object object : responseObject) {
                                        Map<String, Object> user = (Map<String, Object>) object;
                                        multiPhotos.put((int) user.get("id"), user.get("photo_50").toString());
                                        names.put((int) user.get("id"), user.get("first_name") + " " + user.get("last_name"));

                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                notifyDataSetChanged();
                                            }
                                        });
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        thread.start();

                    }

                    @Override
                    public void onError(VKError error) {
                        if (error.apiError != null && error.apiError.errorCode == 6) {
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    request.repeat();
                                }
                            }, 700);
                        }
                    }
                });
                request.start();
            }
        });

        tsbLoadGroups = new TimerSearchBreaker(ctx, new TimerSearchBreaker.ISearchTask() {
            @Override
            public void searchUpdate(String query) {
                if(query == null || query.isEmpty()){
                    return;
                }
                final Handler handler = new Handler(Looper.getMainLooper());
                L.d("test query = " + query);
                final VKRequest request = new VKRequest("groups.getById", VKParameters.from("group_ids", query));

                request.setRequestListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(final VKResponse response) {
                        super.onComplete(response);

                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                L.d("VKResponse response = " + response.responseString);
                                chatsActiveUidsGroup = "";
                                try {
                                    List<Object> responseObject = JsonUtils.toList(response.json.getJSONArray("response"));
                                    L.d("responseObject size = " + responseObject.size());
                                    for (Object object : responseObject) {
                                        Map<String, Object> group = (Map<String, Object>) object;
                                        multiPhotos.put(-(int) group.get("id"), group.get("photo_50").toString());
                                        names.put(-(int) group.get("id"), group.get("name")+"");

                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                notifyDataSetChanged();
                                            }
                                        });
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        thread.start();
                    }

                    @Override
                    public void onError(VKError error) {
                        try{
                           L.d("error.apiError = " + error.apiError);
                        }catch (Exception e){

                        }
                        if (error.apiError != null && error.apiError.errorCode == 6) {
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    request.repeat();
                                }
                            }, 700);
                        }
                    }
                });
                request.start();
            }
        });
    }

    public String getUserName(Integer position) {
        Map<String,Object> message = (Map<String,Object>) getItem(position).get("message");
        if (message.get("user_id") == null) {
            return "";
        }
        return names.get(message.get("user_id"));
    }

    public String getUserAvatar(Integer position) {
        Map<String,Object> message = (Map<String,Object>)getItem(position).get("message");
        if (message.get("user_id") == null) {
            return "";
        }
        return multiPhotos.get(message.get("user_id"));
    }

    public String getChatActiveUids(Integer position) {
        Map<String,Object> message = (Map<String,Object>)getItem(position).get("message");
        if (message.get("chat_active") == null) {
            return "";
        }
        List<Integer> chat_active = (List<Integer>) message.get("chat_active");
        String uids = "";
        for (Integer uid:chat_active) {
            uids += uid + ",";
        }
        return uids;
    }

    private List<Integer> chatIds = new ArrayList<>();

    private void loadDialogInfo(Integer chatId, final Map<String,Object> message) {// загрузка инфы о беседе, фото и имя
        L.d("loadDialogInfo");
        if (chatIds.contains(chatId)) {
            return;
        }
        chatIds.add(chatId);
        VKRequest request = new VKRequest("messages.getChat", VKParameters.from("chat_ids", chatId, "fields", "photo_50"));

        request.setRequestListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(final VKResponse response) {
                super.onComplete(response);
                L.d("VKResponse response = " + response.responseString);
                try {
                    JSONArray responseArray = response.json.getJSONArray("response");

                    JSONObject item = responseArray.getJSONObject(0);
                    JSONArray users = item.getJSONArray("users");
                    List<Integer> chat_active = new ArrayList<>();
                    for (int i = 0; i < users.length(); i++) {
                        JSONObject user = users.getJSONObject(i);
                        if(AppPreferences.getMyId(ctx).equals(user.get("id").toString())) {
                            continue;
                        }
                        L.d("loadDialogsInfo photo_50 = " + user.getString("photo_50"));
                        L.d("loadDialogsInfo first_name = " + user.getString("first_name"));
                        multiPhotos.put((int) user.get("id"), user.getString("photo_50"));
                        names.put(user.getInt("id"), user.getString("first_name") + " " + user.getString("last_name"));
                        chat_active.add((int) user.get("id"));
                    }

                    message.put("chat_active", chat_active);
                    notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VKError error) {
                L.d("error.apiError errorCode = " + error.errorCode + " errorMessage = " + error.errorMessage + " errorReason = " + error.errorReason);
            }
        });
        request.start();
    }

    public void setDialogs(List<Map<String,Object>> dialogs) {
        L.d("dialogs = " + dialogs);
        this.dialogs = dialogs;
        fillInfoUsers();
        notifyDataSetChanged();
    }

    public void setSearchedDialogs(List<Map<String,Object>> dialogs, Boolean isSearchMode) {
        L.d("dialogs = " + dialogs);
        if(!isSearchMode){
            this.dialogsBackup = new ArrayList<>(this.dialogs);
        }

        List<Map<String,Object>> formattedDialos = new ArrayList<>();
        for(Map<String,Object> dialog:dialogs){
            Map<String,Object> wrapperMessage = new HashMap<>();
            wrapperMessage.put("message",dialog);
            formattedDialos.add(wrapperMessage);
        }

        this.dialogs = formattedDialos;

        fillInfoUsers();
        notifyDataSetChanged();
    }
    public void returnAllDialogs() {
        if(dialogsBackup == null){
            return;
        }
        this.dialogs = dialogsBackup;
        fillInfoUsers();
        notifyDataSetChanged();
    }

    public Map<String,Object> getItem(int position) {
        return dialogs.get(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView timeTV;
        public TextView titleTV;
        public TextView bodyTV;
        public RoundedImageView avatarIV;
        public TextView unReadTV;
        public ImageView stateMessageIV;
        public RelativeLayout rootRL;

        public ViewHolder(View view) {
            super(view);
            timeTV = (TextView) view.findViewById(R.id.timeTV);
            titleTV = (TextView) view.findViewById(R.id.titleTV);
            bodyTV = (TextView) view.findViewById(R.id.bodyTV);
            avatarIV = (RoundedImageView) view.findViewById(R.id.avatarIV);
            unReadTV = (TextView) view.findViewById(R.id.unReadTV);
            stateMessageIV = (ImageView) view.findViewById(R.id.stateMessageIV);
            rootRL = (RelativeLayout) view.findViewById(R.id.rootRL);
        }
    }
}

