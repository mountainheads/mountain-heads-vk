package com.mountain.orthius.serverApi;



import com.mountain.orthius.model.MistakesModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServerApi {

    @FormUrlEncoded
    @POST("checkText")
    Call<ArrayList<MistakesModel>> checkText(@Field("text") String text);

    @FormUrlEncoded
    @POST("checkText")
    Call<ArrayList<MistakesModel>> checkTextOnlyMistakes(@Field("text") String text, @Field("options") String options);

    @FormUrlEncoded
    @POST("checkText")
    Call<ArrayList<MistakesModel>> checkText(@Field("text") String text,  @Field("options") String options);


}
