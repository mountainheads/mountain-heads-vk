package com.mountain.orthius.model;


public class MistakesModel
{
    private String col;

    private String[] s;

    private String word;

    private String len;

    private String code;

    private String row;

    private String pos;

    public String getCol ()
    {
        return col;
    }

    public void setCol (String col)
    {
        this.col = col;
    }

    public String[] getS ()
    {
        return s;
    }

    public void setS (String[] s)
    {
        this.s = s;
    }

    public String getWord ()
    {
        return word;
    }

    public void setWord (String word)
    {
        this.word = word;
    }

    public String getLen ()
    {
        return len;
    }

    public void setLen (String len)
    {
        this.len = len;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getRow ()
    {
        return row;
    }

    public void setRow (String row)
    {
        this.row = row;
    }

    public String getPos ()
    {
        return pos;
    }

    public void setPos (String pos)
    {
        this.pos = pos;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [col = "+col+", s = "+s+", word = "+word+", len = "+len+", code = "+code+", row = "+row+", pos = "+pos+"]";
    }
}
