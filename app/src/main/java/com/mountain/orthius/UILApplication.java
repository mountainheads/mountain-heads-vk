package com.mountain.orthius;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.telephony.TelephonyManager;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.preferences.AppPreferences;
import com.mountain.orthius.serverApi.ServerApi;
import com.mountain.orthius.vkdialogs.Dialogs;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.UUID;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class UILApplication extends Application {
    public static Context context;
    private Dialogs dialogs;
    private Retrofit retrofit;
    private static ServerApi serverApi;

    VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            if (newToken == null) {
                // VKAccessToken is invalid
                return;
            }
//            ParseInstallation.getCurrentInstallation().put("userId", newToken.userId);
//            ParseInstallation.getCurrentInstallation().put("userToken", newToken.accessToken);
//            ParseInstallation.getCurrentInstallation().saveInBackground();

            AppPreferences.setVkId(getApplicationContext(), newToken.userId);
//            ParseInstallation.getCurrentInstallation().put("userId", newToken.userId);
//            ParseInstallation.getCurrentInstallation().put("userToken", newToken.accessToken);
//            ParseInstallation.getCurrentInstallation().saveInBackground();
            L.d("dialogs = " + dialogs.isConnect());

//            if(!dialogs.isConnect()){
//                dialogs.connect();
//            }
//            setupPush();
        }
    };

    private void setupPush() {
        if(VKSdk.isLoggedIn()){
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    final InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
                    String senderId = "269083836714";
                    try {
                        // request token that will be used by the server to send push notifications

                        String token = instanceID.getToken(senderId, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                        L.d("GCM Registration Token: " + token);

                        // pass along this data
                        sendRegistrationToServer(token);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        }
    }

    private void sendRegistrationToServer(String token) {
        String deviceId = getDeviceId();

        JSONObject settings = new JSONObject();
        try {
            settings.put("msg","on");
            settings.put("wall_post","on");
            settings.put("new_post","on");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        L.d("token = " + token + " deviceId = " + deviceId);
        VKRequest requestNoReadMessages = new VKRequest("account.registerDevice", VKParameters.from("token", token, "device_id", deviceId, "settings",settings));
        requestNoReadMessages.setRequestListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(final VKResponse response) {
                super.onComplete(response);
                L.d("response.json = " + response.json);
            }

            @Override
            public void onError(VKError error) {
                L.d("error = " + error.toString());
            }
        });
        requestNoReadMessages.start();
    }
    public String getDeviceId(){
        L.d("getDeviceId");
        try {
            final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

            final String tmDevice, tmSerial, androidId;
            tmDevice = "" + tm.getDeviceId();
            tmSerial = "" + tm.getSimSerialNumber();
            androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

            UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
            String deviceId = deviceUuid.toString();
            return deviceId;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.context = getApplicationContext();

        setupVK();
        setupPush();

        retrofit = new Retrofit.Builder()
                .baseUrl("http://speller.yandex.net/services/spellservice.json/") //Базовая часть адреса
                .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
                .build();
        serverApi = retrofit.create(ServerApi.class); //Создаем объект, при помощи которого будем выполнять запросы
    }


    private void setupVK(){
        vkAccessTokenTracker.startTracking();
        VKSdk.initialize(getApplicationContext());

        dialogs = Dialogs.getInstance();
    }
    public Dialogs getDialogs() {
        return dialogs;
    }

    public static ServerApi getApi() {
        return serverApi;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        MultiDex.install(this);
    }

}