package com.mountain.orthius.longpollserver;

/**
 * Created by vladimir on 10.02.16.
 */
public interface Observable {
    void registerObserver(LongPollObserver o);
    void removeObserver(LongPollObserver o);
}
