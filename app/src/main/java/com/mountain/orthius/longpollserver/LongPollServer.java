package com.mountain.orthius.longpollserver;

import android.os.Handler;
import android.os.Looper;

import com.mountain.orthius.loging.L;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


/**
 * Created by vladimir on 09.02.16.
 */
public class LongPollServer implements Observable {
    private Observers observers;

    private static LongPollServer instance;

    public static String ts;
    public static String pts;

    private Boolean isRunning = true;

    public static LongPollServer getInstance() {
        if (instance == null) {
            instance = new LongPollServer();
        }
        return instance;
    }

    public LongPollServer(){
        observers = new Observers();
        connectLongServer();
    }

    public void connectLongServer(){
        isRunning = true;
        L.d("testDialog LongPollServer connectLongServer");
        VKRequest request = new VKRequest("messages.getLongPollServer", VKParameters.from("use_ssl", 1, "need_pts", 1));

        request.setRequestListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(final VKResponse response) {
                L.d("VKResponse = " + response.json);
                try {
                    JSONObject responseObject = response.json.getJSONObject("response");
                    if(responseObject != null){
                        ts = responseObject.getString("ts");
                        pts = responseObject.getString("pts");
                        queryToLongServer(responseObject.getString("server"),responseObject.getString("key"),responseObject.getString("ts"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                super.onComplete(response);
            }

            @Override
            public void onError(VKError error) {
                L.d("error.apiError errorCode = " + error.errorCode + " errorMessage = " + error.errorMessage + " errorReason = " + error.errorReason);
                connectLongServer();
            }
        });
        request.start();
    }

    private void queryToLongServer(final String server, final String key, final String ts){
        if(!isRunning){
            return;
        }
        L.d("testDialog LongPollServer queryToLongServer");
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {// вызывается в handler чтобы процесс умирал при закрытии приложения
            @Override
            public void run() {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        URL url;
                        String response = "";
                        try {
                            //http://{$server}?act=a_check&key={$key}&ts={$ts}&wait=25&mode=2
                            url = new URL("https://" + server + "?act=a_check&key=" + key + "&ts=" + ts + "&wait=25&mode=2");
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setReadTimeout(15000);
                            conn.setConnectTimeout(15000);
                            conn.setDoInput(true);
                            conn.setDoOutput(true);

                            int responseCode = conn.getResponseCode();

                            if (responseCode == HttpsURLConnection.HTTP_OK) {
                                String line;
                                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                                while ((line = br.readLine()) != null) {
                                    response += line;
                                }
                                final JSONObject responseJson = new JSONObject(response);
                                if (!responseJson.isNull("failed")) {
                                    if (responseJson.getInt("failed") == 2) {
                                        try {
                                            Thread.sleep(5000);
                                        } catch (InterruptedException e1) {

                                        }
                                        connectLongServer();
                                        return;
                                    }
                                }
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        observers.notifyUpdateMessage(responseJson);
                                    }
                                });
                                L.d("responseJson = " + responseJson);
                                queryToLongServer(server, key, responseJson.getString("ts"));
                            } else {

                            }
                        } catch (Exception e) {
                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e1) {
                            }
                            queryToLongServer(server, key, ts);
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
            }
        });
    }

    public void stop(){
        instance = null;
        isRunning = false;
    }


    @Override
    public void registerObserver(LongPollObserver o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(LongPollObserver o) {
        observers.remove(o);
    }
}
