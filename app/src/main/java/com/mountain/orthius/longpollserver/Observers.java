package com.mountain.orthius.longpollserver;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by vladimir on 10.02.16.
 */
public class Observers<T extends LongPollObserver> extends ArrayList<T> {
    public void notifyUpdateMessage(JSONObject obj) {
        for (Iterator<T> iter = (Iterator<T>) iterator(); iter.hasNext();)
            iter.next().updateMessage(obj);
    }
}
