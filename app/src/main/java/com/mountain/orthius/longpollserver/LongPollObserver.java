package com.mountain.orthius.longpollserver;

import org.json.JSONObject;

/**
 * Created by vladimir on 10.02.16.
 */
public interface LongPollObserver {
    void updateMessage(JSONObject obj);
}
