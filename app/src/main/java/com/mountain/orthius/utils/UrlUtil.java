package com.mountain.orthius.utils;

import java.util.Map;

/**
 * Created by alexeybalabanov on 25.11.14.
 */
public class UrlUtil {
    public static String getStringForUrlGetFromMap(Map<String,String> map){
        String getParams = "";
        for (Map.Entry<String, String> entry : map.entrySet()){
            if (getParams.isEmpty()){
                getParams += entry.getKey() + "=" + entry.getValue();
            }else {
                getParams += "&" + entry.getKey() + "=" + entry.getValue();
            }
        }
        return getParams;
    }
}
