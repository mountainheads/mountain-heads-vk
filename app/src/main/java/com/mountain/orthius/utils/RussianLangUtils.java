package com.mountain.orthius.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.mountain.orthius.R;

import java.util.Map;


/**
 * Created by alexeybalabanov on 31.03.15.
 */
public class RussianLangUtils {
    public static String getAge(String age){
        if(age == null || age.isEmpty()){
            return "";
        }
        String lastChar;
        try {
            lastChar = age.substring(age.length()-1,age.length());
        }catch (Exception e){
            return age;
        }
        if(age.length() > 1){
            String preLastChar = age.substring(age.length()-2,1);
            if(preLastChar.equals("1")){
                return age + " лет";
            }
        }
        if(lastChar.equals("1")){
            return age + " год";
        }
        if(lastChar.equals("2") || lastChar.equals("3") || lastChar.equals("4")){
            return age + " года";
        }else{
            return age + " лет";
        }
    }

    public static Boolean setRelationText(Context ctx, Map<String,Object> user, TextView statusTV){
        if(user.get("relation") != null){
            switch ((int) user.get("relation")){
                case 0:
                    statusTV.setText("Статус не указан");
                    break;
                case 1:
                    if(user.get("sex") != null && (int)user.get("sex") == 1){
                        statusTV.setText("Не замужем");
                    }else{
                        statusTV.setText("не женат");
                    }
                    break;
                case 2:
                    if(user.get("sex") != null && (int)user.get("sex") == 1){
                        statusTV.setText("Есть друг");
                    }else{
                        statusTV.setText("Есть подруга");
                    }
                    break;
                case 3:
                    if(user.get("sex") != null && (int)user.get("sex") == 1){
                        statusTV.setText("Помолвлена");
                    }else{
                        statusTV.setText("Помолвлен");
                    }
                    break;
                case 4:
                    if(user.get("sex") != null && (int)user.get("sex") == 1){
                        statusTV.setText("Замужем");
                    }else{
                        statusTV.setText("Женат");
                    }
                    break;
                case 5:
                    statusTV.setText("Всё сложно");
                    break;
                case 6:
                    statusTV.setText("В активном поиске");
                    break;
                case 7:
                    if(user.get("sex") != null && (int)user.get("sex") == 1){
                        statusTV.setText("Влюблена");
                    }else{
                        statusTV.setText("Влюблён");
                    }
                    break;
            }
            return false;
        }else{
            statusTV.setText("Статус не указан");
            return true;
        }
    }
    public static Boolean setRelationColor(Context ctx, Map<String,Object> user, TextView statusTV){
        if(user.get("relation") != null){
            switch ((int) user.get("relation")){
                case 0:
                    statusTV.setTextColor(ContextCompat.getColor(ctx, R.color.text_gray));
                    break;
                case 1:
                    statusTV.setTextColor(ContextCompat.getColor(ctx, R.color.green));
                    break;
                case 2:
                    statusTV.setTextColor(ContextCompat.getColor(ctx, R.color.color_red));
                    break;
                case 3:
                    statusTV.setTextColor(ContextCompat.getColor(ctx, R.color.color_red));
                    break;
                case 4:
                    statusTV.setTextColor(ContextCompat.getColor(ctx, R.color.color_red));
                    break;
                case 5:
                    statusTV.setTextColor(ContextCompat.getColor(ctx, R.color.green));
                    break;
                case 6:
                    statusTV.setTextColor(ContextCompat.getColor(ctx, R.color.green));
                    break;
                case 7:
                    statusTV.setTextColor(ContextCompat.getColor(ctx, R.color.color_red));
                    break;
            }
            return false;
        }else{
            statusTV.setTextColor(ContextCompat.getColor(ctx, R.color.text_gray));
            return true;
        }
    }

}