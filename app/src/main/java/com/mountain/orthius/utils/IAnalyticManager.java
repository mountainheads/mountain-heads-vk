package com.mountain.orthius.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by alexeybalabanov on 25.06.15.
 */
public interface IAnalyticManager {
    public void run(final OnAnalyticListener onServerResult, final ArrayList<String> ids);
    public boolean isStop();
    public void setStop(boolean isStop);
    public Map<Integer, Integer> getResult();
    public int getAllOutWordsCount();
    public ArrayList<String> getAllWords();
    public ArrayList<String> getTopMistakes();
}
