package com.mountain.orthius.utils;



import com.mountain.orthius.UILApplication;

import java.util.Locale;

/**
 * Created by X550V on 03.08.2016.
 */
public class DeclensionManager {


    public static String declension(int count, String one, String two, String five) {
        int realCount = count;
        Locale current = UILApplication.context.getResources().getConfiguration().locale;
        Locale locale = new Locale("en");
       if (current.equals(locale)){
           if (count == 0 || count == 1){
               return realCount + " " + one;
           } else {
               return realCount + " " + two;
           }
       }
        if (count > 100)
            count %= 100;

        if (count > 20)
            count %= 10;

        switch (count) {
            case 1:
                return realCount + " " + one;
            case 2:
            case 3:
            case 4:
                return realCount + " " + two;
            default:
                return realCount + " " + five;
        }
    }


}
