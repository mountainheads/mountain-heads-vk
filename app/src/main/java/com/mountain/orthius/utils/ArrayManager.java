package com.mountain.orthius.utils;

import com.mountain.orthius.loging.L;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;



/**
 * Created by alexeybalabanov on 20.10.14.
 */
public class ArrayManager {
    public interface OnCheckElementsGoneAndNew{
        public void onGoneElements(String description);
        public void onNewElements(String description);
    }

    public static void mergeListWithoutDuplicate(List<Map<String, Object>> mainList, List<Map<String, Object>> addList, String uniqueParameter) {
        L.d("addList.size() = " + addList.size() + " mainList.size() = " + mainList.size());
        List<Map<String, Object>> supportList = new ArrayList<Map<String, Object>>();
        boolean match = false;
        for (Map<String, Object> addItem : addList) {
            match = false;
            for (Map<String, Object> mainItem : mainList) {
                if (mainItem.get(uniqueParameter).equals(addItem.get(uniqueParameter))) {
                    match = true;
                }
            }
            if(!match){
                supportList.add(addItem);
            }
        }
        mainList.addAll(supportList);
    }
    public static String listToString(List<String> list){
        String out = "";
        for(String lang:list){
            if(out.isEmpty()){
                out += lang;
            }else{
                out += ", " + lang;
            }
        }
        L.d("out = " + out + " list = " + list);
        return out;
    }

    public static List<String> newElements(List<String> oldElements, List<String> newElements) {
        List<String> outList = new ArrayList<>();
        for (String newElement : newElements) {
            boolean isOut = true;
            for (String oldElement : oldElements) {
                if (newElement.equals(oldElement)) {
                    isOut = false;
                    break;
                }
            }
            if (isOut) {
                outList.add(newElement);
            }
        }
        return outList;
    }

    public static List<String> goneElements(List<String> oldElements, List<String> newElements) {
        List<String> outList = new ArrayList<>();
        for (String oldElement : oldElements) {
            boolean isOut = true;
            for (String newElement : newElements) {
                if (oldElement.equals(newElement)) {
                    isOut = false;
                    break;
                }
            }
            if (isOut) {
                outList.add(oldElement);
            }
        }
        return outList;
    }

    public static void checkGoneAndNewElements(String oldStr, String newStr, OnCheckElementsGoneAndNew onCheckElementsGoneAndNew){
        String[] oldArray = oldStr.split(",");
        String[] newArray = newStr.split(",");

        ArrayList<String> oldList = new ArrayList<String>(Arrays.asList(oldArray));
        ArrayList<String> newList = new ArrayList<String>(Arrays.asList(newArray));

        if (oldList.isEmpty() || newList.isEmpty()) {
            return;
        }
        Collections.sort(oldList);
        Collections.sort(newList);

        if (!oldList.equals(newList)) {
            List<String> goneList = ArrayManager.goneElements(oldList, newList);
            List<String> addList = ArrayManager.newElements(oldList, newList);

            if (!goneList.isEmpty()) {
                String descGone = "";
                for (String element : goneList) {
                    if (descGone.isEmpty()) {
                        descGone += element;
                    } else {
                        descGone += ", " + element;
                    }
                }
                onCheckElementsGoneAndNew.onGoneElements(descGone);
            }
            if (!addList.isEmpty()) {
                String descNew = "";
                for (String element : addList) {
                    if (descNew.isEmpty()) {
                        descNew += element;
                    } else {
                        descNew += ", " + element;
                    }
                }
                onCheckElementsGoneAndNew.onNewElements(descNew);
            }
        }
    }
}
