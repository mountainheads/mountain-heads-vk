package com.mountain.orthius.utils;

import java.util.List;
import java.util.Map;

/**
 * Created by spbiphones on 22.10.17.
 */

public class BaseManager {
    protected String[] trashWord = new String[]{"в","не","и","на","ну","то","а","я","как","там","у","надо","что","ты","все","да","для","так","по","это","6","если","но","еще","че","чтобы","уже","за","можно","может","тоже","android","д","аг","через","же"," есть","тогда","о","тебя","только","ща","вот","из","меня","или","ок","тут","тебе","когда","было","х","бы"," ага","Ну","В","при","А","он","нет","a","И","над","где","до","его","от","та","ли","чем","будешь","Да","e","их","во","без","про","У","Не","под","такой","Но","со","Там","эту","На","Ок","c","туда=","Ага","эти","Это","С","с","Ты","Я","ты","так","мне","Как","как","Что","был","к","твои","Твои","какая","Тогда","она","он","они","Она","Он","Они","уж","мы","Если","Нет","Мне","ему","Так","вы","Xaxa","xaxa","Мда","мда","com","https","http","Xaxa","xaxa"};

    public int countWords(String s, Map<String,Integer> wordsList, List<String> trashList){
        int wordCount = 0;
        boolean word = false;
        int endOfLine = s.length() - 1;
        StringBuffer wordBuffer = new StringBuffer("");
        for (int i = 0; i < s.length(); i++) {
            // if the char is a letter, word = true.
            if (Character.isLetter(s.charAt(i)) && i != endOfLine) {
                word = true;
                // if char isn't a letter and there have been letters before,
                // counter goes up.
                wordBuffer.append(s.charAt(i));
            } else if (!Character.isLetter(s.charAt(i)) && word) {
                wordCount++;
                word = false;
                boolean isNotLatin = wordBuffer.toString().matches("[^\\x00-\\x7F]+");
                if (!trashList.contains(wordBuffer.toString()) && isNotLatin && wordBuffer.toString().length() > 2) {
                    if (wordsList.containsKey(wordBuffer.toString())) {
                        wordsList.put(wordBuffer.toString(), wordsList.get(wordBuffer.toString()) + 1);
                    } else {
                        wordsList.put(wordBuffer.toString(), 1);
                    }
                }
                wordBuffer.setLength(0);
                // last word of String; if it doesn't end with a non letter, it
                // wouldn't count without this.
            } else if (Character.isLetter(s.charAt(i)) && i == endOfLine) {
                wordCount++;
                boolean isNotLatin = wordBuffer.toString().matches("[^\\x00-\\x7F]+");
                if (!trashList.contains(wordBuffer.toString()) && isNotLatin && wordBuffer.toString().length() > 2) {
                    if (wordsList.containsKey(wordBuffer.toString())) {
                        wordsList.put(wordBuffer.toString(), wordsList.get(wordBuffer.toString()) + 1);
                    } else {
                        wordsList.put(wordBuffer.toString(), 1);
                    }
                }
                wordBuffer.setLength(0);
            }
        }
        return wordCount;
    }

    public int countWords(String s){
        int wordCount = 0;
        boolean word = false;
        int endOfLine = s.length() - 1;
        StringBuffer wordBuffer = new StringBuffer("");
        for (int i = 0; i < s.length(); i++) {
            // if the char is a letter, word = true.
            if (Character.isLetter(s.charAt(i)) && i != endOfLine) {
                word = true;
                // if char isn't a letter and there have been letters before,
                // counter goes up.
                wordBuffer.append(s.charAt(i));
            } else if (!Character.isLetter(s.charAt(i)) && word) {
                wordCount++;
                word = false;

                wordBuffer.setLength(0);
                // last word of String; if it doesn't end with a non letter, it
                // wouldn't count without this.
            } else if (Character.isLetter(s.charAt(i)) && i == endOfLine) {
                wordCount++;

                wordBuffer.setLength(0);
            }
        }
        return wordCount;
    }
}
