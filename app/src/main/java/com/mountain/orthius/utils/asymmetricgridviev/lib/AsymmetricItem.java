package com.mountain.orthius.utils.asymmetricgridviev.lib;

import android.os.Parcelable;

public interface AsymmetricItem extends Parcelable {
  int getColumnSpan();
  int getRowSpan();
}
