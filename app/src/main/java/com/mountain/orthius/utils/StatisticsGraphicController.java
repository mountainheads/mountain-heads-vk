package com.mountain.orthius.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.utils.view.GetAllMessagesForDiaologManager;
import com.mountain.orthius.view.adapter.TopWordsAdapter;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.mountain.orthius.utils.DateFormatter.isLoadMore;

/**
 * Created by spbiphones on 21.10.17.
 */

public class StatisticsGraphicController {

    protected List<Entry> yVals1;
    protected List<String> xVals = new ArrayList<>();
    protected Integer maxFollowers;
    protected Integer minFollowers;
    protected ArrayList historyList;
    private ArrayList<String> userIds;
    private int offset = 0;
    private boolean isFinished = false;
    private  Map<Integer, Integer> mistakesPercents;
    private LineChart graphLC;
    private Handler handler = new Handler(Looper.getMainLooper());
    private Activity activity;
    private TextView percentTV;
    private int countAllWords = 0;
    private TextView countWordsTV;
    private RecyclerView topWordsRV;
    private RecyclerView topMistakesRV;
    private RelativeLayout progressRL;
    private RelativeLayout progressRL2;
    private RelativeLayout progressRL3;
    private ProgressBar textProgressPB;
    private TextView myWordsTV;
    private IAnalyticManager manager;

    private ArrayList<JSONObject> outMessages = new ArrayList<>();
    public void setProgress(RelativeLayout progressRL,RelativeLayout progressRL2,RelativeLayout progressRL3, ProgressBar textProgressPB, TextView myWordsTV){
        this.progressRL = progressRL;
        this.progressRL2 = progressRL2;
        this.progressRL3 = progressRL3;
        this.textProgressPB = textProgressPB;
        this.myWordsTV = myWordsTV;
    }

    public void getData(ArrayList<String> userIds, LineChart graphLC, Activity activity, TextView percentTV, TextView countWordsTV, RecyclerView topWordsRV, RecyclerView topMistakesRV) {
        this.activity = activity;
        yVals1 = new ArrayList<Entry>();
        this.graphLC = graphLC;
        this.userIds = userIds;
        this.percentTV = percentTV;
        this.countWordsTV = countWordsTV;
        this.topWordsRV = topWordsRV;
        this.topMistakesRV = topMistakesRV;

       // drawGraphic(graphLC);
        loadAllOutMessages();
    }
    public void setStop(){
        if (manager != null){
            manager.setStop(true);
        }
    }

    private void loadAllOutMessages() {
        manager = new GetAllMessagesForDiaologManager();
        manager.run(new OnAnalyticListener() {
            @Override
            public void onSuccess() {

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        mistakesPercents = manager.getResult();
                        countAllWords = manager.getAllOutWordsCount();
                        countWordsTV.setText(DeclensionManager.declension(countAllWords, "слово", "слова", "слов"));
                        TopWordsAdapter topWordsAdapter = new TopWordsAdapter(activity, manager.getAllWords());
                        topWordsRV.setAdapter(topWordsAdapter);

                        TopWordsAdapter topWordsAdapter1 = new TopWordsAdapter(activity, manager.getTopMistakes());
                        topMistakesRV.setAdapter(topWordsAdapter1);

                        drawGraphic(graphLC);
                        progressRL.setVisibility(View.GONE);
                        progressRL2.setVisibility(View.GONE);
                        progressRL3.setVisibility(View.GONE);
                        textProgressPB.setVisibility(View.GONE);
                        myWordsTV.setVisibility(View.VISIBLE);


                    }
                });







            }

            @Override
            public void onStep(String user) {

            }

            @Override
            public void onStep(Map<String, Object> photo) {

            }

            @Override
            public void onStep(Map<String, Object> photo, Integer id) {

            }

            @Override
            public void onProgress(Integer indexUser, Integer lengthUser) {

            }

            @Override
            public void onError() {

            }
        }, userIds);


    }



    public void drawGraphic(LineChart lineChart) {



        xVals.add("АВГ");
        xVals.add("АВГ");
        xVals.add("АВГ");
        xVals.add("АВГ");
        xVals.add("СЕН");
        xVals.add("СЕН");
        xVals.add("СЕН");
        xVals.add("СЕН");
        xVals.add("ОКТ");
        xVals.add("ОКТ");
        xVals.add("ОКТ");
        xVals.add("ОКТ");

//        yVals1.add(new Entry(mistakesPercents.get(0), 0));
//        yVals1.add(new Entry(6, 1));
//        yVals1.add(new Entry(6, 2));
//        yVals1.add(new Entry(5, 3));
//        yVals1.add(new Entry(4, 4));
//        yVals1.add(new Entry(4, 5));
//        yVals1.add(new Entry(6, 6));
//        yVals1.add(new Entry(13, 7));
//        yVals1.add(new Entry(11, 8));
//        yVals1.add(new Entry(6, 9));
//        yVals1.add(new Entry(7, 10));
//        yVals1.add(new Entry(8, 11));


        for  (int i = 0; i < mistakesPercents.size(); i++) {

            L.d("drawGraphic entry = " + mistakesPercents.get(i));
            yVals1.add(new Entry(mistakesPercents.get(i), i));
        }
        L.d("drawGraphic yVals1Size = " + yVals1.size() + " xVals.size = " + xVals.size());

        LineDataSet set1 = new LineDataSet(yVals1, "");

        //set1.setCircleColor(ContextCompat.getColor(activity, R.color.button_stroke));
        set1.setDrawFilled(true);
        set1.setColor(ContextCompat.getColor(activity, R.color.button_blue2_pressed));
        set1.setFillColor(ContextCompat.getColor(activity, R.color.button_stroke));
        set1.setDrawCircleHole(false);
        set1.setDrawCircles(false);
        set1.setCircleHoleRadius(0);
       // set1.setCircleColorHole(ContextCompat.getColor(activity, R.color.button_light_blue_pressed));
        //set1.setCircleColor(Color.RED);
        set1.setValueTextColor(Color.WHITE);
        set1.setLineWidth(2f);
        set1.setCircleRadius(3f);
        set1.setDrawValues(false);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        LineData data = new LineData(xVals, dataSets);
        dataSets.add(set1); // add the datasets
        lineChart.setData(data);
        lineChart.animateX(500);
        lineChart.setDescription("");

        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        XAxis botAxis = lineChart.getXAxis();
        botAxis.setTextColor(Color.BLACK);
        botAxis.setDrawGridLines(true);


            botAxis.setLabelsToSkip(4);



        lineChart.getLegend().setEnabled(false);

        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.setTextColor(Color.BLACK);
        leftAxis.setAxisMaxValue(100);
       leftAxis.setAxisMinValue(0);



        leftAxis.setDrawZeroLine(false);

        leftAxis.setDrawLimitLinesBehindData(true);
        lineChart.getXAxis().setDrawLabels(true);
        lineChart.getAxisRight().setEnabled(false);


        lineChart.notifyDataSetChanged();



        int percent = (mistakesPercents.get(11) + mistakesPercents.get(10) + mistakesPercents.get(9))/3;
        percentTV.setText(percent + "%");


    }




}