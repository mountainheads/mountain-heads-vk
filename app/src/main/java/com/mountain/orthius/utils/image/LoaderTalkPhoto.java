package com.mountain.orthius.utils.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.makeramen.RoundedImageView;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.utils.ConverterUtil;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class LoaderTalkPhoto {
    private static Integer widthPhoto;
    private final static Integer LINE_BETWEEN_PHOTO_DP = 2;
    private final static Integer WIDTH_PHOTO_DP = 56;
    private static Integer lineBetweenPhoto;

    private Context context;

    public static void loadImage(Context context, String uri, final RoundedImageView imageView){
        L.d("LoaderTalkPhoto loadImage()");
        ImageManager.loadRoundedImage(context, uri, imageView);
    }

    LoaderTalkPhoto (Context context){
        this.context = context;
    }

    public static LoaderTalkPhoto with(Context context){

        return new LoaderTalkPhoto(context);
    }

    public void loadImages(final List<String> sourcesimages, final RoundedImageView imageView, Integer position){
        L.d("LoaderTalkPhoto loadImages()");
        if(widthPhoto == null){// init sizes
            widthPhoto = (int) ConverterUtil.convertDpToPixel(WIDTH_PHOTO_DP, context);
            lineBetweenPhoto = (int) ConverterUtil.convertDpToPixel(LINE_BETWEEN_PHOTO_DP,context);
        }

        List<String> images = getFirst4Images(sourcesimages);
        new SetImageTask(imageView,images, position).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void loadImage(String uri, final RoundedImageView imageView, Integer position){
        L.d("LoaderTalkPhoto loadImage()");
        if(widthPhoto == null){// init sizes
            widthPhoto = (int) ConverterUtil.convertDpToPixel(WIDTH_PHOTO_DP,context);
            lineBetweenPhoto = (int) ConverterUtil.convertDpToPixel(LINE_BETWEEN_PHOTO_DP,context);
        }

        List<String> images = new ArrayList<>();
        images.add(uri);
        new SetImageTask(imageView,images, position).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private List<String> getFirst4Images(final List<String> sourcesimages){
        L.d("LoaderTalkPhoto getFirst4Images()");
        int i = 0;
        List<String> images = new ArrayList<>();
        for(String sourceBitmap:sourcesimages){
            images.add(sourceBitmap);
            i++;
            if(i == 4){
                break;
            }
        }
        return images;
    }


    private class SetImageTask extends AsyncTask<String, Void, Integer> {
        private RoundedImageView imageView;
        private List<Bitmap> bitMaps = new ArrayList<>();
        private List<String> images;
        private Bitmap finalBitmap;
        private Integer position;


        public SetImageTask(RoundedImageView imageView, List<String> images, Integer position) {
            L.d("LoaderTalkPhoto SetImageTask()");
            this.imageView = imageView;
            this.images = images;
            this.position = position;

            imageView.setTag(position);
        }

        @Override
        protected Integer doInBackground(String... params) {
            L.d("LoaderTalkPhoto doInBackground() start");
            try {
                finalBitmap = (Bitmap) Cache.getInstance().getLru().get(position);
                L.d("LoaderTalkPhoto finalBitmap = " + finalBitmap);
                if(finalBitmap == null){
                    for(String url:images){
                        Bitmap bitmap = getBitmapFromURL(url);
                        bitMaps.add(bitmap);
                    }
                    if(images.size() == 1){
                        finalBitmap = bitMaps.get(0);
                    }else{
                        finalBitmap = BitmapManager.mergeBitmap(bitMaps, lineBetweenPhoto, widthPhoto);
                    }

                    if (finalBitmap != null) {
                        Cache.getInstance().getLru().put(position, finalBitmap);
                    } else {
                        L.d("LoaderTalkPhoto doInBackground() return 0");
                        return 0;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                L.d("LoaderTalkPhoto doInBackground() return 0");
                return 0;
            }
            L.d("LoaderTalkPhoto doInBackground() return 1");
            return 1;
        }

        @Override
        protected void onPostExecute(Integer result) {
            L.d("LoaderTalkPhoto onPostExecute() = " + result);
            if (result == 1 && imageView.getTag().equals(position)) {
                imageView.setImageBitmap(finalBitmap);
            }
            super.onPostExecute(result);
        }

        private Bitmap getBitmapFromURL(String src) {
            L.d("LoaderTalkPhoto getBitmapFromURL()");
            try {
                URL url = new URL(src);
                Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                return BitmapManager.getResizedBitmap(bitmap, widthPhoto, widthPhoto);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

    }
}
