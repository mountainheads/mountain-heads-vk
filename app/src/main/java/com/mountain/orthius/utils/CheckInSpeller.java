package com.mountain.orthius.utils;

import com.mountain.orthius.UILApplication;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.model.MistakesModel;
import com.mountain.orthius.utils.view.IAnalyticSpeller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mountain.orthius.utils.view.GetAllMessagesForDiaologManager.sortByValue;

/**
 * Created by spbiphones on 21.10.17.
 */

public class CheckInSpeller extends BaseManager implements IAnalyticSpeller {

    private OnAnalyticListener onServerResult;
    private List<Map<Integer, String>> messagesSortedByWeeks;
    private Integer counter = 0;
    private Map<Integer, Integer> mistakesPercents = new HashMap<>();
    private Map<String, Integer> topMistakes = new LinkedHashMap<>();
    private boolean isStop = false;

    List<String> trashList;

    @Override
    public void run(OnAnalyticListener onServerResult, List<Map<Integer, String>> messagesSortedByWeeks) {
        this.onServerResult = onServerResult;
        this.messagesSortedByWeeks = messagesSortedByWeeks;
        trashList =  Arrays.asList(trashWord);
        loadToSpeller();
    }

    private void loadToSpeller() {

        PauseRunnable pauseRunnable = new PauseRunnable(new PauseRunnable.OnThreadListener() {
            private PauseRunnable pauseRunnable;

            @Override
            public void onCreate(PauseRunnable pauseRunnable) {
                this.pauseRunnable = pauseRunnable;
            }

            @Override
            public void onCall(final int i) {
                if (isStop()) {
                    return;
                }
                UILApplication.getApi().checkText(messagesSortedByWeeks.get(i).get(i), "23").enqueue(new Callback<ArrayList<MistakesModel>>() {

                    @Override
                    public void onResponse(Call<ArrayList<MistakesModel>> call, Response<ArrayList<MistakesModel>> response) {
                        L.d("CheckInSpeller i = " + i + " length = " + messagesSortedByWeeks.get(i).get(i).length());
                      //  L.d("checkText onCall response = " + response.body().get(0));
                        if (response.body() != null && response.body().size() > 0) {


                            int percents = (int) (((float) response.body().size() / (float) countWords(messagesSortedByWeeks.get(i).get(i))) * 100);
                            mistakesPercents.put(i, percents);
                            L.d("CheckInSpeller percents = " + percents);

                            for (int j = 0; j < response.body().size(); j++){

                                if (response.body().get(j).getS().length > 0 && !trashList.contains(response.body().get(j).getWord()) && response.body().get(j).getWord().length() > 3){
                                    if(response.body().get(j).getWord().equals("айосе")){
                                        L.d("check айосе = " + response.body().get(j).getS()[0]);
                                    }
                                    if (topMistakes.containsKey(response.body().get(j).getWord())){
                                        topMistakes.put(response.body().get(j).getWord(), topMistakes.get(response.body().get(j).getWord()) + 1);
                                    } else {
                                        topMistakes.put(response.body().get(j).getWord(), 1);
                                    }

                                }

                            }

                        }

                        if (i == (messagesSortedByWeeks.size() - 1)) {
                            pauseRunnable.setFinish();

                        } else {
                            pauseRunnable.setResume();
                        }
                    }

                    @Override
                    public void onFailure(Call<ArrayList<MistakesModel>> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onFinish() {

                topMistakes = sortByValue(topMistakes);

                onServerResult.onSuccess();
                getResult();
            }
        }, 500);
        Thread thread = new Thread(pauseRunnable);
        thread.start();


    }
    public Map<Integer, Integer> getMistakesPercents(){
        return mistakesPercents;
    }

    @Override
    public boolean isStop() {
        return isStop;
    }

    @Override
    public void setStop(boolean isStop) {
        this.isStop = isStop;
    }

    @Override
    public Map<Integer, Integer> getResult() {
        return mistakesPercents;
    }

    @Override
    public ArrayList<String> getTopMistakes() {
        ArrayList<String> keys = new ArrayList<String>(topMistakes.keySet());
        ArrayList<String> top10Words = new ArrayList<>();
        for (int i = 0; i < 10; i ++){

            top10Words.add(keys.get(i));

            L.d("getAllWords value = " + topMistakes.get(keys.get(i)));

        }

        return top10Words;

    }


}
