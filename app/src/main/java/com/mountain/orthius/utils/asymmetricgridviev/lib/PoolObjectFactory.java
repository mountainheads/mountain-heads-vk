package com.mountain.orthius.utils.asymmetricgridviev.lib;

public interface PoolObjectFactory<T> {
  T createObject();
}
