package com.mountain.orthius.utils.asymmetricgridviev;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;


/**
 * Created by vladimir on 26.02.16.
 */
public class ViewHolder extends RecyclerView.ViewHolder {

    public final ImageView image;
    public final ImageView videoPlayIV;
    public final RelativeLayout videoPanelRL;
    public final TextView titleTV;
    public ViewHolder(ViewGroup parent, int viewType) {
        super(LayoutInflater.from(parent.getContext()).inflate(
                viewType == 0 ? R.layout.photo_item : R.layout.photo_item, parent, false));
        if (viewType == 0) {
            image = (ImageView) itemView.findViewById(R.id.imageView1);
            videoPlayIV = (ImageView) itemView.findViewById(R.id.videoPlayIV);
            videoPanelRL = (RelativeLayout) itemView.findViewById(R.id.videoPanelRL);
            titleTV = (TextView) itemView.findViewById(R.id.titleTV);
        } else {
            image = (ImageView) itemView.findViewById(R.id.imageView1);
            videoPlayIV = (ImageView) itemView.findViewById(R.id.videoPlayIV);
            videoPanelRL = (RelativeLayout) itemView.findViewById(R.id.videoPanelRL);
            titleTV = (TextView) itemView.findViewById(R.id.titleTV);
        }


        L.d("ViewHolder = " + viewType);
    }

    public void bind(DemoItem item) {

    }
}
