package com.mountain.orthius.utils;

import android.content.Context;
import android.graphics.Bitmap;

import com.makeramen.RoundedDrawable;
import com.makeramen.RoundedImageView;
import com.mountain.orthius.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by Andoid on 06.08.2014.
 */
public class PicassoCrutch {
    public static void loadRoundedImage(Context context, String uri, final RoundedImageView imageView){
        Picasso.with(context)
                .load(uri).placeholder(R.drawable.ic_account_box_56dp).fit().centerCrop().into(imageView, new Callback() {

            @Override
            public void onSuccess() {
                Bitmap bitmap = ((RoundedDrawable) imageView.getDrawable()).toBitmap();
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onError() {

            }
        });
    }
}
