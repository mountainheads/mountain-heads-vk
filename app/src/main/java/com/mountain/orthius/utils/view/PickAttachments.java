package com.mountain.orthius.utils.view;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.utils.image.BitmapManager;
import com.mountain.orthius.view.adapter.AttachmentsCheckedPhotoAdapter;
import com.mountain.orthius.view.adapter.HorizontalAttachmentsAdapter;
import com.nononsenseapps.filepicker.FilePickerActivity;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by vladimir on 12.09.16.
 */
public class PickAttachments {
    public static final int RESULT_OK = -1;

    private final int REQUEST_IMAGE_CAPTURE = 1;
    private final int REQUEST_CODE_PHOTO = 2;
    private final int REQUEST_CODE_DOCUMENT = 3;
    private final int REQUEST_PLACE = 4;
    private final int REQUEST_CONTACT = 5;

    private Activity act;

    private EditText messageET;

    private BottomSheetBehavior<View> behavior;
    private AttachmentsCheckedPhotoAdapter photoAttachAdapter;

    private HorizontalAttachmentsAdapter attachmentsAdapter;
    private RecyclerView attachmentsTWV;
    private RecyclerView photosTWV;
    private List<Map<String, Object>> newAttachments = new ArrayList<>();
    private LatLng attachToMessagePlace;

    public static Boolean isTypePost = false;

    public PickAttachments(Activity activity, Boolean isTypePost){
        PickAttachments.isTypePost = isTypePost;
        act = activity;
        attachmentsTWV = (RecyclerView) act.findViewById(R.id.attachmentsTWV);
        photosTWV = (RecyclerView) act.findViewById(R.id.photosTWV);

        DefaultItemAnimator animator = new DefaultItemAnimator() {
            @Override
            public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
                return true;
            }
        };
        photosTWV.setItemAnimator(animator);
        getAllLocalPhotosForAttachments();
        CoordinatorLayout rootCL = (CoordinatorLayout) act.findViewById(R.id.rootCL);
        View bottomSheet = rootCL.findViewById(R.id.bottom_sheet);

        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if(newState == BottomSheetBehavior.STATE_EXPANDED){
//                    newAttachments = new ArrayList<>();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        behavior.setHideable(true);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        attachmentsAdapter = new HorizontalAttachmentsAdapter(act, attachmentsTWV);
        attachmentsTWV.setAdapter(attachmentsAdapter);

        attachmentsAdapter.setOnItemClickListener(new HorizontalAttachmentsAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                attachmentsAdapter.removeItem(position);
            }
        });

        messageET = (EditText) act.findViewById(R.id.messageET);

        LinearLayout attachDoneLL = (LinearLayout) act.findViewById(R.id.attachDoneLL);
        attachDoneLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickDone();
            }
        });


        LinearLayout cameraLL = (LinearLayout) act.findViewById(R.id.cameraLL);
        cameraLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        LinearLayout photoLL = (LinearLayout) act.findViewById(R.id.photoLL);
        photoLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        LinearLayout videoLL = (LinearLayout) act.findViewById(R.id.videoLL);
        videoLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        LinearLayout audioLL = (LinearLayout) act.findViewById(R.id.audioLL);
        audioLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        LinearLayout docLL = (LinearLayout) act.findViewById(R.id.docLL);
        docLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runPickDocument();
            }
        });

        LinearLayout placeLL = (LinearLayout) act.findViewById(R.id.placeLL);
        placeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                try {
                    act.startActivityForResult(builder.build(getActivity()), REQUEST_PLACE);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        LinearLayout contactLL = (LinearLayout) act.findViewById(R.id.contactLL);
        contactLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickContact();
            }
        });
    }

    private void pickContact() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        act.startActivityForResult(contactPickerIntent, REQUEST_CONTACT);
    }

    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null ;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = act.getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            // Set the value to the textviews
            messageET.getText().append("\n" + name + ": " + phoneNo);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void runPickDocument(){
        Intent i = new Intent(getActivity(), FilePickerActivity.class);
        // This works if you defined the intent filter
        // Intent i = new Intent(Intent.ACTION_GET_CONTENT);

        // Set these depending on your use case. These are the defaults.
//        i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, true);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);

        // Configure initial directory by specifying a String.
        // You could specify a String like "/storage/emulated/0/", but that can
        // dangerous. Always use Android's API calls to get paths to the SD-card or
        // internal memory.
        i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

        act.startActivityForResult(i, REQUEST_CODE_DOCUMENT);
    }

    private void clickDone(){
        addAttachments(photoAttachAdapter.getSelected());
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                attachmentsAdapter.addItems(newAttachments);
                newAttachments = new ArrayList<>();
                photoAttachAdapter.setAllUnselected();
                behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });
    }



    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(act.getPackageManager()) != null) {
            act.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            String path = BitmapManager.storeImage(imageBitmap);

            Map tempPhotoByCamera = new HashMap<>();

            tempPhotoByCamera.put(AttachmentsCheckedPhotoAdapter.IMAGE,path);
            tempPhotoByCamera.put(AttachmentsCheckedPhotoAdapter.TYPE, AttachmentsCheckedPhotoAdapter.TYPE_PHOTO);
            addAttachment(tempPhotoByCamera);
            clickDone();
        }
        if (requestCode == REQUEST_CODE_PHOTO && resultCode == RESULT_OK) {

        }

        if (requestCode == REQUEST_CODE_DOCUMENT && resultCode == RESULT_OK) {
            L.d("REQUEST_CODE_DOCUMENT = " + data.getBooleanExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false));
            if (data.getBooleanExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false)) {
                // For JellyBean and above
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ClipData clip = data.getClipData();

                    if (clip != null) {
                        for (int i = 0; i < clip.getItemCount(); i++) {
                            Uri uri = clip.getItemAt(i).getUri();
                            // Do something with the URI
                            addDocument(uri);
                        }
                    }
                    // For Ice Cream Sandwich
                } else {
                    ArrayList<String> paths = data.getStringArrayListExtra
                            (FilePickerActivity.EXTRA_PATHS);

                    if (paths != null) {
                        for (String path: paths) {
                            Uri uri = Uri.parse(path);
                            // Do something with the URI
                            addDocument(uri);
                        }
                    }
                }

            } else {
                Uri uri = data.getData();
                addDocument(uri);
                // Do something with the URI
            }
        }
        if (requestCode == REQUEST_PLACE && resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(data, act);

            attachToMessagePlace = place.getLatLng();

            attachmentsAdapter.clearGeoPlaceItem();
            Map tempPhotoByCamera = new HashMap<>();
            tempPhotoByCamera.put(AttachmentsCheckedPhotoAdapter.TYPE, HorizontalAttachmentsAdapter.TYPE_PLACE);
            addAttachment(tempPhotoByCamera);
            clickDone();
        }

        if (requestCode == REQUEST_CONTACT && resultCode == RESULT_OK) {
            contactPicked(data);
            clickDone();
        }
    }

    private void addDocument(final Uri path){

    }

    private void addAttachments(List<Map<String,Object>> newList){
        if(newAttachments.size() + newList.size() >= 10){
            Toast.makeText(act, "Максимум 10 вложений", Toast.LENGTH_LONG).show();
            return;
        }
        for(Map<String,Object> item:newList){
            addAttachment(item);
        }
    }

    private void addAttachment(Map<String,Object> item){
        L.d("addAttachment = " + item);
        if(newAttachments.size() < 10){
            newAttachments.add(item);
        }else{
            Toast.makeText(act, "Максимум 10 вложений", Toast.LENGTH_LONG).show();
        }
    }

    private void getAllLocalPhotosForAttachments() {
//        Uri uri;
//        Cursor cursor;
//        int column_index_data, column_index_folder_name;
//        List<Map<String,Object>> listOfAllImages = new ArrayList<>();
//        String absolutePathOfImage = null;
//        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//
//        String[] projection = { MediaStore.MediaColumns.DATA,
//                MediaStore.Images.Media.BUCKET_DISPLAY_NAME };
//
//        cursor = getActivity().getContentResolver().query(uri, projection, null,
//                null, null);
//
//        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//        column_index_folder_name = cursor
//                .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
//        while (cursor.moveToNext()) {
//            absolutePathOfImage = cursor.getString(column_index_data);
//
//            Map<String,Object> item = new HashMap<>();
//            item.put(AttachmentsCheckedPhotoAdapter.IMAGE,absolutePathOfImage);
//            item.put(AttachmentsCheckedPhotoAdapter.TYPE, AttachmentsCheckedPhotoAdapter.TYPE_PHOTO);
//            listOfAllImages.add(item);
//        }
//        Collections.reverse(listOfAllImages);
//        photoAttachAdapter = new AttachmentsCheckedPhotoAdapter(act, photosTWV);
//        photoAttachAdapter.addItems(listOfAllImages);
//        photosTWV.setAdapter(photoAttachAdapter);
    }

    private Activity getActivity(){
        return act;
    }

    public String getAttachmentsToSend() {
        return attachmentsAdapter.getAttachmentsToSend();
    }

    public void clearAll() {
        attachmentsAdapter.clearAll();
    }

    public void toggleState(){
        if(behavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
            behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }else{
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
    }

    public LatLng getAttachToMessagePlace() {
        return attachToMessagePlace;
    }
    public void clearAttachToMessagePlace() {
        attachToMessagePlace = null;
    }

    public void setCollapsed() {
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }
}
