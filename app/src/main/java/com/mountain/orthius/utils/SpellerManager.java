package com.mountain.orthius.utils;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.mountain.orthius.UILApplication;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.model.MistakesModel;
import com.mountain.orthius.utils.view.ColorTextView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class SpellerManager {
    private final Boolean editedMistakes;
    private String text;
    private ColorTextView mistakesText;
    private ArrayList<MistakesModel> mistakes;
    private int currentIndex = 0;
    private TextView rightWordTV;
    private EditText myWordET;
    private TextView variantsTV;
    private LinearLayout variantsLL;

    public SpellerManager(final String text, final ColorTextView mistakesText, TextView rightWordTV, EditText myWordET, TextView variantsTV, LinearLayout variantsLL, Boolean editedMistakes) {
        this.text = text;
        this.mistakesText = mistakesText;
        this.rightWordTV = rightWordTV;
        this.myWordET = myWordET;
        this.variantsTV = variantsTV;
        this.variantsLL = variantsLL;
        this.editedMistakes = editedMistakes;
    }

    public void checkText() {
        L.d("checkText text.length() = " + text.length());
        UILApplication.getApi().checkText(text).enqueue(new Callback<ArrayList<MistakesModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MistakesModel>> call, Response<ArrayList<MistakesModel>> response) {
                L.d("checkText response = " + response);
                if (response.body() != null) {
                    mistakesText.setText(text);
                    mistakesText.textSetted();
                    L.d("checkText = " + response.body().size());

                    mistakes = response.body();

                    for (MistakesModel mistake : mistakes) {
                        mistakesText.findAndSetStrColor(mistake.getWord(), "#f26180");

                        L.d("checkText mistake.getWord() = " + mistake.getWord());
                    }
                    if(editedMistakes){
                        checkCurrentWord();
                    }else{
                        checkCurrentWordNoFix();
                    }


                } else {
                    Toast.makeText(UILApplication.context, "Проверьте интернет соединение", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MistakesModel>> call, Throwable t) {

            }
        });
    }

    private boolean isGoNextWord() {
        if (currentIndex == mistakes.size()) {
            Toast.makeText(UILApplication.context, "Все слова исправлены", Toast.LENGTH_LONG).show();
            variantsLL.setVisibility(View.INVISIBLE);
            return false;
        } else return true;
    }

    public void checkNextWord() {
        if (!isGoNextWord()) {
            return;
        }
        if (currentIndex > 0) {
            mistakesText.findAndFixCurrentMistake(mistakes.get(currentIndex).getWord(), mistakes.get(currentIndex).getWord());
        }
        updateIndex();

    }

    public void checkNextWordNoFix() {
        if (currentIndex < (mistakes.size() - 1)) {
            if (!isGoNextWord()) {
                return;
            }

            mistakesText.findCurrentMistake(mistakes.get(currentIndex).getWord(), mistakes.get(currentIndex).getWord());

            updateIndexNoFix();
        }
    }

    public void checkPrevWordNoFix() {
        if (currentIndex > 0) {
            if (!isGoNextWord()) {
                return;
            }

            mistakesText.findCurrentMistake(mistakes.get(currentIndex).getWord(), mistakes.get(currentIndex).getWord());


            currentIndex -= 1;
            checkCurrentWordNoFix();
        }
    }

    public void updateIndex() {
        currentIndex += 1;
        checkCurrentWord();
    }

    public void updateIndexNoFix() {
        currentIndex += 1;
        checkCurrentWordNoFix();
    }

    public void fixCurrentMistake() {
        if (!isGoNextWord()) {
            return;
        }
        String[] word = mistakes.get(currentIndex).getS();
        if (word != null && word.length > 0) {
            mistakesText.findAndFixCurrentMistake(mistakes.get(currentIndex).getWord(), word[0]);
        } else {
            if (myWordET.getText().length() > 0) {
                mistakesText.findAndFixCurrentMistake(mistakes.get(currentIndex).getWord(), myWordET.getText().toString());
            } else {
                Toast.makeText(UILApplication.context, "Введите слово для исправления", Toast.LENGTH_LONG).show();
            }
        }
        updateIndex();
    }

    public void checkCurrentWord() {
        if (!isGoNextWord()) {
            return;
        }
        mistakesText.findAndSetCurrent(mistakes.get(currentIndex).getWord(), "#f26180");
        String[] word = mistakes.get(currentIndex).getS();
        if (word != null && word.length > 0) {
            myWordET.setVisibility(View.GONE);
            rightWordTV.setVisibility(View.VISIBLE);
            variantsTV.setText("Варианты");
            rightWordTV.setText(word[0]);
        } else {
            variantsTV.setText("Нет вариантов замены");
            myWordET.setVisibility(View.VISIBLE);
            rightWordTV.setVisibility(View.GONE);
        }
    }

    public void fixMistakes() {
        if (mistakes != null && mistakes.size() > 0) {
            mistakesText.readyToFixMistakes(text);
            for (MistakesModel mistake : mistakes) {
                if (mistake.getS()[0] != null) {
                    mistakesText.findAndFixMistake(mistake.getWord(), mistake.getS()[0]);
                }
            }
        }
    }

    public void checkCurrentWordNoFix() {
        if (!isGoNextWord()) {
            return;
        }
        mistakesText.findAndSetCurrent(mistakes.get(currentIndex).getWord(), "#f26180");
        String[] word = mistakes.get(currentIndex).getS();
        if (word != null && word.length > 0) {
            myWordET.setVisibility(View.GONE);
            rightWordTV.setVisibility(View.VISIBLE);
            variantsTV.setText("Варианты");
            rightWordTV.setText(word[0]);
        } else {
            variantsTV.setText("Нет вариантов замены");
            rightWordTV.setText("");
        }
    }
}



