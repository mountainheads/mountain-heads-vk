package com.mountain.orthius.utils;


/**
 * Created by vladimir on 26.05.16.
 */
public class PauseRunnable implements Runnable {
    private final OnThreadListener onThreadListener;

    public interface OnThreadListener {
        void onCreate(PauseRunnable pauseRunnable);

        void onCall(int i);

        void onFinish();
    }

    private Object mPauseLock;
    private boolean mPaused;
    private boolean mFinished;
    private int i = 0;
    private long delay;


    public PauseRunnable(OnThreadListener onThreadListener) {
        mPauseLock = new Object();
        mPaused = false;
        mFinished = false;
        this.onThreadListener = onThreadListener;
        onThreadListener.onCreate(this);
        delay = 500;
    }

    public PauseRunnable(OnThreadListener onThreadListener, long delay) {
        mPauseLock = new Object();
        mPaused = false;
        mFinished = false;
        this.onThreadListener = onThreadListener;
        onThreadListener.onCreate(this);
        this.delay = delay;
    }

    public void run() {
        while (!mFinished) {
            // Do stuff.
            synchronized (mPauseLock) {
                while (mPaused) {
                    try {
                        mPauseLock.wait();
                    } catch (InterruptedException e) {
                    }
                }
                setPause();
                onThreadListener.onCall(i);
                i++;
            }
        }
    }

    /**
     * Call this on pause.
     */
    public void setPause() {
        synchronized (mPauseLock) {
            mPaused = true;
        }
    }

    public void setFinish() {
        mFinished = true;
        onThreadListener.onFinish();
    }

    /**
     * Call this on resume.
     */
    public void setResume() {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (mPauseLock) {
            mPaused = false;
            mPauseLock.notifyAll();
        }
    }

}





