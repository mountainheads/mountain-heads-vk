package com.mountain.orthius.utils;

import java.util.Map;

/**
 * Created by alexeybalabanov on 25.06.15.
 */
public interface OnAnalyticListener {
    public void onSuccess();

    public void onStep(String user);
    public void onStep(Map<String, Object> photo);
    public void onStep(Map<String, Object> photo, Integer id);
    public void onProgress(Integer indexUser, Integer lengthUser);

    public void onError();
}
