package com.mountain.orthius.utils.view;

import com.mountain.orthius.utils.OnAnalyticListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by spbiphones on 21.10.17.
 */

public interface IAnalyticSpeller {
        public void run(final OnAnalyticListener onServerResult, List<Map<Integer, String>> messagesSortedByWeeks);
        public boolean isStop();
        public void setStop(boolean isStop);

        public Map<Integer, Integer> getResult();
        public ArrayList<String> getTopMistakes();

}
