package com.mountain.orthius.utils;

/**
 * Created by alexeybalabanov on 31.03.15.
 */
public class RussianLangUtil {
    public static String getAge(String age){
        if(age == null || age.isEmpty()){
            return "";
        }
        String lastChar;
        try {
            lastChar = age.substring(age.length()-1,1);
        }catch (Exception e){
            return age;
        }

        if(age.length() > 1){
            String preLastChar = age.substring(age.length()-2,1);
            if(preLastChar.equals("1")){
                return age + " лет";
            }
        }
        if(lastChar.equals("1")){
            return age + " год";
        }
        if(lastChar.equals("2") || lastChar.equals("3") || lastChar.equals("4")){
            return age + " года";
        }else{
            return age + " лет";
        }
    }
}
