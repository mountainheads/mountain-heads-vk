package com.mountain.orthius.utils;

/**
 * Created by alexeybalabanov on 28.06.15.
 */
public class ZodiacUtils {
    public static final int January = 1;
    public static final int February = 2;
    public static final int March = 3;
    public static final int April = 4;
    public static final int May = 5;
    public static final int June = 6;
    public static final int July = 7;
    public static final int August = 8;
    public static final int September = 9;
    public static final int October = 10;
    public static final int November = 11;
    public static final int December = 12;

    public static String getZodiac(String dateStr){//dd.MM.yyyy
        String[] parts = dateStr.split("\\.");
        int day = Integer.parseInt(parts[0]);
        int month = Integer.parseInt(parts[1]);
        switch (month){
            case January:
                if(day <= 19){
                    return "Козерог";
                }else{
                    return "Водолей";
                }

            case February:
                if(day <= 18){
                    return "Водолей";
                }else{
                    return "Рыбы";
                }
            case March:
                if(day <= 20){
                    return "Рыбы";
                }else{
                    return "Овен";
                }
            case April:
                if(day <= 19){
                    return "Овен";
                }else{
                    return "Телец";
                }
            case May:
                if(day <= 20){
                    return "Телец";
                }else{
                    return "Близнецы";
                }
            case June:
                if(day <= 20){
                    return "Близнецы";
                }else{
                    return "Рак";
                }
            case July:
                if(day <= 22){
                    return "Рак";
                }else{
                    return "Лев";
                }
            case August:
                if(day <= 22){
                    return "Лев";
                }else{
                    return "Дева";
                }
            case September:
                if(day <= 22){
                    return "Дева";
                }else{
                    return "Весы";
                }
            case October:
                if(day <= 22){
                    return "Весы";
                }else{
                    return "Скорпион";
                }
            case November:
                if(day <= 21){
                    return "Скорпион";
                }else{
                    return "Стрелец";
                }
            case December:
                if(day <= 21){
                    return "Стрелец";
                }else{
                    return "Козерог";
                }


            default:
                return "Зодиак не определить";
        }
        /*
        * 22 декабря — 19 января 12 - 1
        * 20 января — 18 февраля 1 - 2
        * 19 февраля — 20 марта 2 - 3
        * 21 марта — 19 апреля 3 - 4
        * 20 апреля — 20 мая 4 - 5
        * 21 мая — 20 июня 5 - 6
        * 21 июня — 22 июля 6 - 7
        * 23 июля — 22 августа 7 - 8
        * 23 августа — 22 сентября 8 - 9
        * 23 сентября — 22 октября 9 - 10
        * 23 октября — 21 ноября 10 - 11
        * 22 ноября — 21 декабря 11 - 12
        *
        *
        * */

    }
}
