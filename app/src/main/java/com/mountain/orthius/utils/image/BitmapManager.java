package com.mountain.orthius.utils.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Environment;

import com.mountain.orthius.UILApplication;
import com.mountain.orthius.loging.L;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class BitmapManager {
    static public Bitmap mergeBitmap(List<Bitmap> bitmaps, Integer lineBetweenPhoto, Integer widthPhoto) {
        L.d("lineBetweenPhoto = " + lineBetweenPhoto);
        L.d("widthPhoto = " + widthPhoto);
        L.d("bitmaps.size() = " + bitmaps.size());
        if(bitmaps.size() == 1){
            return bitmaps.get(0);
        }
        Bitmap bmOverlay = Bitmap.createBitmap(bitmaps.get(0).getWidth(), bitmaps.get(0).getHeight(), bitmaps.get(0).getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        if(bitmaps.size() == 2){
            Bitmap bitmap1 = cropHalfBitmap(bitmaps.get(0));
            Bitmap bitmap2 = cropHalfBitmap(getResizedBitmap(bitmaps.get(1), widthPhoto,widthPhoto));
            canvas.drawBitmap(bitmap1, new Matrix(), null);
            canvas.drawBitmap(bitmap2, bitmap1.getWidth() + lineBetweenPhoto, 0, null);
        }
        if (bitmaps.size() == 3){
            Bitmap bitmap1 = cropHalfBitmap(bitmaps.get(0));
            Bitmap bitmap2 = getResizedBitmap(bitmaps.get(1),widthPhoto/2,widthPhoto/2);
            Bitmap bitmap3 = getResizedBitmap(bitmaps.get(2),widthPhoto/2,widthPhoto/2);


            canvas.drawBitmap(bitmap1, new Matrix(), null);
            canvas.drawBitmap(bitmap2, bitmap1.getWidth() + lineBetweenPhoto, 0, null);
            canvas.drawBitmap(bitmap3, bitmap1.getWidth() + lineBetweenPhoto, bitmap2.getHeight() + lineBetweenPhoto, null);
        }
        if (bitmaps.size() == 4){

            Bitmap bitmap1 = getResizedBitmap(bitmaps.get(0),widthPhoto/2,widthPhoto/2);
            Bitmap bitmap2 = getResizedBitmap(bitmaps.get(1),widthPhoto/2,widthPhoto/2);
            Bitmap bitmap3 = getResizedBitmap(bitmaps.get(2),widthPhoto/2,widthPhoto/2);
            Bitmap bitmap4 = getResizedBitmap(bitmaps.get(3),widthPhoto/2,widthPhoto/2);

            canvas.drawBitmap(bitmap1, new Matrix(), null);
            canvas.drawBitmap(bitmap2, 0, bitmap1.getHeight() + lineBetweenPhoto, null);
            canvas.drawBitmap(bitmap3, bitmap1.getWidth() + lineBetweenPhoto, 0, null);
            canvas.drawBitmap(bitmap4, bitmap1.getWidth() + lineBetweenPhoto, bitmap1.getHeight() + lineBetweenPhoto, null);
        }
        return bmOverlay;
    }

    static public Bitmap cropHalfBitmap(Bitmap srcBmp){
        Bitmap dstBmp;
        dstBmp = Bitmap.createBitmap(
                srcBmp,
                srcBmp.getWidth()/4,
                0,
                srcBmp.getWidth()/2,
                srcBmp.getHeight()
        );

        return dstBmp;
    }

    static public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        L.d("newWidth = " + newWidth);
        L.d("newHeight = " + newHeight);
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public interface OnBimapLoadedListener{
        void onLoaded(Bitmap bitmap);
        void onError();
    }
    static public void getBitmapFromURL(final String strURL, final OnBimapLoadedListener onBimapLoadedListener) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(strURL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(input);
                    onBimapLoadedListener.onLoaded(myBitmap);
                } catch (IOException e) {
                    onBimapLoadedListener.onError();
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    public static String storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            L.d("Error creating media file, check storage permissions: ");// e.getMessage());
            return null;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
            return pictureFile.getAbsolutePath();
        } catch (FileNotFoundException e) {
            L.d("File not found: " + e.getMessage());
        } catch (IOException e) {
            L.d("Error accessing file: " + e.getMessage());
        }
        return null;
    }
    private static File getOutputMediaFile(){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + UILApplication.context.getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName="MI_"+ timeStamp +".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }
}
