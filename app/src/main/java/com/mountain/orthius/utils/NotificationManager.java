package com.mountain.orthius.utils;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;

import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;
import com.mountain.orthius.utils.image.BitmapManager;
import com.mountain.orthius.view.activity.ChatActivity;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;



/**
 * Created by alexeybalabanov on 30.10.14.
 */
public class NotificationManager {
    public interface NotificationListener{
        public void onNotification();
    }

    private static NotificationListener notificationListener;

    static public void setNotificationListener(NotificationListener mNotificationListener) {
        notificationListener = mNotificationListener;
    }

    public static void sendParseNotifMessage(final Context context, final Bundle data) {
        if(context == null){
            return;
        }

        final String writerId = data.get("uid") != null ? data.get("uid")+"" : data.get("from_id")+"";
        if(writerId.contains("-")){
            final VKRequest request = new VKRequest("groups.getById", VKParameters.from("group_ids", writerId.replace("-",""), "fields", "members_count,description,is_subscribed"));

            request.setRequestListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(final VKResponse responseJson) {
                    super.onComplete(responseJson);
                    final Map<String, Object> group;
                    try {
                        group = JsonUtils.jsonToMap(responseJson.json.getJSONArray("response").getJSONObject(0));

                        String photo = null;
                        if(group.get("photo_200") != null){
                            photo = group.get("photo_200").toString();
                        }else if(group.get("photo_100") != null){
                            photo = group.get("photo_100").toString();
                        }else{
                            photo = group.get("photo_50").toString();
                        }

                        final String finalPhoto = photo;
                        BitmapManager.getBitmapFromURL(photo, new BitmapManager.OnBimapLoadedListener() {
                            @Override
                            public void onLoaded(Bitmap bitmap) {
                                try {
                                    // 1-я часть
                                    android.app.NotificationManager nm = (android.app.NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

                                    Intent intent;

                                    String text;
                                    if(!data.getString("type").equals("new_post")){
                                        intent = new Intent(context, ChatActivity.class);
                                        text = data.getString("text");
                                        intent.putExtra(ChatActivity.USER_ID, data.get("uid")+"");
                                        intent.putExtra(ChatActivity.USER_NAME, group.get("name")+"");
                                        intent.putExtra(ChatActivity.USER_AVATAR, finalPhoto);
                                    }else{
                                        return;
                                    }

                                    PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);


                                    NotificationCompat.Builder builder = new NotificationCompat.Builder(
                                            context);
                                    Notification notif = builder.setContentIntent(pIntent)
                                            .setSmallIcon(R.mipmap.ic_launcher)
                                            .setLargeIcon(bitmap)
                                            .setTicker(group.get("name").toString())
                                            .setAutoCancel(true).setContentTitle(group.get("name")+"")
                                            .setContentText(text).build();

                                    // ставим флаг, чтобы уведомление пропало после нажатия
                                    notif.flags |= Notification.FLAG_AUTO_CANCEL;

                                    // отправляем
                                    nm.notify(1, notif);
                                    if(notificationListener != null){
                                        notificationListener.onNotification();
                                    }
                                    playSound(context);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onError() {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onError(VKError error) {
                    L.d("error.apiError errorCode = " + error.errorCode + " errorMessage = " + error.errorMessage + " errorReason = " + error.errorReason);
                    if (error.apiError != null && error.apiError.errorCode == 6) {
                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                request.repeat();
                            }
                        }, 700);
                    }
                }
            });
            request.start();
        }else{
            VKRequest vkRequest = new VKRequest("users.get", VKParameters.from("user_ids",data.get("uid") != null ? data.get("uid") : data.get("from_id"), "fields", "photo_200,photo_100,photo_50"));
            vkRequest.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    super.onComplete(response);
                    try {
                        JSONObject responseObject = response.json;
                        Map<String, Object> responseMap = JsonUtils.jsonToMap(responseObject);
                        List<Map<String, Object>> users = (List<Map<String, Object>>) responseMap.get("response");
                        final Map<String, Object> user = users.get(0);
                        String photo = null;
                        if(user.get("photo_200") != null){
                            photo = user.get("photo_200").toString();
                        }else if(user.get("photo_100") != null){
                            photo = user.get("photo_100").toString();
                        }else{
                            photo = user.get("photo_50").toString();
                        }

                        final String finalPhoto = photo;
                        BitmapManager.getBitmapFromURL(photo, new BitmapManager.OnBimapLoadedListener() {
                            @Override
                            public void onLoaded(Bitmap bitmap) {
                                try {
                                    // 1-я часть
                                    android.app.NotificationManager nm = (android.app.NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

                                    Intent intent = null;

                                    String text = "";
                                    if(!data.getString("type").equals("new_post")){
                                        intent = new Intent(context, ChatActivity.class);
                                        text = data.getString("text");
                                        intent.putExtra(ChatActivity.USER_ID, data.get("uid").toString());
                                        intent.putExtra(ChatActivity.USER_NAME, user.get("first_name") + " " + user.get("last_name"));
                                        intent.putExtra(ChatActivity.USER_AVATAR, finalPhoto);
                                    }else{
                                        return;
                                    }

                                    PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);


                                    NotificationCompat.Builder builder = new NotificationCompat.Builder(
                                            context);
                                    Notification notif = builder.setContentIntent(pIntent)
                                            .setSmallIcon(R.mipmap.ic_launcher)
                                            .setLargeIcon(bitmap)
                                            .setTicker(user.get("first_name") + " " + user.get("last_name"))
                                            .setAutoCancel(true).setContentTitle(user.get("first_name") + " " + user.get("last_name"))
                                            .setContentText(text).build();

                                    // ставим флаг, чтобы уведомление пропало после нажатия
                                    notif.flags |= Notification.FLAG_AUTO_CANCEL;

                                    // отправляем
                                    nm.notify(1, notif);
                                    if(notificationListener != null){
                                        notificationListener.onNotification();
                                    }
                                    playSound(context);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onError() {

                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }
    public static void playSound(Context context){
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}