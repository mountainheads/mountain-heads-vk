package com.mountain.orthius.utils.view;

import android.os.Handler;
import android.os.Looper;
import android.util.Pair;

import com.mountain.orthius.loging.L;
import com.mountain.orthius.utils.BaseManager;
import com.mountain.orthius.utils.CheckInSpeller;
import com.mountain.orthius.utils.DateFormatter;
import com.mountain.orthius.utils.IAnalyticManager;
import com.mountain.orthius.utils.JsonUtils;
import com.mountain.orthius.utils.OnAnalyticListener;
import com.mountain.orthius.utils.PauseRunnable;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Created by alexeybalabanov on 06.05.15.
 */
public class GetAllMessagesForDiaologManager extends BaseManager implements IAnalyticManager {
    public static final String LIKED_USER = "likedUser";
    public static final String FIND_PHOTOS = "photo";
    public static final String FIND_POSTS = "post";
    private boolean isStop = false;
    private String findType = FIND_PHOTOS;
    List<Map<String,Object>> items = new ArrayList<>();
    List<Map<String,Object>> outMessagesItems = new ArrayList<>();
    List<Map<Integer, String>> messagesSortedByWeeks = new ArrayList<>();
    public Integer countMessages = 0;
    public ArrayList<String> userIds;
    private int counter = 0;
    private ArrayList<String> topMistakes;
    private IAnalyticSpeller checkInSpeller;
    List<String> trashList;
    public Integer myMessages = 0;
    public Integer otherMessages = 0;

    public Integer myWords = 0;
    public Integer otherWords = 0;

    public Map<String,Integer> myWordsList = new HashMap<>();
    public Map<String,Integer> otherWordsList = new HashMap<>();
private  Map<Integer, Integer> mistakesPercents;
    public Map<String,Integer> myCountAttachments = new HashMap<>();
    public Map<String,Integer> otherCountAttachments = new HashMap<>();
    private long dateThreeMonthsAgo ;
    private Map<String, Integer> allOutWords = new LinkedHashMap<>();
    private Handler handler = new Handler(Looper.getMainLooper());
    private OnAnalyticListener onServerResult;


    public void run(final OnAnalyticListener onServerResult, final ArrayList<String> userIds) {
        this.onServerResult = onServerResult;
        this.userIds = userIds;
        trashList =  Arrays.asList(trashWord);
        this.dateThreeMonthsAgo = DateFormatter.dateThreeMothAgo();
        PauseRunnable pr = new PauseRunnable(new PauseRunnable.OnThreadListener() {
            private PauseRunnable pauseRunnable;
            @Override
            public void onCreate(PauseRunnable pauseRunnable) {
                this.pauseRunnable = pauseRunnable;
            }

            @Override
            public void onCall(final int i) {
                if (isStop()) {
                    return;
                }
                final VKRequest request = new VKRequest("execute.getMessagesForDialog",  VKParameters.from("user_id", userIds.get(i)));
//        VKRequest request = new VKRequest("likes.isLiked", VKParameters.from(VKApiConst.USER_ID, id,VKApiConst.OWNER_ID, ConverterUtil.getIntFromStringDouble(photo.get("owner_id").toString()), "item_id", ConverterUtil.getIntFromStringDouble(photo.get("id").toString()), "type","photo"));
                request.setRequestListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(final VKResponse response) {
                        super.onComplete(response);
                        final Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                if (isStop()) {
                                    return;
                                }
                                L.d("VKResponse response = " + response.responseString);
                                if (isStop()) {
                                    return;
                                }
                                L.d("response = " + response.responseString);

                                try{
                                    JSONObject responseObject = response.json.getJSONObject("response");
                                    JSONArray itemsArray = responseObject.getJSONArray("items");


                                    List<Map<String, Object>> itemsPart = (List<Map<String, Object>>) JsonUtils.jsonToMap(responseObject).get("items");



                                    for (Map<String,Object> item: itemsPart) {
                                        if (item.get("out").toString().equals("1") && dateThreeMonthsAgo <= Long.valueOf(item.get("date").toString())){
                                            outMessagesItems.add(item);

                                            countWords(item.get("body").toString(), allOutWords, trashList);

                                        }
                                    }

                                    items.addAll(itemsPart);

                                    L.d("checkInSpeller outMessagesItems size = " + outMessagesItems.size());

                                    Integer currentMessagesCount =  (int) JsonUtils.jsonToMap(responseObject).get("currentMessagesCount");
                                    onServerResult.onProgress(i*4800 + currentMessagesCount,0);
                                    countMessages +=currentMessagesCount;

                                    //parseMessageToStatistic(itemsPart, userIds.get(i));


                                    if((userIds.size() - 1) == i || counter == 4){
                                        pauseRunnable.setFinish();
                                    }else{

                                        pauseRunnable.setResume();
                                    }
                                    if (currentMessagesCount > 2000){
                                        counter += 1;
                                    }
                                    L.d("currentMessagesCount = " + currentMessagesCount + " userID = " + userIds.get(i) + " itemsCount = " + items.size());
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        });
                        thread.start();
                    }

                    @Override
                    public void onError(VKError error) {
                        if(error.apiError != null && error.apiError.errorCode == 2
                                ){
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    request.repeat();
                                }
                            }, 1000);
                        }
                        L.d("error.apiError errorCode = " + error.errorCode + " errorMessage = " + error.errorMessage + " errorReason = " + error.errorReason);
                    }
                });
                request.start();
            }

            @Override
            public void onFinish() {
//                otherWordsList = sortByValue(otherWordsList);
//                myWordsList = sortByValue(myWordsList);
                allOutWords = sortByValue(allOutWords);

                checkInSpeller();


            }
        }, 1000);
        Thread thread = new Thread(pr);
        thread.start();
    }

    @Override
    public boolean isStop() {
        return isStop;
    }

    @Override
    public void setStop(boolean isStop) {
        this.isStop = isStop;
        if (checkInSpeller != null){
            checkInSpeller.setStop(true);
        }
    }

    @Override
    public Map<Integer, Integer> getResult() {
        return mistakesPercents;
    }
    @Override
    public ArrayList<String> getAllWords() {

       ArrayList<String> keys = new ArrayList<String>(allOutWords.keySet());
        ArrayList<String> top10Words = new ArrayList<>();
        for (int i = 0; i < 10; i ++){

            top10Words.add(keys.get(i));

            L.d("getAllWords value = " + allOutWords.get(keys.get(i)));

        }

        return top10Words;
    }
    @Override
    public ArrayList<String> getTopMistakes() {
        return topMistakes;
    }
    @Override
    public int getAllOutWordsCount() {
        return allOutWords.size();
    }

    public void setFindType(String findType) {
        this.findType = findType;
    }

    private void checkInSpeller(){

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<Pair> weeks = DateFormatter.createWeeks();

                for (int i = 0; i < 12; i++) {

                    L.d("messagesSortedByWeeks checkInSpeller i = " + i);
                    String currentWeekString = "";

                    for (Map<String, Object> item : outMessagesItems) {
                      //  L.d("currentWeekString this =" + item.get("body").toString());

                        long date = Long.valueOf(item.get("date").toString());

                        if (Long.valueOf(weeks.get(i).first.toString())  <=  date &&  date <= Long.valueOf(weeks.get(i).second.toString())) {

                            if ((currentWeekString.length() + item.get("body").toString().length()) > 10000){
                                L.d("currentWeekString first = " +  (currentWeekString.length() + item.get("body").toString().length()));
                                break;
                            } else {
                                currentWeekString += item.get("body").toString() + " ";
                            }


                        }


                    }
                    L.d("currentWeekString full = " +  currentWeekString);
                    Map<Integer, String> sortedItem = new HashMap<>();
                    sortedItem.put(i, currentWeekString);

                    messagesSortedByWeeks.add(sortedItem);

                    L.d("messagesSortedByWeeks size = " + messagesSortedByWeeks.size());

                    L.d("messagesSortedByWeeks length = " + currentWeekString.length() + " week = " + i);


                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        checkInSpeller = new CheckInSpeller();
                        checkInSpeller.run(new OnAnalyticListener() {
                            @Override
                            public void onSuccess() {

                                mistakesPercents = checkInSpeller.getResult();
                                topMistakes = checkInSpeller.getTopMistakes();
                                L.d("mistakesPercents size = " + mistakesPercents.size());
                               onServerResult.onSuccess();
                            }

                            @Override
                            public void onStep(String user) {

                            }

                            @Override
                            public void onStep(Map<String, Object> photo) {

                            }

                            @Override
                            public void onStep(Map<String, Object> photo, Integer id) {

                            }

                            @Override
                            public void onProgress(Integer indexUser, Integer lengthUser) {

                            }

                            @Override
                            public void onError() {

                            }
                        }, messagesSortedByWeeks);
                    }
                });
            }
        });
        thread.start();




    }

//    public void parseMessageToStatistic(List<Map<String, Object>> itemsPart, String userId){
//        for(Map<String, Object> item:itemsPart){
//            if(!item.get("from_id").toString().equals(userId)){
//                myMessages++;
//                myWords += countWords(item.get("body").toString(), myWordsList);
//
//                //считаем аттачменты
//                if(item.get("attachments") != null){
//                    List<Map<String,Object>> attachments = ((List<Map<String,Object>>) item.get("attachments"));
//                    for(Map<String,Object> attachment:attachments){
//                        if(myCountAttachments.containsKey(attachment.get("type").toString())){
//                            myCountAttachments.put(attachment.get("type").toString(), myCountAttachments.get(attachment.get("type").toString()) + 1);
//                        }else{
//                            myCountAttachments.put(attachment.get("type").toString(), 1);
//                        }
//                    }
//                }
//            }else{
//                otherMessages++;
//                otherWords += countWords(item.get("body").toString(), otherWordsList);
//
//                //считаем аттачменты
//                if(item.get("attachments") != null){
//                    List<Map<String,Object>> attachments = ((List<Map<String,Object>>) item.get("attachments"));
//                    for(Map<String,Object> attachment:attachments){
//                        if(otherCountAttachments.containsKey(attachment.get("type").toString())){
//                            otherCountAttachments.put(attachment.get("type").toString(), otherCountAttachments.get(attachment.get("type").toString()) + 1);
//                        }else{
//                            otherCountAttachments.put(attachment.get("type").toString(), 1);
//                        }
//                    }
//                }
//            }
//        }
//    }



    public static <K, V extends Comparable<? super V>> Map<K, V>  sortByValue( Map<K, V> map ) {
        List<Map.Entry<K, V>> list =
                new LinkedList<Map.Entry<K, V>>( map.entrySet() );
        Collections.sort( list, new Comparator<Map.Entry<K, V>>()
        {
            public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
            {
                return (o2.getValue()).compareTo( o1.getValue() );
            }
        } );

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list)
        {
            result.put( entry.getKey(), entry.getValue() );
        }
        return result;
    }
}
