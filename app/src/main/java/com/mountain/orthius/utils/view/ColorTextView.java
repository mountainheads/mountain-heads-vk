package com.mountain.orthius.utils.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;


import com.mountain.orthius.R;
import com.mountain.orthius.loging.L;
import com.pixplicity.htmlcompat.HtmlCompat;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by spbiphones on 19.10.17.
 */

public class ColorTextView extends HtmlTextView {

    private List<String> mColorTexts = new ArrayList<>();
    private List<String> mColors = new ArrayList<>();
    private int size;
    private String mCurrentText;
    private String colorTexts;
    private String colors;
    private Context context;

    public ColorTextView(Context context) {
        super(context);
        this.context = context;
    }

    public ColorTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ColorTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray attrsArray = getContext().obtainStyledAttributes(attrs, R.styleable.ColorTextView, 0, 0);
        colorTexts = attrsArray.getString(R.styleable.ColorTextView_color_texts);
        colors = attrsArray.getString(R.styleable.ColorTextView_color_arrays);
        attrsArray.recycle();

        initData(colorTexts, colors);

        setHtmlText();
    }

    private void initData(String colorTexts, String colors) {
        try {
            if (!TextUtils.isEmpty(colorTexts)) {
                String[] texts = colorTexts.split("\\|");
                for (int i = 0; i < texts.length; i++) {
                    mColorTexts.add(texts[i]);
                }
            }

            if (!TextUtils.isEmpty(colors)) {
                String[] texts = colors.split("\\|");
                for (int i = 0; i < texts.length; i++) {
                    mColors.add(texts[i]);
                }
            }
            size = Math.min(mColors.size(), mColorTexts.size());
            mCurrentText = getText().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setHtmlText() {
        try {
            if (!TextUtils.isEmpty(mCurrentText)) {
                for (int i = 0; i < size; i++) {
                    mCurrentText = mCurrentText.replace(mColorTexts.get(i), color(mColors.get(i), mColorTexts.get(i)));
                }
            }
            if (!TextUtils.isEmpty(mCurrentText)) {
                Spanned fromHtml = HtmlCompat.fromHtml(context, mCurrentText, 0);

                setMovementMethod(LinkMovementMethod.getInstance());
                setText(fromHtml);
                // setHtml(mCurrentText);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void textSetted() {
        initData(colorTexts, colors);

        setHtmlText();
    }


    public ColorTextView findAndSetStrColor(String str, String color) {

        try {
            if (!TextUtils.isEmpty(mCurrentText)) {
                mCurrentText = mCurrentText.replaceAll(str, color(color, str));
            }
            if (!TextUtils.isEmpty(mCurrentText)) {
                Spanned fromHtml = HtmlCompat.fromHtml(context, mCurrentText, 0);

                setMovementMethod(LinkMovementMethod.getInstance());
                setText(fromHtml);
                //setHtml(mCurrentText);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }


    public void readyToFixMistakes(String fullText) {
        mCurrentText = fullText;
    }

    public ColorTextView findAndFixMistake(String str, String fixedWord) {

        try {
            if (!TextUtils.isEmpty(mCurrentText)) {
                mCurrentText = mCurrentText.replaceAll(str, fixedWord);
            }
            if (!TextUtils.isEmpty(mCurrentText)) {

                setText(mCurrentText);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }


    public String color(String colorCode, String str) {
        //String html = "<font background-color = \""  + colorCode + "\">" + str + "</font>";
        // String html = "<font style=\"background : " + colorCode + "\">" + str + "</font>";
        String html = "<span style=\"color: " + colorCode + "\">" + str + "</span>";
        L.d("checkText html changed = " + html);
        return html;
    }

    public String colorCurrent(String colorCode, String str) {
        //String html = "<font background-color = \""  + colorCode + "\">" + str + "</font>";
        // String html = "<font style=\"background : " + colorCode + "\">" + str + "</font>";
        String html = "<span style=\"background-color: " + colorCode + "; color: #ffffff\">" + str + "</span>";
        L.d("checkText html changed = " + html);
        return html;
    }

    public String colorCurrentFixed(String str) {
        //String html = "<font background-color = \""  + colorCode + "\">" + str + "</font>";
        // String html = "<font style=\"background : " + colorCode + "\">" + str + "</font>";
        String html = "<span style=\"color: " + "#000000" + "\">" + str + "</span>";
        L.d("checkText html changed = " + html);
        return html;
    }


    public ColorTextView findAndSetCurrent(String str, String color) {
        String currentStr = "<span style=\"color: #f26180\">" + str + "</span>";
        try {
            if (!TextUtils.isEmpty(mCurrentText)) {
                mCurrentText = mCurrentText.replaceAll(currentStr, colorCurrent(color, str));
            }
            if (!TextUtils.isEmpty(mCurrentText)) {
                Spanned fromHtml = HtmlCompat.fromHtml(context, mCurrentText, 0);

                setMovementMethod(LinkMovementMethod.getInstance());
                setText(fromHtml);
                //setHtml(mCurrentText);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public ColorTextView findAndFixCurrentMistake(String str, String fixedWord) {
        String currentStr = "<span style=\"background-color: #f26180; color: #ffffff\">" + str + "</span>";
        try {
            if (!TextUtils.isEmpty(mCurrentText)) {
                mCurrentText = mCurrentText.replaceAll(currentStr, colorCurrentFixed(fixedWord));
            }
            if (!TextUtils.isEmpty(mCurrentText)) {

                Spanned fromHtml = HtmlCompat.fromHtml(context, mCurrentText, 0);

                setMovementMethod(LinkMovementMethod.getInstance());
                setText(fromHtml);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public ColorTextView findCurrentMistake(String str, String fixedWord) {
        String currentStr = "<span style=\"background-color: #f26180; color: #ffffff\">" + str + "</span>";
        try {
            if (!TextUtils.isEmpty(mCurrentText)) {
                mCurrentText = mCurrentText.replaceAll(currentStr, color("#f26180", fixedWord));
            }
            if (!TextUtils.isEmpty(mCurrentText)) {

                Spanned fromHtml = HtmlCompat.fromHtml(context, mCurrentText, 0);

                setMovementMethod(LinkMovementMethod.getInstance());
                setText(fromHtml);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

}