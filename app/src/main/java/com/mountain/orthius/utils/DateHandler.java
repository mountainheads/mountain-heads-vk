package com.mountain.orthius.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by alexeybalabanov on 20.01.15.
 */
public class DateHandler {
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String ONLY_DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_FORMAT_VK = "dd.MM.yyyy";

    public static final String ONLY_TIME = "HH:mm";

    public final static long SECOND_MILLIS = 1000;
    public final static long MINUTE_MILLIS = SECOND_MILLIS*60;
    public final static long HOUR_MILLIS = MINUTE_MILLIS*60;
    public final static long DAY_MILLIS = HOUR_MILLIS*24;

    public static int getDiffYears(Date first, Date last) {
        Calendar a = getCalendar(first);
        Calendar b = getCalendar(last);
        int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
        if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) ||
                (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        }
        return diff;
    }

    public static String getCHPUDurationBy(long minutes){
        long outputMinutes = 0, outputHours = 0, outputDays = 0;
        outputMinutes = minutes % 60;
        outputHours = Math.abs(minutes / 60) % 24;
        outputDays = Math.abs(minutes / 60 / 24);

        String output = (outputDays != 0 ? outputDays + " дн. " : "") + (outputHours != 0 ? outputHours + " ч. " : "") + (outputMinutes != 0 ? outputMinutes + " мин. " : "");

        return output;
    }

    public static long getDiffMinutes(Date first, Date last) {
        if(first == null || last == null){
            return 0;
        }

        long firstUnixMinutes = Math.round(first.getTime()/60/1000);
        long lastUnixMinutes = Math.round(last.getTime()/60/1000);

        long diff = lastUnixMinutes - firstUnixMinutes;
        return diff;
    }
    public static long getDiffSeconds(Date first, Date last) {
        if(first == null || last == null){
            return 0;
        }

        long firstUnixMinutes = Math.round(first.getTime()/1000);
        long lastUnixMinutes = Math.round(last.getTime()/1000);

        long diff = lastUnixMinutes - firstUnixMinutes;
        return diff;
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

    public static String getFullDateToJournal(String mDate) throws Exception {
        String dayAndMonth = getDateToJournal(mDate);
        String year = mDate.substring(0, 4);
        return dayAndMonth + " " + year;
    }

    public static String getDateToJournal(String mDate) throws Exception {
        String date = mDate.substring(0, 10);
        String[] dateArr = date.split("-");
        String month = dateArr[1];
        String day = dateArr[2].substring(0,1).equals("0") ? dateArr[2].substring(1,2) : dateArr[2];


        String today = convertDateToString(getCurrentTime()).substring(0, 10);

        String output = null;
        if(month.equals("01")){
            output = day + " Января";
        }else if(month.equals("02")){
            output = day + " Февраля";
        }else if(month.equals("03")){
            output = day + " Марта";
        }else if(month.equals("04")){
            output = day + " Апреля";
        }else if(month.equals("05")){
            output = day + " Мая";
        }else if(month.equals("06")){
            output = day + " Июня";
        }else if(month.equals("07")){
            output = day + " Июля";
        }else if(month.equals("08")){
            output = day + " Августа";
        }else if(month.equals("09")){
            output = day + " Сентября";
        }else if(month.equals("10")){
            output = day + " Октября";
        }else if(month.equals("11")){
            output = day + " Ноября";
        }else if(month.equals("12")){
            output = day + " Декабря";
        }
        if(output != null){
            if(today.equals(date)){
                output = "Сегодня";
            }
//            if(getAmountDate(mDate) == 0){
//                output = "Вчера";
//            }
            return output;
        }else{
            throw new Exception("Month undefined");
        }
    }

    public static String getDate() {
        Date date = new Date();
        return convertDateToString(date);
    }
    public static String getOnlyDate() {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat(ONLY_DATE_FORMAT);
        return dateFormat.format(date);
    }

    public static String getOnlyDate(Date date) {
        DateFormat dateFormat = new SimpleDateFormat(ONLY_DATE_FORMAT);
        return dateFormat.format(date);
    }

    public static String getYesterdayDate() {
        Date date = new Date();
        Calendar c = new GregorianCalendar();//��������� �� ������� ����

        c.add(Calendar.DAY_OF_YEAR, -1); //��������� ���� �� 1 ����
        return convertDateToString(c.getTime());
    }

    public static String convertDateToString(Date date) {
        DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        return dateFormat.format(date);
    }
    public static String convertOnlyDateToString(Date date) {
        DateFormat dateFormat = new SimpleDateFormat(ONLY_DATE_FORMAT);
        return dateFormat.format(date);
    }



    public static int getAmountDate(String startDate) {
        if (startDate.equals("-1")){
            return 0;
        }

        String d1 = getDate();
        String d2 = startDate;

        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);

        Date date1 = null;
        Date date2 = null;
        try {
            date1 = format.parse(d1);
            date2 = format.parse(d2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long difference = date1.getTime() - date2.getTime();
        int days = (int) difference / (24 * 60 * 60 * 1000);
        return days;
    }
    public static int getAmountOnlyDate(String startDate) {
        if (startDate.equals("-1")){
            return 0;
        }

        String d1 = getOnlyDate();
        String d2 = startDate;

        SimpleDateFormat format = new SimpleDateFormat(ONLY_DATE_FORMAT);

        Date date1 = null;
        Date date2 = null;
        try {
            date1 = format.parse(d1);
            date2 = format.parse(d2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long difference = date1.getTime() - date2.getTime();
        int days = (int) difference / (24 * 60 * 60 * 1000);
        return days;
    }

    public static Date getCurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        String strDate = sdf.format(c.getTime());

        return convertStringToDate(strDate);
    }
    public static Date convertStringToDate(String strDate){
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        Date date = null;
        try {
            date = format.parse(strDate);
            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }
    public static Date convertStringToDateVK(String strDate){
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_VK);
        Date date = null;
        try {
            date = format.parse(strDate);
            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }
    public static Integer getSubstractYear (int age, int month, int day){
        try{


            SimpleDateFormat sdf = new SimpleDateFormat(ONLY_DATE_FORMAT);
            Date myDate = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(myDate);
            //calendar.add(Calendar.YEAR , -age );
            int nowYear = calendar.get(Calendar.YEAR);
            int nowMonth = calendar.get(Calendar.MONTH);
            int nowDay = calendar.get(Calendar.DAY_OF_MONTH);

            boolean wasBDay = false;
            if(month < nowMonth){
                wasBDay = true;
            }else{
                if(month == nowMonth){
                    if(day <= nowDay){
                        wasBDay = true;
                    }
                }
            }

            if(wasBDay){
                calendar.add(Calendar.YEAR , -age );
            }else{
                calendar.add(Calendar.YEAR , -(age + 1));
            }


            return calendar.get(Calendar.YEAR);
        }
        catch ( Exception e ){
            e.printStackTrace();
            return null;
        }
    }
    public static String getOnlyTime(Date date) {
        DateFormat dateFormat = new SimpleDateFormat(ONLY_TIME);
        return dateFormat.format(date);
    }
}
