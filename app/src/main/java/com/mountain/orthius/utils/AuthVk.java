package com.mountain.orthius.utils;

import android.app.Activity;

import com.mountain.orthius.Define;
import com.mountain.orthius.loging.L;
import com.vk.sdk.VKSdk;


/**
 * Created by alexeybalabanov on 13.11.14.
 */
public class AuthVk {
    public interface AuthVkListener{
        public void onAuth();
    }
    public static void checkAndLogin(final AuthVkListener authVkListener, final Activity activity){
        if(VKSdk.isLoggedIn()){
            authVkListener.onAuth();
        }else{
            VKSdk.login(activity, Define.SCOPE);
            Thread myThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    L.d("run() end");
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            authVkListener.onAuth();
                        }
                    });
                }
            });
            myThread.start();
        }
    }
}
