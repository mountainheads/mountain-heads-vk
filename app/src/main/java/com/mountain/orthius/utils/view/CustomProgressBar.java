package com.mountain.orthius.utils.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by alexeybalabanov on 19.12.14.
 */
public class CustomProgressBar extends ImageView {

    public CustomProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomProgressBar(Context context) {
        super(context);
        init();
    }

    private void init() {

    }
}
