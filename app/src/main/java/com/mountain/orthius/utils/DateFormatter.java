package com.mountain.orthius.utils;


import android.util.Pair;

import com.mountain.orthius.loging.L;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by spbiphones on 20.09.17.
 */

public class DateFormatter {
    public static String getFormattedDate(long date) {
        L.d("my date = " + date);
        Calendar thirdDay = new GregorianCalendar();
        thirdDay.setTimeInMillis(date);
        thirdDay.add(Calendar.DAY_OF_YEAR, 3);
        thirdDay.set(Calendar.HOUR_OF_DAY, 0);
        L.d("my date third = " + thirdDay.getTimeInMillis());
        long newDate = thirdDay.getTimeInMillis();
        L.d("my date third2 = " + newDate);
        String your_format = new SimpleDateFormat("dd-MM-yyyy").format(new Date(newDate));
        L.d("my date third3 = " + your_format);
        return your_format;
    }
    public static String getFormattedDateAndTime(long date) {

        String your_format = new SimpleDateFormat("dd.MM.yyyy").format(new Date(date));
        L.d("my date third3 = " + your_format);
        return your_format;
    }
    public static boolean isReadyToCompleteThirdDay(long date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(System.currentTimeMillis());

        Calendar calendarLastPhoto = new GregorianCalendar();
        calendarLastPhoto.setTimeInMillis(date);
        calendarLastPhoto.add(Calendar.DAY_OF_YEAR, 3);
        calendarLastPhoto.set(Calendar.HOUR_OF_DAY, 0);
        if (calendar.getTimeInMillis() >= calendarLastPhoto.getTimeInMillis()) {
            L.d("Alarm compare dates true");
            return true;
        } else return false;

    }
    public static boolean isLoadMore(long date) {

        Calendar calendarThreeMonthAgo = new GregorianCalendar();
        calendarThreeMonthAgo.setTimeInMillis(System.currentTimeMillis());
        calendarThreeMonthAgo.add(Calendar.MONTH, -3);
        calendarThreeMonthAgo.set(Calendar.HOUR_OF_DAY, 0);
        L.d("compare dates calendarThreeMonthAgo = " + calendarThreeMonthAgo.getTimeInMillis());
        L.d("compare dates date = " + date);
        if (calendarThreeMonthAgo.getTimeInMillis()/1000 <= date) {
            L.d("compare dates calendarThreeMonthAgo = " + calendarThreeMonthAgo.getTimeInMillis());
            L.d("Alarm compare dates true");
            return true;
        } else return false;

    }

    public static long dateThreeMothAgo(){
        Calendar calendarThreeMonthAgo = new GregorianCalendar();
        calendarThreeMonthAgo.setTimeInMillis(System.currentTimeMillis());
        calendarThreeMonthAgo.add(Calendar.MONTH, -3);
        calendarThreeMonthAgo.set(Calendar.HOUR_OF_DAY, 0);

        return calendarThreeMonthAgo.getTimeInMillis()/1000;
    }

    public static ArrayList<Pair> createWeeks(){

        ArrayList<Pair> weeks = new ArrayList<>();

        for (int i = 1; i < 13; i++){
            Calendar startOfWeek = new GregorianCalendar();
            startOfWeek.setTimeInMillis(System.currentTimeMillis());
            startOfWeek.add(Calendar.WEEK_OF_YEAR, -(i));
            startOfWeek.set(Calendar.HOUR_OF_DAY, 0);

            Calendar endOfWeek = new GregorianCalendar();
            endOfWeek.setTimeInMillis(System.currentTimeMillis());
            endOfWeek.add(Calendar.WEEK_OF_YEAR, -(i-1));
            endOfWeek.set(Calendar.HOUR_OF_DAY, 0);

            weeks.add(new Pair(startOfWeek.getTimeInMillis()/1000, endOfWeek.getTimeInMillis()/1000));


        }
        return weeks;



    }

    public static boolean isCurrentWeek(long date, int currentWeek){
        int prevWeekC = currentWeek - 1;
        Calendar weeksAgo = new GregorianCalendar();
        weeksAgo.setTimeInMillis(System.currentTimeMillis());
        weeksAgo.add(Calendar.WEEK_OF_YEAR, -(currentWeek));
        weeksAgo.set(Calendar.HOUR_OF_DAY, 0);

        Calendar prevWeek = new GregorianCalendar();
        prevWeek.setTimeInMillis(System.currentTimeMillis());
        prevWeek.add(Calendar.WEEK_OF_YEAR, -(prevWeekC));
        prevWeek.set(Calendar.HOUR_OF_DAY, 0);

        L.d("compare dates weeksAgo = " + weeksAgo.getTimeInMillis()/1000);
        L.d("compare dates prevWeek = " + prevWeek.getTimeInMillis()/1000);
        L.d("compare dates date = " + date);
        L.d("compare dates week = " + currentWeek);

        if (weeksAgo.getTimeInMillis()/1000 <= date && date >= prevWeek.getTimeInMillis()/1000) {

            return true;
        } else return false;
    }
}
