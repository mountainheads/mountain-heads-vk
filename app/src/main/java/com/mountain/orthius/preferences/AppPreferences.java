package com.mountain.orthius.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class AppPreferences {
	private static final String MY_ID = "myId";
    public static final String PASSWORD = "PASSWORD";
    public static final String PHPSESSID = "PHPSESSID";
    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";
    public static final String IS_AUTH_VK = "IS_AUTH_VK";
    public static final String VK_USER_ID = "VK_USER_ID";

    public static final String IS_LAUNCH = "IS_LAUNCH";
    public static final String COUNT_COINS = "COUNT_COINS";

    public static String getMyId(Context context) {
        SharedPreferences sPref = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sPref.getString(VK_USER_ID,"-1");
    }

    public static Integer getCountCoins(Context context) {
        SharedPreferences sPref = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sPref.getInt(COUNT_COINS, 0);
    }



    public static void addCountCoins(Context context, Integer count) {
        SharedPreferences sPref = PreferenceManager
                .getDefaultSharedPreferences(context);
        Editor ed = sPref.edit();
        ed.putInt(COUNT_COINS, getCountCoins(context) + count);
        ed.commit();
    }




    public static boolean isAuthVk(Context context) {
        SharedPreferences sPref = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sPref.getBoolean(IS_AUTH_VK,false);
    }

    public static void setAuthVk(Context context) {
        SharedPreferences sPref = PreferenceManager
                .getDefaultSharedPreferences(context);
        Editor ed = sPref.edit();
        ed.putBoolean(IS_AUTH_VK, true);
        ed.commit();
    }




    public static String getVkId(Context context) {
        SharedPreferences sPref = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sPref.getString(VK_USER_ID,"-1");
    }

    public static void setVkId(Context context, String id) {
        SharedPreferences sPref = PreferenceManager
                .getDefaultSharedPreferences(context);
        Editor ed = sPref.edit();
        ed.putString(VK_USER_ID, id);
        ed.commit();
    }

    public static boolean isLaunch(Context context) {
        SharedPreferences sPref = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sPref.getBoolean(IS_LAUNCH, false);
    }

    public static void setLaunch(Context context){
        SharedPreferences sPref = PreferenceManager
                .getDefaultSharedPreferences(context);
        Editor ed = sPref.edit();
        ed.putBoolean(IS_LAUNCH, true);
        ed.commit();
    }
}
